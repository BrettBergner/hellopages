# MTTL Reference Table

## Purpose

This repository contains the MTTL list. The main page’s README.md file has a short introduction about the repository and contributing guidelines.

## MTTL Table Management

As an effort to assist those managing this repo, several reports are generated that show certain statistical data about the questions, such as attempts, passes and failures; and how well the MTTL is covered by the test bank. These reports are saved as artifacts to the repo and are freely available for download. Additionally, there are several scripts to assist with managing the MTTL.

## Directory Structure
The following information explains the directories and files in this repository that are relevant to developers/owners of this repository.

The question structure is explained in the [User Guide](http://mpls-as-701v:8090/display/TRAIN/MTTL+GitLab+Repository+User+Guide).

```
├── Basic Dev.md
├── Basic Po.md
├── DeveloperGuide.md
├── Embedded.md
├── MTTL.csv
├── MTTL.md
├── Mobile.md
├── Network.md
├── README.md
├── Sdev-Lin.md
├── Sdev-Win.md
├── Senior Po.md
├── knowledge (see UserGuide.md in the knowledge repo)
├── performance see UserGuide.md in the performance repo)
└── questioncustodian (see UpdaterGenerator repo)
```

*   [Basic Dev.md](Basic Dev.md)
    *   This is a markdown table of all the Basic Dev work role tasks.
*   [Basic Po.md](Basic Po.md)
    *   This is a markdown table of all the Basic Product Owner work role tasks.
*   [DevelopersGuide.md](DevelopersGuide.md)
    *   This document will detail how to submit a question.
*   [Embedded.md](Embedded.md)
    *   This is a markdown table of all the Embedded work role tasks.
*   [MTTL.csv](MTTL.csv)
    *   This is a csv of all the MTTL line items on a csv format.
*   [MTTL.md](MTTL.md)
    *   This is a markdown table of all the MTTL line items.
*   [README.md](README.md)
    *   This is a markdown table of all the Mobile work role tasks.
*   [Network.md](Network.md)
    *   This is a markdown table of all the Network work role tasks.
*   [README.md](README.md)
    *   This is the repository's file that contains the master question list.
*   [Sdev-Lin.md](Sdev-Lin.md)
    *   This is a markdown table of all the Senior Developer-Linux work role tasks.
*   [Sdev-Win.md](Sdev-Win.md)
    *   This is a markdown table of all the Senior Developer-Windows work role tasks.
*   [Senior Po.md](Senior Po.md)
    *   This is a markdown table of all the Senior Product Owner work role tasks.
*   [knowledge/](knowledge)
    *   This folder contains the Knowledge repo as a submodule.
*   [performance/](performance)
    *   This folder contains the Performance repo as a submodule.
*   [questioncustodian/](questioncustodian)
    *   This folder contains the UpdaterGenerator repo as a submodule.

## Creating New MTTL

Creating a new task/work role in the MTTL is trivial but these procedures will assist in reducing problems encountered during creation. 

### To add a new task/work role:
1.	Start by creating a new branch from the appropriate repo’s ‘dev’ branch called “\<work role\>-submission” or "\<name-of-the-task\>-submission", depending on what you're doing.

2.	Determine the work role of the new task(s). If the work role does not have a column, add it the far right of the table but just before the comments column; ensure you create an accompanying critical tasks column to go with the new work role.

3.	Create your new task row at the end of the existing list and fill in the appropriate values for each cell.
    *	Leave the UID blank, this will be created from the scripts


4.	When you’re satisfied with the content of your question, push to your new branch. The CI/CD pipeline will run scripts to validate your new task and give it a UID.
    
    *	UpdateMTTLMarkdown.py for updating the overall MTTL tables.


5.	Check the pipeline and make sure all the jobs run successfully. If something fails then look at the output and fix the problems.

6.	When everything runs without error, you’re now ready to submit a merge request to merge your branch into dev. Note, make sure the target branch is set to dev; it currently defaults to master.

## Running Scripts Manually

Several scripts were created to help automate some of the more repeatable tasks. This includes things like configuring/building question README files and generating statistical reports. While you can use the repo’s WebIDE to create your new question(s), you’ll need a system with git and python 3 installed to run these scripts manually. Additionally, you’ll need to clone the repository and ensure you have the appropriate permissions to pull and push to the repository in question, whether that’s the Performance, performance, or both. If your GitLab account doesn’t have a ssh key configured then you can use the http link when cloning the repository. The list below are the available modules that can be run using the auto.py script.

*	GenerateCoverageReports
*	GenerateGoogleScriptFiles
*	MasterQuestionListUpdater (must be run with auto.py’s -a option only)
*	MTTLStatisticsReport
*	QuestionGroupConfig
*	QuestionReadmeGen
*	QuestionStatisticReport
*	UidUpdater
*	ValidateJson
*   UpdateMTTLMarkdown

Follow the link below for an explanation of running these modules.

*   [Script User Guide](https://gitlab.com/90COS/Private/evaluation/questions/questioncustodian)