# Mobile Table Test

Note: Use left/right arrows to scroll the table left/right.

| UID | Req Src | Req Src ID | Requirement Owner\(s\) | Category | Topic/Language/Tool/Application | TASK | SUB TASK | Mobile Crit Task | Mobile | COMMENTS |  |  |  |
| :--- | :--- | :--- | :--- | :--- | :--- | :--- | :--- | :--- | :--- | :--- | :--- | :--- | :--- |
| 961 |  |  |  | Project | Organization Overview | 90 IOS Mission and Vision |  |  | A |  |  |  |  |
| 962 |  |  |  | Project | Organization Overview | 90 IOS Organizational Structure |  |  | A |  |  |  |  |
| 963 |  |  |  | Project | Organization Overview | Real\-Time Operations and Innovation \(RTO&I\) Development Requirements |  |  | A |  |  |  |  |
| 964 |  |  |  | Project | Organization Overview | Capability Development Initiation |  |  | A |  |  |  |  |
| 965 |  |  |  | Project | Organization Overview | Flight Missions |  |  | B |  |  |  |  |
| 966 |  |  |  | Project | Supporting/Supported Organizations | 90 IOS\-Supported Organizations |  |  | A |  |  |  |  |
| 967 |  |  |  | Project | Supporting/Supported Organizations | Organizations Supporting 90 IOS |  |  | A |  |  |  |  |
| 968 |  |  |  | Project | Supporting/Supported Organizations | Mission Area Responsibilities of Supported/Supporting Organizations |  |  | B |  |  |  |  |
| 970 |  |  |  | Project | Cyber Mission Forces | Cyber Mission Forces \(CMF\) Teams |  |  | B |  |  |  |  |
| 971 |  |  |  | Project | Cyber Mission Forces | 90 IOS CMF\-Role |  |  | A |  |  |  |  |
| 972 |  |  |  | Project | Cyber Mission Forces | Interaction Between CMF Teams |  |  | A |  |  |  |  |
| 973 |  |  |  | Project | Cyber Mission Forces | 90 IOS CMF Teams |  |  | A |  |  |  |  |
| 975 |  |  |  | Project | Exercises | Common 90 IOS Exercises |  |  | A |  |  |  |  |
| 1119 |  |  |  | Programming | Python | Conversion Functions |  |  | 3c |  |  |  |  |
| 1122 |  |  |  | Programming | Python | Modules |  |  | 3c |  |  |  |  |
| 1126 |  |  |  | Programming | Python | Python Standard Library |  |  | 1a |  |  |  |  |
| 1127 |  |  |  | Programming | Python | Programming with Functions and Metaclasses |  |  | 2c |  |  |  |  |
| 1128 |  |  |  | Programming | Python | Multi\-threading |  |  | 1a |  |  |  |  |
| 1129 |  |  |  | Programming | Python | Networking modules |  |  | 3c |  |  |  |  |
| 1130 |  |  |  | Programming | Python | Client/Server programming in Python |  |  | 2c |  |  |  |  |
| 1132 |  |  |  | Programming | Python | Python from Command line |  |  | A |  |  |  |  |
| 1140 |  |  |  | Programming | C | Advanced Pointers |  |  | 3c |  |  |  |  |
| 1141 |  |  |  | Programming | C | Structures |  |  | 3c |  |  |  |  |
| 1142 |  |  |  | Programming | C | Function/structure recursion |  |  | 3c |  |  |  |  |
| 1143 |  |  |  | Programming | C | Memory Management concepts |  |  | 3c |  |  |  |  |
| 1145 |  |  |  | Programming | C | How the process stack functions |  |  | 3c |  |  |  |  |
| 1146 |  |  |  | Programming | C | Stack based memory and heap based memory |  |  | 3c |  |  |  |  |
| 1147 |  |  |  | Programming | C | Overview of the process stack memory |  |  | 3c |  |  |  |  |
| 1148 |  |  |  | Programming | C | Stack pointers |  |  | 3c |  |  |  |  |
| 1149 |  |  |  | Programming | C | Header files and program structure |  |  | 3c |  |  |  |  |
| 1150 |  |  |  | Programming | C | Casting in C |  |  | 3c |  |  |  |  |
| 1151 |  |  |  | Programming | C | C Standard Library |  |  | 3c |  |  |  |  |
| 1152 |  |  |  | Programming | C | Basic Data Structures |  |  | 3c |  |  |  |  |
| 1153 |  |  |  | Programming | C\+\+ | Shared Pointers |  |  | 3c |  |  |  |  |
| 1154 |  |  |  | Programming | C\+\+ | Unique Pointers |  |  | 3c |  |  |  |  |
| 1155 |  |  |  | Programming | C\+\+ | Classes |  |  | 3c |  |  |  |  |
| 1156 |  |  |  | Programming | C\+\+ | Standard Template Library |  |  | 3c |  |  |  |  |
| 1157 |  |  |  | Programming | C\+\+ | Unique C\+\+ Tools |  |  | 3c |  |  |  |  |
| 1158 |  |  |  | Programming | C\+\+ | Virtual Functions |  |  | B |  |  |  |  |
| 1159 |  |  |  | Programming | C\+\+ | Virtual Classes |  |  | B |  |  |  |  |
| 1160 |  |  |  | Programming | C\+\+ | Polymorphism |  |  | B |  |  |  |  |
| 1161 |  |  |  | Programming | C\+\+ | Operator Overloading |  |  | B |  |  |  |  |
| 1162 |  |  |  | Programming | C\+\+ | Inheritance |  |  | B |  |  |  |  |
| 1163 |  |  |  | Programming | C\+\+ | Templates |  |  | B |  |  |  |  |
| 1164 |  |  |  | Programming | C\+\+ | C\+\+ Types |  |  | 2b |  |  |  |  |
| 1165 |  |  |  | Programming | C\+\+ | History |  |  | B |  |  |  |  |
| 1166 |  |  |  | Programming | Assembly Programming | Overview |  |  | A |  |  |  |  |
| 1167 |  |  |  | Programming | Assembly Programming | x86 64 |  |  | B |  |  |  |  |
| 1168 |  |  |  | Programming | Assembly Programming | Syscall |  |  | B |  |  |  |  |
| 1169 |  |  |  | Programming | Assembly Programming | Sysenter |  |  | B |  |  |  |  |
| 1170 |  |  |  | Programming | Assembly Programming | Interrupts |  |  | B |  |  |  |  |
| 1171 |  |  |  | Programming | Assembly Programming | Memory segmentation |  |  | B |  |  |  |  |
| 1172 |  |  |  | Programming | Assembly Programming | x86 64 Calling Conventions |  |  | B |  |  |  |  |
| 1173 |  |  |  | Programming | Assembly Programming | Single Instruction Multiple Data \(SIMD\) |  |  | A |  |  |  |  |
| 1174 |  |  |  | Programming | Assembly Programming | Privilege Levels |  |  | A |  |  |  |  |
| 1175 |  |  |  | Programming | Assembly Programming | Stack frame |  |  | 2c |  |  |  |  |
| 1176 |  |  |  | Programming | Assembly Programming | General Purpose Instruction |  |  | 2c |  |  |  |  |
| 1177 |  |  |  | Programming | Assembly Programming | Registers and Sub\-registers |  |  | 2c |  |  |  |  |
| 1178 |  |  |  | Programming | Assembly Programming | Branch in |  |  | 2c |  |  |  |  |
| 1179 |  |  |  | Programming | Assembly Programming | Memory access |  |  | 2c |  |  |  |  |
| 1180 |  |  |  | Programming | Assembly Programming | Functions |  |  | 2c |  |  |  |  |
| 1181 |  |  |  | Programming | Assembly Programming | x86 Calling Conventions |  |  | 2c |  |  |  |  |
| 1182 |  |  |  | Programming | Assembly Programming | Name Mangling |  |  | 2c |  |  |  |  |
| 1183 |  |  |  | Programming | Assembly Programming | Flags |  |  | 2c |  |  |  |  |
| 1184 |  |  |  | Programming | Assembly Programming | String Instructions |  |  | 2c |  |  |  |  |
| 1185 |  |  |  | Programming | Assembly Programming | Bitwise Instructions |  |  | 2c |  |  |  |  |
| 1186 |  |  |  | Programming | Assembly Programming | Protection Mechanisms |  |  | 2c |  |  |  |  |
| 1187 |  |  |  | Programming | Assembly Programming | Opcodes |  |  | 2c |  |  |  |  |
| 1188 |  |  |  | Programming | Assembly Programming | Byte ordering |  |  | 2c |  |  |  |  |
| 1189 |  |  |  | Programming | Assembly Programming | Begin debugging with the GNU Source\-Level Debugger \(GDB\) |  |  | 2c |  |  |  |  |
| 1190 |  |  |  | Programming | Assembly Programming | Arithmetic Operations |  |  | 2c |  |  |  |  |
| 1191 |  |  |  | Programming | PowerShell | Overview |  |  | A |  |  |  |  |
| 1218 |  |  |  | Programming | Bash Scripting | Overview |  |  | A |  |  |  |  |
| 1229 |  |  |  | Software | Virtualization Software | Overview |  |  | B |  |  |  |  |
| 1234 |  |  |  | Software | Virtualization Software | Core Concepts |  |  | A |  |  |  |  |
| 1235 |  |  |  | Software | Virtualization Software | Tracking Issues |  |  | A |  |  |  |  |
| 1236 |  |  |  | Project | Wiki | Overview |  |  | A |  |  |  |  |
| 1238 |  |  |  | Programming | Immunity | Overview |  |  | A |  |  |  |  |
| 1247 |  |  |  | Software | Metasploit | Overview |  |  | A |  |  |  |  |
| 1256 |  |  |  | Network | Networking Concepts | Packet Captures |  |  | 2c |  |  |  |  |
| 1257 |  |  |  | Network | Networking Concepts | Packet Capture \(PCAP\) File Analysis |  |  | 2c |  |  |  |  |
| 1258 |  |  |  | Network | Networking Concepts | Capture Filters |  |  | 2c |  |  |  |  |
| 1259 |  |  |  | Network | Networking Concepts | Trace Connection |  |  | 2c |  |  |  |  |
| 1260 |  |  |  | Network | Networking Concepts | Secure vs Non\-secure Traffic \(encrypted vs non\-encrypted\) |  |  | 2c |  |  |  |  |
| 1261 |  |  |  | Network | Networking Concepts | Packet Capture \(PCAP\) based Intrusion Detection System \(IDS\) Analysis |  |  | B |  |  |  |  |
| 1262 |  |  |  | Programming | Reverse Engineering | IDA Extensions |  |  | A |  |  |  |  |
| 1263 |  |  |  | Programming | Reverse Engineering | IDA Scripting via Python |  |  | A |  |  |  |  |
| 1264 |  |  |  | Programming | Reverse Engineering | Advanced IDA Features |  |  | A |  |  |  |  |
| 1265 |  |  |  | Software | Reverse Engineering | WinDbg Dynamic Analysis |  |  | 2c |  |  |  |  |
| 1266 |  |  |  | Programming | Reverse Engineering | Disassemble Binary |  |  | 2c |  |  |  |  |
| 1267 |  |  |  | Software | Reverse Engineering | Static Analysis |  |  | 2b |  |  |  |  |
| 1268 |  |  |  | Software | Reverse Engineering | Dynamic Analysis |  |  | 2b |  |  |  |  |
| 1269 |  |  |  | Software | Reverse Engineering | Areas of Interest |  |  | 2b |  |  |  |  |
| 1270 |  |  |  | Programming | Reverse Engineering | Disassembly and C Programming |  |  | 2b |  |  |  |  |
| 1271 |  |  |  | Software | Reverse Engineering | Combine Dynamic and Static Analysis |  |  | 2b |  |  |  |  |
| 1272 |  |  |  | Software | Reverse Engineering | Static/Dynamic Tools |  |  | 2b |  |  |  |  |
| 1273 |  |  |  | Software | Hacking Methodologies | Hacking operating systems and software |  |  | A |  |  |  |  |
| 1274 |  |  |  | Software | Hacking Methodologies | Hacking fuzzing fundamentals |  |  | A |  |  |  |  |
| 1293 |  |  |  | Network | Networking Concepts | Basic Networking | Ports and Protocols |  | B |  |  |  |  |
| 1294 |  |  |  | Network | Networking Concepts | Basic Networking | Address Resolution Protocol \(ARP\) |  | B |  |  |  |  |
| 1295 |  |  |  | Network | Networking Concepts | Basic Networking | Hypertext Transfer Protocol/Secure \(HTTP/HTTPS\) |  | B |  |  |  |  |
| 1296 |  |  |  | Network | Networking Concepts | Basic Networking | Domain Name System \(DNS\) |  | B |  |  |  |  |
| 1297 |  |  |  | Network | Networking Concepts | Basic Networking | Simple Mail Transfer Protocol \(SMTP\) |  | B |  |  |  |  |
| 1299 |  |  |  | Network | Networking Concepts | Sockets | Intro to Sockets |  | 3c |  |  |  |  |
| 1300 |  |  |  | Network | Networking Concepts | String\-based Protocol |  |  | 2b |  |  |  |  |
| 1301 |  |  |  | Network | Networking Concepts | Bitwise Protocol |  |  | 2b |  |  |  |  |
| 1302 |  |  |  | Network | Networking Concepts | Basic Networking | Internal Control Message Protocol \(ICMP\) |  | B |  |  |  |  |
| 1303 |  |  |  | Network | Networking Concepts | Basic Networking | Dynamic Host Configuration Protocol \(DHCP\) |  | B |  |  |  |  |
| 1304 |  |  |  | Network | Networking Concepts | Basic Networking | Reading Request For Comments \(RFC\) |  | C |  |  |  |  |
| 1305 |  |  |  | Network | Networking Concepts | POSIX API/BSD Sockets |  |  | C |  |  |  |  |
| 1306 |  |  |  | Network | Networking Concepts | C Socket Programming \(move to apprentice?\) |  |  | C |  |  |  |  |
| 1307 |  |  |  | Network | Networking Concepts | Advanced Networking | Python Socket Programming |  | C |  |  |  |  |
| 1308 |  |  |  | Network | Networking Concepts | OSI Layer 3 | Subnets |  | C |  |  |  |  |
| 1309 |  |  |  | Network | Networking Concepts | OSI Layer 3 | IPv4 Addressing |  | C |  |  |  |  |
| 1310 |  |  |  | Network | Networking Concepts | OSI Layer 3 | IPv6 Addressing |  | C |  |  |  |  |
| 1311 |  |  |  | Project | Agile Development | Overview |  |  | B |  |  |  |  |
| 1312 |  |  |  | Project | Agile Development | Agile Development Roles |  |  | B |  |  |  |  |
| 1313 |  |  |  | Project | Agile Development | Scrum Event Distinction and Process Order |  |  | B |  |  |  |  |
| 1314 |  |  |  | Project | Agile Development | Product Backlog Grooming |  |  | B |  |  |  |  |
| 1315 |  |  |  | Project | Agile Development | Daily Scrum Meeting |  |  | B |  |  |  |  |
| 1316 |  |  |  | Project | Agile Development | Scrum Advantages to Rapid Capability Development |  |  | B |  |  |  |  |
| 1317 |  |  |  | Project | Agile Development | Unit Testing |  |  | B |  |  |  |  |
| 1318 |  |  |  | Project | Agile Development | Size and Complexity of Functional Test Items |  |  | B |  |  |  |  |
| 1319 |  |  |  | Project | Agile Development | User Story Development |  |  | B |  |  |  |  |
| 1321 |  |  |  | Project | Technical Writing | Writing Executive Summaries |  |  | A |  |  |  |  |
| 1322 |  |  |  | Project | Technical Writing | Edit Executive Summaries |  |  | A |  |  |  |  |
| 1323 |  |  |  | Project | Technical Writing | Writing User Guides |  |  | A |  |  |  |  |
| 1324 |  |  |  | Project | Technical Writing | Edit User Guides |  |  | A |  |  |  |  |
| 1325 |  |  |  | Project | Technical Writing | Writing Analysis Reports |  |  | A |  |  |  |  |
| 1326 |  |  |  | Project | Technical Writing | Reading Capability Requirements Specifications |  |  | A |  |  |  |  |
| 1327 |  |  |  | Project | Technical Writing | Writing Capability Requirements Specifications |  |  | A |  |  |  |  |
| 1328 |  |  |  | Project | Technical Writing | Edit Capability Requirements Specifications |  |  | A |  |  |  |  |
| 1329 |  |  |  | Project | Technical Writing | Writing Statement of Requirements |  |  | A |  |  |  |  |
| 1330 |  |  |  | Project | Technical Writing | Edit Statement of Requirements |  |  | A |  |  |  |  |
| 1331 |  |  |  | Project | Technical Writing | Writing Test Plan/Report |  |  | A |  |  |  |  |
| 1336 |  |  |  | Software | Software Development Process | Overview |  |  | B |  |  |  |  |
| 1337 |  |  |  | Software | Software Development Process | Version Control |  |  | B |  |  |  |  |
| 1338 |  |  |  | Software | Software Development Process | Best Practices |  |  | B |  |  |  |  |
| 1339 |  |  |  | Software | Software Development Process | Peer Review |  |  | 2b |  |  |  |  |
| 1340 |  |  |  | Software | Software Development Process | Integrated Development Environments |  |  | B |  |  |  |  |
| 1341 |  |  |  | Software | Software Development Process | Debugging Tools |  |  | A |  |  |  |  |
| 1403 | JCT&CS | T001 | USCYBERCOM/J7 | Cyber Capabilities | Mission | \(U\) Modify cyber capabilites to detect and disrupt nation\-state cyber threat actors across the threat chain \(kill chain\) of adversary execution |  |  | 2 |  |  |  |  |
| 1404 | JCT&CS | T002 | USCYBERCOM/J7 | Cyber Capabilities | Mission | \(U\) Create or enhance cyberspace capabilities to compromise, deny, degrade, disrupt, destroy, or manipulate automated information systems\. |  |  | 2 |  |  |  |  |
| 1405 | JCT&CS | T003 | USCYBERCOM/J7 | Cyber Capabilities | Mission | \(U\) Create or enhance cyberspace solutions to enable surveillance and reconnaissance of automated information systems\. |  |  | 2 |  |  |  |  |
| 1406 | JCT&CS | T004 | USCYBERCOM/J7 | Cyber Capabilities | Mission | \(U\) Create or enhance cyberspace solutions to enable the defense of national interests\. |  |  | 2 |  |  |  |  |
| 1407 | JCT&CS | T005 | USCYBERCOM/J7 | Project | Agile | \(U\) Interpret customer requirements and evaluate resource and system constraints to create solution design specifications\. |  |  | 2 |  |  |  |  |
| 1408 | JCT&CS | T006 | USCYBERCOM/J7 | Project | Agile | \(U\) Reference capability repositories and other sources to identify existing capabilities which fully/partially meet customer requirements \(with or without modification\)\. |  |  | 2 |  |  |  |  |
| 1409 | JCT&CS | T007 | USCYBERCOM/J7 | Project | Agile | \(U\) Collaborate with stakeholders to identify and/or develop appropriate cyberspace solutions\. |  |  | 2 |  |  |  |  |
| 1410 | JCT&CS | T008 | USCYBERCOM/J7 | Programming | Kernel Development | \(U\) Analyze, modify, develop, debug and document software and applications which run in kernel and user space\. |  |  | 2 |  |  |  |  |
| 1411 | JCT&CS | T009 | USCYBERCOM/J7 | Programming | C | \(U\) Analyze, modify, develop, debug and document software and applications in C programming language\. |  |  | 3 |  |  |  |  |
| 1412 | JCT&CS | T010 | USCYBERCOM/J7 | Programming | Python | \(U\) Analyze, modify, develop, debug and document software and applications in Python programming language\. |  |  | 3 |  |  |  |  |
| 1413 | JCT&CS | T011 | USCYBERCOM/J7 | Network | Network Analysis/Programming | \(U\) Analyze, modify, develop, debug and document software and applications utilizing standard, non\-standard, specialized, and/or unique network communication protocols\. |  |  | 2 |  |  |  |  |
| 1414 | JCT&CS | T012 | USCYBERCOM/J7 | Programming | Software Engineering | \(U\) Analyze, modify, develop, debug and document custom interface specifications between interconnected systems\. |  |  | 2 |  |  |  |  |
| 1415 | JCT&CS | T013 | USCYBERCOM/J7 | Project | Version Control | \(U\) Manage source code using version control \(e\.g\. Git\) |  |  | 3 |  |  |  |  |
| 1416 | JCT&CS | T014 | USCYBERCOM/J7 | Programming | DevOps | \(U\) Develop, modify, and utilize automation technologies to enable employment of capabilities as efficiently as possible \(e\.g\. TDD, CI/CD, etc\.\) |  |  | 2 |  |  |  |  |
| 1417 | JCT&CS | T015 | USCYBERCOM/J7 | Software | Software Testing | \(U\) Build, test, and modify prototypes using representative environments\. |  |  | 2 |  |  |  |  |
| 1418 | JCT&CS | T016 | USCYBERCOM/J7 | Software | Software Testing | \(U\) Analyze and correct technical problems encountered during testing and implementation of cyberspace solutions\. |  |  | 2 |  |  |  |  |
| 1419 | JCT&CS | T017 | USCYBERCOM/J7 | Programming | Assembly Programming | \(U\) Analyze, modify, develop, debug and document software and applications using assembly languages\. |  |  | 2 |  |  |  |  |
| 1420 | JCT&CS | T018 | USCYBERCOM/J7 | Software | Reverse Engineering | \(U\) Utilize tools to decompile, disassembe, analzye, and reverse engineer compiled binaries\. |  |  | 2 |  |  |  |  |
| 1421 | JCT&CS | T019 | USCYBERCOM/J7 | Programming | Programming | \(U\) Make use of compiler attributes and platform\-specific features\. |  |  | 1 |  |  |  |  |
| 1422 | JCT&CS | T020 | USCYBERCOM/J7 | Programming | Debugging | \(U\) Perform static and dynamic analysis in order to find errors and flaws\. |  |  | 2 |  |  |  |  |
| 1423 | JCT&CS | T021 | USCYBERCOM/J7 | Security | Reverse Engineering | \(U\) Conduct hardware and/or software static and dynamic analysis to reverse engineer malicious or benign systems |  |  | 2 |  |  |  |  |
| 1424 | JCT&CS | T022 | USCYBERCOM/J7 | Security | Vulnerability Research | \(U\) Research, reference, discover, analyze, modify, develop and document known and new vulnerabilities in computer systems\. |  |  | 1 |  |  |  |  |
| 1425 | JCT&CS | T023 | USCYBERCOM/J7 | Programming | Software Engineering | \(U\) Design and develop data requirements, database structure, process flow, systematic procedures, algorithms, data analysis, and file structures\. |  |  | 2 |  |  |  |  |
| 1426 | JCT&CS | T024 | USCYBERCOM/J7 | Programming | Software Engineering | \(U\) Utilize data structures to organize, sort, and manipulate elements of information |  |  | 3 |  |  |  |  |
| 1427 | JCT&CS | T025 | USCYBERCOM/J7 | Programming | Software Engineering | \(U\) Design and develop user interfaces \(e\.g\. web pages, GUIs, CLIs, Console Interfaces\) |  |  | 2 |  |  |  |  |
| 1428 | JCT&CS | T026 | USCYBERCOM/J7 | Programming | Software Engineering | \(U\) Utilize secure coding techniques during development of software and applications |  |  | 2 |  |  |  |  |
| 1429 | JCT&CS | T027 | USCYBERCOM/J7 | Security | Implement Security Measures | \(U\) Employ tradecraft to provide confidentiality, integrity, availability, authentication, and non\-repudiation of developed accesses, payloads and tools\. |  |  | 2 |  |  |  |  |
| 1430 | JCT&CS | T028 | USCYBERCOM/J7 | Security | Implement Security Measures | \(U\) Apply cryptography primitives to protect the confidentiality and integrity of sensitive data\. |  |  | 2 |  |  |  |  |
| 1431 | JCT&CS | T029 | USCYBERCOM/J7 | Programming | Secure Coding | \(U\) Perform code review and analysis to inform OPSEC analysis and application \(attribution, sanitization, etc\.\) |  |  | 2 |  |  |  |  |
| 1432 | JCT&CS | T030 | USCYBERCOM/J7 | Programming | Secure Coding | \(U\) Produce artifacts to inform risk analysis, acceptance testing, and legal review\. |  |  | 2 |  |  |  |  |
| 1433 | JCT&CS | T031 | USCYBERCOM/J7 | Standardization | Standards | \(U\) Locate and utilize technical specifications and industry standards \(e\.g\. Internet Engineering Task Force \(IETF\), IEEE, IEC, International Standards Organization \(ISO\)\)\. |  |  | 2 |  |  |  |  |
| 1434 | JCT&CS | T032 | USCYBERCOM/J7 | Standardization | Technical Writing | \(U\) Deliver technical documentation \(e\.g\. user guides, design documentation, test plans, change logs, training materials, etc\.\) to end users\. |  |  | 2 |  |  |  |  |
| 1435 | JCT&CS | T033 | USCYBERCOM/J7 | Standardization | Project Management | \(U\) Implement project management, software engineering philosophies, modern capability development methodologies \(Agile, TDD, CI/CD, etc\) |  |  | 2 |  |  |  |  |
| 1436 | JCT&CS | T034 | USCYBERCOM/J7 | Standardization | Software Engineering | \(U\) Apply software engineering best practices to enable sustainability and extesibility\. |  |  | 2 |  |  |  |  |
| 1437 | JCT&CS | T035 | USCYBERCOM/J7 | Standardization | Project Management Tools | \(U\) Enter work into Task and project management tools used for software development \(e\.g\. Jira, Confluence, Trac, MediaWiki, etc\.\) |  |  | 2 |  |  |  |  |
| 1438 | JCT&CS | T036 | USCYBERCOM/J7 | Cyber Capabilities | Software Engineering | \(U\) Enhance capability design strategies and tactics by synthesizing information, processes, and techniques in the areas of malicious software, vulnerabilities, reverse engineering, secure software engineering, and exploitation\. |  |  | 2 |  |  |  |  |
| 1439 | JCT&CS | T037 | USCYBERCOM/J7 | Standardization | Lessons Learned | \(U\) Document and communicate tradecraft, best practices, TTPs, training, briefings, presentations, papers, studies, lessons learned, etc\. to both technical and non\-technical audiences |  |  | 2 |  |  |  |  |
| 1440 | JCT&CS | KSA1 | USCYBERCOM/J7 | Organization | USCC Basic Info | \(U\) Explain US Cyber Command  authorities, responsibilities, and contributions to the National Cyber Mission\. |  |  | C |  |  |  |  |
| 1441 | JCT&CS | KSA2 | USCYBERCOM/J7 | Organization | Organizational Partner Info | \(U\) Describe mission and organizational partners, including information needs, mission, structure, capabilities, etc\. |  |  | C |  |  |  |  |
| 1442 | JCT&CS | KSA3 | USCYBERCOM/J7 | Organization | Cyber Mission Forces | \(U\) Describe cyber mission force equipment taxonomy \(Platform\-Access\-Payloads/Toolset\), capability development process and repository |  |  | B |  |  |  |  |
| 1443 | JCT&CS | KSA4 | USCYBERCOM/J7 | Organization | USCC Basic Info | \(U\) Describe USCYBERCOM legal, policy, and compliance requirements relevant to capability development\. |  |  | B |  |  |  |  |
| 1444 | JCT&CS | KSA5 | USCYBERCOM/J7 | Organization | Threats & Vulnerabilities | \(U\) Identify cyber adversary threat tier taxonomy \(2014 National Intellegence Estimate \(NIE\)\), DIA/NSA Standard Cyber Threat Model, etc\.\) |  |  | A |  |  |  |  |
| 1445 | JCT&CS | KSA6 | USCYBERCOM/J7 | Organization | USCC Basic Info | \(U\) Describe sources and locations of cyber capability registries and repositories \(E\.g\. Cyber Capability Registry \(CCR\), Agency and service repositories, etc\.\) |  |  | B |  |  |  |  |
| 1446 | JCT&CS | KSA7 | USCYBERCOM/J7 | Organization | USCC Basic Info | \(U\) Describe sources and locations \(public and classified\) of capability development TTPs and tradecraft information/intelligence used by the US Gov and others\. |  |  | B |  |  |  |  |
| 1447 | JCT&CS | KSA8 | USCYBERCOM/J7 | Network | Networking Concepts | \(U\) Describe Network Classes and their key characteristics, and common payload & access approaches \(T1, T2, T3\)  \(General Purpose, Telepresence, Commercial Mobile, Control Systems, Battlefield Systems\) |  |  | A |  |  |  |  |
| 1448 | JCT&CS | KSA9 | USCYBERCOM/J7 | Organization | Supported Organization Processes | \(U\) Describe supported organizations requirements processes\. |  |  | C |  |  |  |  |
| 1449 | JCT&CS | KSA10 | USCYBERCOM/J7 | Organization | Supported Organization Processes | \(U\) Describe the supported organization's approval process for operational use of a capability\. |  |  | C |  |  |  |  |
| 1450 | JCT&CS | KSA11 | USCYBERCOM/J7 | Organization | Mission | \(U\) Describe relevant mission processes including version control processes, release processes, documentation requirements, and testing requirements\. |  |  | C |  |  |  |  |
| 1451 | JCT&CS | KSA12 | USCYBERCOM/J7 | Programming | Software Engineering | \(U\) Explain how to apply modern software engineering practices\. |  |  | C |  |  |  |  |
| 1452 | JCT&CS | KSA13 | USCYBERCOM/J7 | Programming | Software Development Process | \(U\) Describe modern software development methodologies \(e\.g\. Continuous Integration \(CI\), Continuous Delivery \(CD\), Test Driven Development \(TDD\), etc\.\) |  |  | C |  |  |  |  |
| 1453 | JCT&CS | KSA14 | USCYBERCOM/J7 | Organization | Project Management | \(U\) Explain your organizations project management, timeline estimation, and software engineering philosophy \(e\.g\. CI/CD, TDD, etc\.\) |  |  | C |  |  |  |  |
| 1454 | JCT&CS | KSA15 | USCYBERCOM/J7 | Standardization | Software Testing | \(U\) Describe principles, methodologies, and tools used to improve quality of software \(e\.g\. regression testing, test coverage, code review, pair programming, etc\.\) |  |  | B |  |  |  |  |
| 1455 | JCT&CS | KSA16 | USCYBERCOM/J7 | Software | OS | \(U\) Explain terms and concepts of operating system fundamentals \(e\.g\. File systems, I/O, Memory Management, Process Abstraction, etc\.\) |  |  | C |  |  |  |  |
| 1456 | JCT&CS | KSA17 | USCYBERCOM/J7 | Programming | Software Engineering Fundamentals | \(U\) Explain basic programming concepts \(e\.g\., levels, structures, compiled vs\. interpreted languages\)\. |  |  | C |  |  |  |  |
| 1457 | JCT&CS | KSA18 | USCYBERCOM/J7 | Programming | Software Engineering Fundamentals | \(U\) Explain procedural and object\-oriented programming paradigms\. |  |  | C |  |  |  |  |
| 1458 | JCT&CS | KSA19 | USCYBERCOM/J7 | Programming | Standards | \(U\) Explain how to utilize reference documentation for C, Python, assembly, and other international technical standards and specifications \(IEEE, ISO, IETF, etc\.\) |  |  | C |  |  |  |  |
| 1459 | JCT&CS | KSA20 | USCYBERCOM/J7 | Programming | C | \(U\) Explain the purpose and usage of elements and components of the C programming language |  |  | C |  |  |  |  |
| 1460 | JCT&CS | KSA21 | USCYBERCOM/J7 | Programming | Python | \(U\) Explain the purpose and usage of elements and components of the Python programming language |  |  | C |  |  |  |  |
| 1461 | JCT&CS | KSA22 | USCYBERCOM/J7 | Programming | Assembly Programming | \(U\) Explain the purpose and usage of elements of Assembly language \(e\.g\., x86 and x86\_64, ARM, PowerPC\)\. |  |  | C |  |  |  |  |
| 1462 | JCT&CS | KSA23 | USCYBERCOM/J7 | Programming | Software Engineering | \(U\) Explain the use and application of static and dynamic program analysis\. |  |  | C |  |  |  |  |
| 1463 | JCT&CS | KSA24 | USCYBERCOM/J7 | Security | Reverse Engineering | \(U\) Explain what it is to decompile, disassembe, analzye, and reverse engineer compiled binaries\. |  |  | B |  |  |  |  |
| 1464 | JCT&CS | KSA25 | USCYBERCOM/J7 | Security | Reverse Engineering | \(U\) Identify commonly used tools to decompile, disassembe, analzye, and reverse engineer compiled binaries\. |  |  | B |  |  |  |  |
| 1465 | JCT&CS | KSA26 | USCYBERCOM/J7 | Programming | Software Engineering | \(U\) Explain software optimization techniques\. |  |  | C |  |  |  |  |
| 1466 | JCT&CS | KSA27 | USCYBERCOM/J7 | Network | Networking Concepts | \(U\) Explain terms and concepts of networking protocols \(e\.g\. Ethernet, IP, DHCP, ICMP, SMTP, DNS, TCP/OSI, etc\.\) |  |  | C |  |  |  |  |
| 1467 | JCT&CS | KSA28 | USCYBERCOM/J7 | Network | Networking Concepts | \(U\) Describe and explain physical and logical network infrastructure, to include hubs, switches, routers, firewalls, etc\. |  |  | C |  |  |  |  |
| 1468 | JCT&CS | KSA29 | USCYBERCOM/J7 | Network | Networking Concepts | \(U\) Explain network defense mechanisms\. \(e\.g\., encryption, authentication, detection, perimeter protection\)\. |  |  | B |  |  |  |  |
| 1469 | JCT&CS | KSA30 | USCYBERCOM/J7 | Software | Data Serialization | \(U\) Explain data serialization formats \(e\.g\. XML, JSON, etc\.\) |  |  | B |  |  |  |  |
| 1470 | JCT&CS | KSA31 | USCYBERCOM/J7 | Programming | Data Structures | \(U\) Explain the concepts and terminology of datastructures and associated algorithms \(e\.g\., search, sort, traverse, insert, delete\)\. |  |  | C |  |  |  |  |
| 1471 | JCT&CS | KSA32 | USCYBERCOM/J7 | Programming | Secure Coding | \(U\) Explain terms and concepts of secure coding practices\. |  |  | C |  |  |  |  |
| 1472 | JCT&CS | KSA33 | USCYBERCOM/J7 | Security | Network Security | \(U\) Describe capability OPSEC analysis and application \(attribution, sanitization, etc\.\) |  |  | B |  |  |  |  |
| 1473 | JCT&CS | KSA34 | USCYBERCOM/J7 | Security | Network Security | \(U\) Explain standard, non\-standard, specialized, and/or unique communication techniques used to provide confidentiality, integrity, availability, authentication, and non\-repudiation \. |  |  | C |  |  |  |  |
| 1474 | JCT&CS | KSA35 | USCYBERCOM/J7 | Security | Cryptography | \(U\) Describe common pitfalls surrounding the use, design, and implementation of cryptographic facilities\. |  |  | B |  |  |  |  |
| 1475 | JCT&CS | KSA36 | USCYBERCOM/J7 | Security | Cryptography | \(U\) Demonstrate understanding of pervasive cryptographic primitives\. |  |  | C |  |  |  |  |
| 1476 | JCT&CS | KSA37 | USCYBERCOM/J7 | Project | Project Management | \(U\) Explain Task and project management tools used for software development \(e\.g\. Jira, Confluence, Trac, MediaWiki, etc\.\) |  |  | B |  |  |  |  |
| 1477 | JCT&CS | KSA38 | USCYBERCOM/J7 | Organization | USCC Basic Info | \(U\) Identify cyber development conferences used to share development and community of interest information \(e\.g\. CNEDev, MTEM, etc\.\) |  |  | B |  |  |  |  |
