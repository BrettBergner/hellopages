# SEE Table

Note: Use left/right arrows to scroll the table left/right.

| UID | Req Src | Req Src ID | Requirement Owner\(s\) | Category | Topic/Language/Tool/Application | TASK | SUB TASK | SEE Crit Task | SEE | COMMENTS |  |  |  |
| :--- | :--- | :--- | :--- | :--- | :--- | :--- | :--- | :--- | :--- | :--- | :--- | :--- | :--- |
| 1376 | ACCI 17\-202v2 |  |  | SEE | Stan/Eval | Implement directives pertaining to the administration of an evaluation | Ensure Examiner Go/No\-Go requirements have been met |  | 3c |  |  |  |  |
| 1377 | ACCI 17\-202v2 |  |  | SEE | Stan/Eval | Implement directives pertaining to the administration of an evaluation | Ensure Examinee Go/No\-Go requirements have been met |  | 3c |  |  |  |  |
| 1378 | ACCI 17\-202v2 |  |  | SEE | Stan/Eval | Implement directives pertaining to the administration of an evaluation | Obtain all required evaluation material  |  | 3c |  |  |  |  |
| 1379 | ACCI 17\-202v2 |  |  | SEE | Stan/Eval | Conduct examiner's briefing | Brief the overall conduct of the evaluation to include when the evaluation begins/ends |  | 3c |  |  |  |  |
| 1380 | ACCI 17\-202v2 |  |  | SEE | Stan/Eval | Conduct examiner's briefing | Brief scenario requirements to include Go/No\-Go scenario |  | 3c |  |  |  |  |
| 1381 | ACCI 17\-202v2 |  |  | SEE | Stan/Eval | Conduct examiner's briefing | Brief grading criteria and overall grade definitions |  | 3c |  |  |  |  |
| 1382 | ACCI 17\-202v2 |  |  | SEE | Stan/Eval | Conduct examiner's briefing | Brief examiner/examinee evaluation roles and responsibilities |  | 3c |  |  |  |  |
| 1383 | ACCI 17\-202v2 |  |  | SEE | Stan/Eval | Identify Discrepancies in evaluation  | Identify and documents all discrepancies in the appropriate evaluation area |  | 3c |  |  |  |  |
| 1384 | ACCI 17\-202v2 |  |  | SEE | Stan/Eval | Assign appropriate  Area Grades during evaluation | Assign proper area grades based on discrepancy documentation and criteria guidance |  | 3c |  |  |  |  |
| 1385 | ACCI 17\-202v2 |  |  | SEE | Stan/Eval | Assess overall evaluation performance | Factor all cumulative deviations in the examinee's performance when assigning overall grade |  | 3c |  |  |  |  |
| 1386 | ACCI 17\-202v2 |  |  | SEE | Stan/Eval | Assess overall evaluation performance | Award the appropriate overall grade based on criteria guidance and governing guidance |  | 3c |  |  |  |  |
| 1387 | ACCI 17\-202v2 |  |  | SEE | Stan/Eval | Assign additional training when required | Assign additional training for discrepancies where a debrief is not the proper remedial action\. |  | 3c |  |  |  |  |
| 1388 | ACCI 17\-202v2 |  |  | SEE | Stan/Eval | Assign additional training when required | Assigned sufficient additional training that will ensure examinee would achieve proper qualification level |  | 3c |  |  |  |  |
| 1389 | ACCI 17\-202v2 |  |  | SEE | Stan/Eval | Debrief Evaluation | Debrief a summary of overall evaluation to include the scenario and examinee's tasks supporting the scenario |  | 3c |  |  |  |  |
| 1390 | ACCI 17\-202v2 |  |  | SEE | Stan/Eval | Debrief Evaluation | Debrief all key scenario events,  providing instruction and references as required |  | 3c |  |  |  |  |
| 1391 | ACCI 17\-202v2 |  |  | SEE | Stan/Eval | Debrief Evaluation | Reconstruct key scenario events and examinee's role in each event |  | 3c |  |  |  |  |
| 1392 | ACCI 17\-202v2 |  |  | SEE | Stan/Eval | Debrief Evaluation | Debriefed all discrepancies to include correct actions of each discrepancy |  | 3c |  |  |  |  |
| 1393 | ACCI 17\-202v2 |  |  | SEE | Stan/Eval | Debrief Evaluation | Debrief each evaluation area grade and, if warranted,  additional training to include additional training due date |  | 3c |  |  |  |  |
| 1394 | ACCI 17\-202v2 |  |  | SEE | Stan/Eval | Debrief Evaluation | Debrief overall evaluation grade |  | 3c |  |  |  |  |
| 1395 | ACCI 17\-202v2 |  |  | SEE | Stan/Eval | Document Evaluation | Correctly complete evaluation gradesheet prior to debrief |  | 3c |  |  |  |  |
| 1396 | ACCI 17\-202v2 |  |  | SEE | Stan/Eval | Document Evaluation | Verify accuracy/completion of AF Form 4418 prior to debrief |  | 3c |  |  |  |  |
| 1397 | ACCI 17\-202v2 |  |  | SEE | Stan/Eval | Brief the Supervisor on the Evaluation \(Q2 & Q3 only\) | Debrief supervisor of examinee's performance to include discrepancies,  remedial action,  area/overall grades,  and additional training \(including due date\) |  | 3c |  |  |  |  |
| 1398 | ACCI 17\-202v2 |  |  | SEE | Stan/Eval | Perform Evaluation | Conduct evaluation as briefed to the examinee |  | 3c |  |  |  |  |
| 1399 | ACCI 17\-202v2 |  |  | SEE | Stan/Eval | Perform Evaluation | Does not detract or disrupt the examinee's performance during the evaluation |  | 3c |  |  |  |  |
| 1400 | ACCI 17\-202v2 |  |  | SEE | Stan/Eval | Perform Evaluation | Evaluate all criteria areas |  | 3c |  |  |  |  |
| 1401 | ACCI 17\-202v2 |  |  | SEE | Stan/Eval | Perform Evaluation | Administer task scenario as required\. |  | 3c |  |  |  |  |
| 1402 | ACCI 17\-202v2 |  |  | SEE | Stan/Eval | Perform Evaluation | If applicable,  take over examinee position if/when examinee performance exhibited breaches of safety |  | 3c |  |  |  |  |
