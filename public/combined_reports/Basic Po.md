# Basic PO Table

Note: Use left/right arrows to scroll the table left/right.

| UID | Req Src | Req Src ID | Requirement Owner\(s\) | Category | Topic/Language/Tool/Application | TASK | SUB TASK | Basic PO Crit Task | Basic PO | COMMENTS |  |  |  |
| :--- | :--- | :--- | :--- | :--- | :--- | :--- | :--- | :--- | :--- | :--- | :--- | :--- | :--- |
| 852 |  |  |  | Project | Agile | Fundamentals of the Product Owner Role | Describe the responsibilities of the Product Owner\. |  | A |  |  |  |  |
| 853 |  |  |  | Project | Agile | Fundamentals of the Product Owner Role | Distinguish between the responsibilities of the Product Owner and the rest of the scrum team\. |  | B |  |  |  |  |
| 854 |  |  |  | Project | Agile | Fundamentals of the Product Owner Role | Describe two benefits of mastering the role\. |  | B |  |  |  |  |
| 855 |  |  |  | Project | Agile | Fundamentals of the Product Owner Role | Identify the impact on a Scrum Team and organization anti\-patterns that might exist for Product Owners\. |  | B |  |  |  |  |
| 856 |  |  |  | Project | Agile | Fundamentals of the Product Owner Role | Describe at least three types of organizational contexts that affect the approach to the  Owner role\. |  | B |  |  |  |  |
| 859 |  |  |  | Project | Agile | Working with Stakeholders | Demonstrate at least one technique to provide transparency to stakeholders on progress toward goals\. |  | 2b |  |  |  |  |
| 860 |  |  |  | Project | Agile | Working with Stakeholders | Describe at least three techniques to engage stakeholders to gather information or insights\. |  | B |  |  |  |  |
| 861 |  |  |  | Project | Agile | Working with Stakeholders | List at least three different decision\-making approaches a Product Owner might use\. |  | A |  |  |  |  |
| 862 |  |  |  | Project | Agile | Working with Stakeholders | Describe at least two situations where the Product Owner might act as a neutral facilitator and when they might use a different engagement approach\. |  | B |  |  |  |  |
| 863 |  |  |  | Project | Agile | Working with Stakeholders | Identify at least four major stakeholder groups\. |  | A |  |  |  |  |
| 866 |  |  |  | Project | Agile | Working with Stakeholders | Using two concrete examples, recognize when the Product Owner should not act as the facilitator for the stakeholders\. |  | A |  |  |  |  |
| 868 |  |  |  | Project | Agile | Working with Stakeholders | List at least three alternatives to open discussion\. |  | A |  |  |  |  |
| 869 |  |  |  | Project | Agile | Working with Stakeholders | Demonstrate one alternative to open discussion\. |  | A |  |  |  |  |
| 873 |  |  |  | Project | Agile | Working with the Development Team | Describe how the Product Owner collaborates with the Development Team for activities such as defining "Done" and backlog creation, refinement, and ordering\. |  | B |  |  |  |  |
| 874 |  |  |  | Project | Agile | Working with the Development Team | Define technical debt and explain why the Product Owner should be cautious about accumulating technical debt\. |  | A |  |  |  |  |
| 877 |  |  |  | Project | Agile | Product Ownership with Multiple Teams | List at least three challenges of being a Product Owner with multiple teams\. |  | A |  |  |  |  |
| 881 |  |  |  | Project | Agile | Product Strategy | Describe vision and practice the creation of a product vision\. |  | B |  |  |  |  |
| 882 |  |  |  | Project | Agile | Product Strategy | Describe strategy and relate it to mission and goals\. |  | B |  |  |  |  |
| 883 |  |  |  | Project | Agile | Product Planning and Forecasting | Describe at least three different strategies for the incremental delivery of a product\. |  | B |  |  |  |  |
| 884 |  |  |  | Project | Agile | Product Planning and Forecasting | Explain at least three techniques to plan product delivery over time\. |  | B |  |  |  |  |
| 889 |  |  |  | Project | Agile | Understanding Customers and Users | Describe why a Product Owner performs discovery and validation work\. |  | C |  |  |  |  |
| 890 |  |  |  | Project | Agile | Understanding Customers and Users | Illustrate at least one approach for segmenting customers and users\. |  | B |  |  |  |  |
| 891 |  |  |  | Project | Agile | Understanding Customers and Users | Demonstrate at least one technique to prioritize between conflicting customer \(or user\) needs\. |  | 2b |  |  |  |  |
| 892 |  |  |  | Project | Agile | Understanding Customers and Users | Describe at least three aspects of product discovery and identify how each contributes to successful product outcomes\. |  | B |  |  |  |  |
| 893 |  |  |  | Project | Agile | Understanding Customers and Users | Demonstrate one technique to describe users and customers, their jobs, activities, pains, and gains\. |  | 2b |  |  |  |  |
| 894 |  |  |  | Project | Agile | Understanding Customers and Users | List at least three ways to connect the Development Team directly to customers and users\. |  | A |  |  |  |  |
| 895 |  |  |  | Project | Agile | Understanding Customers and Users | Describe at least three benefits of Development Team direct interactions\. |  | B |  |  |  |  |
| 899 |  |  |  | Project | Agile | Validating Product Assumptions | Describe how Scrum supports validating product assumptions\. |  | B |  |  |  |  |
| 900 |  |  |  | Project | Agile | Validating Product Assumptions | List at least three approaches to validating assumptions by their cost and the quality of learning\. |  | A |  |  |  |  |
| 901 |  |  |  | Project | Agile | Validating Product Assumptions | Describe at least one approach to choosing which assumption should be validated first\. |  | B |  |  |  |  |
| 907 |  |  |  | Project | Agile | Differentiating Outcome and Output | Describe the relationship between outcome and output\. |  | B |  |  |  |  |
| 908 |  |  |  | Project | Agile | Differentiating Outcome and Output | Describe at least three attributes of a Product Backlog item that help assess maximizing outcome\. |  | B |  |  |  |  |
| 909 |  |  |  | Project | Agile | Defining Value | Define value and list at least two techniques to measure value\. |  | A |  |  |  |  |
| 910 |  |  |  | Project | Agile | Defining Value | Describe value from the perspective of at least three different stakeholder groups\. |  | B |  |  |  |  |
| 911 |  |  |  | Project | Agile | Defining Value | List at least five terms and definitions related to product economics that contribute to financial success\. |  | A |  |  |  |  |
| 912 |  |  |  | Project | Agile | Ordering Items | Describe at least three criteria for ordering the Product Backlog\. |  | B |  |  |  |  |
| 913 |  |  |  | Project | Agile | Creating and Refining Items | Identify at least three sources of Product Backlog items\. |  | A |  |  |  |  |
| 914 |  |  |  | Project | Agile | Creating and Refining Items | Describe at least three techniques for generating new Product Backlog items  |  | B |  |  |  |  |
| 915 |  |  |  | Project | Agile | Creating and Refining Items | Create one Product Backlog item that includes description of desired outcome and value\. |  | 2b |  |  |  |  |
| 916 |  |  |  | Project | Agile | Creating and Refining Items | Explain the pros and cons of a "just\-in\-time" approach for Product Backlog refinement vs\. an "all\-at\-once" approach\. |  | B |  |  |  |  |
| 917 |  |  |  | Project | Agile | Creating and Refining Items | Use at least three tools to communicate the purpose and intent of Product Backlog items<br>to improve the Development Team's shared understanding\. |  | 2b |  |  |  |  |
| 918 |  |  |  | Project | Agile | Creating and Refining Items | Explain at least two approaches to identify small, valuable, and releasable subsets of a big idea or feature\. |  | B |  |  |  |  |
| 935 |  |  |  | Project | 90 MQT | Project in Progress | Conduct an effective Product Backlog Grooming Meeting in accordance with squadron standards\. |  | B | http://mpls\-as\-701v:8090/display/9CMDP/%28U%29\+Guidance\+for\+Facilitating\+a\+Product\+Backlog\+Grooming\+Meeting |  |  |  |
| 939 |  |  |  | Project | 90 MQT | Project in Progress | Demonstrate the ability to create product burn\-down charts\. |  | A |  |  |  |  |
| 940 |  |  |  | Project | 90 MQT | Project in Progress | Discuss the advantages and disadvantages of using burn\-down charts\. |  | A |  |  |  |  |
| 961 |  |  |  | Project | Organization Overview | 90 IOS Mission and Vision |  |  | B |  |  |  |  |
| 962 |  |  |  | Project | Organization Overview | 90 IOS Organizational Structure |  |  | B |  |  |  |  |
| 963 |  |  |  | Project | Organization Overview | Real\-Time Operations and Innovation \(RTO&I\) Development Requirements |  |  | B |  |  |  |  |
| 964 |  |  |  | Project | Organization Overview | Capability Development Initiation |  |  | B |  |  |  |  |
| 965 |  |  |  | Project | Organization Overview | Flight Missions |  |  | B |  |  |  |  |
| 966 |  |  |  | Project | Supporting/Supported Organizations | 90 IOS\-Supported Organizations |  |  | B |  |  |  |  |
| 967 |  |  |  | Project | Supporting/Supported Organizations | Organizations Supporting 90 IOS |  |  | B |  |  |  |  |
| 968 |  |  |  | Project | Supporting/Supported Organizations | Mission Area Responsibilities of Supported/Supporting Organizations |  |  | B |  |  |  |  |
| 970 |  |  |  | Project | Cyber Mission Forces | Cyber Mission Forces \(CMF\) Teams |  |  | B |  |  |  |  |
| 971 |  |  |  | Project | Cyber Mission Forces | 90 IOS CMF\-Role |  |  | B |  |  |  |  |
| 972 |  |  |  | Project | Cyber Mission Forces | Interaction Between CMF Teams |  |  | B |  |  |  |  |
| 973 |  |  |  | Project | Cyber Mission Forces | 90 IOS CMF Teams |  |  | B |  |  |  |  |
| 974 |  |  |  | Project | Cyber Mission Forces | 90 IOS CMF Team Back\-Shop Support Requests/Receipt |  |  | B |  |  |  |  |
| 975 |  |  |  | Project | Exercises | Common 90 IOS Exercises |  |  | A |  |  |  |  |
| 977 |  |  |  | Project | Policy and Compliance | Executive Order |  |  | A |  |  |  |  |
| 978 |  |  |  | Project | Policy and Compliance | DoD Directive |  |  | A |  |  |  |  |
| 979 |  |  |  | Project | Policy and Compliance | NSA/CSS Policy |  |  | A |  |  |  |  |
| 980 |  |  |  | Project | Policy and Compliance | Federal Act |  |  | A |  |  |  |  |
| 981 |  |  |  | Project | Policy and Compliance | Signals Intelligence \(SIGINT\) |  |  | A |  |  |  |  |
| 982 |  |  |  | Project | Policy and Compliance | USSID |  |  | A |  |  |  |  |
| 983 |  |  |  | Project | Policy and Compliance | Classification Markings |  |  | A |  |  |  |  |
| 984 |  |  |  | Project | Policy and Compliance | United States Code Titles |  |  | A |  |  |  |  |
| 985 |  |  |  | Project | Policy and Compliance | Legal Targeting |  |  | B |  |  |  |  |
| 987 |  |  |  | Software | Windows Operating System | Basic Administration Using The Graphic User Interface \(GUI\) |  |  | B |  |  |  |  |
| 1342 |  |  |  | Project | Agile | Fundamentals of the Product Owner Role | Outline relationship between the PO and other scrum members\. |  | C |  |  |  |  |
| 1343 |  |  |  | Project |  | Fundamentals of the Product Owner Role | Demonstrate knowledge of presentation and spreadsheet software\. \(PowerPoint/Excel, etc\.\) |  | B |  |  |  |  |
| 1344 |  |  |  | Project | Agile | Fundamentals of the Product Owner Role | Illustrate ability to use software technologies that aid the role of a PO\. |  | B |  |  |  |  |
| 1345 |  |  |  | Project | Agile | Fundamentals of the Product Owner Role | Explain Basic Facts and methodologies associated with Software testing |  | B |  |  |  |  |
| 1346 |  |  |  | Project | Agile | Fundamentals of the Product Owner Role | Interpret basic computer architecture and networking concepts |  | B |  |  |  |  |
| 1347 |  |  |  | Project | Agile | Fundamentals of the Product Owner Role | Identify programming languages commonly used by development teams |  | A |  |  |  |  |
| 1348 |  |  |  | Project | Agile | Fundamentals of the Product Owner Role | Define basic programming terminology and be able to relate programming concepts |  | 1a |  |  |  |  |
| 1349 |  |  |  | Project | Agile | Agile Fundementals | Understand how agile process is used to mitigate risk with customer |  | B |  |  |  |  |
| 1350 |  |  |  | Project | Agile | Fundamentals of the Product Owner Role | Interpret customer expectations |  | B |  |  |  |  |
| 1351 |  |  |  | Project | Agile | Agile Fundementals | Apply knowledge of Agile scrum to manage Epic/user story criteria |  | 2b |  |  |  |  |
| 1352 |  |  |  | Project | Agile | Fundamentals of the Product Owner Role | Create documentation relaying leadership/customer vision |  | 2b |  |  |  |  |
| 1353 |  |  |  | Project | Agile | Fundamentals of the Product Owner Role | Collaborate with team members to create project charter |  | 2b |  |  |  |  |
| 1354 |  |  |  | Project | Agile | Fundamentals of the Product Owner Role | Apply a thorough understanding of tools associated with JIRA to manage development team projects |  | 2b |  |  |  |  |
| 1355 |  |  |  | Project | Agile | Fundamentals of the Product Owner Role | Establish a galvanizing environment to improve team collaboration |  | 2b |  |  |  |  |
| 1356 |  |  |  | Project | Agile | Fundamentals of the Product Owner Role | Relate and demonstrate tools commonly used by 90 COS dev teams for communication \(e\.g\. Slack\) |  | 1a |  |  |  |  |
| 1357 |  |  |  | Project | Git | Fundamentals of the Product Owner Role | Identify features associated with Git/Gitlab |  | A |  |  |  |  |
| 1358 |  |  |  | Project | Agile | Understanding Customers and Users | Develop and Construct a rapport with leadership |  | 1a |  |  |  |  |
| 1359 |  |  |  | Project | Agile | Fundamentals of the Product Owner Role | Identify and weigh product assumptions |  | 1a |  |  |  |  |
| 1360 |  |  |  | Project | Agile | Agile roles and responsibilities | Formulate and prioritize backlog items |  | 2b |  |  |  |  |
| 1361 |  |  |  | Project | Agile | Understanding Customers and Users | Relate customer expectations with team members for product development |  | 2b |  |  |  |  |
| 1362 |  |  |  | Project | Agile | Understanding Customers and Users | Collaborate with tool champion to identify team requirements |  | 2b |  |  |  |  |
| 1363 |  |  |  | Project | Agile | Fundamentals of the Product Owner Role | Assess and appraise the use of deliverables at sprint reviews |  | 2b |  |  |  |  |
| 1364 |  |  |  | Project | Agile | Fundamentals of the Product Owner Role | Design acceptance criteria\. |  | 1a |  |  |  |  |
| 1365 |  |  |  | Project | Agile | Fundamentals of the Product Owner Role | Apply the required principles to create a definition of done |  | 2b |  |  |  |  |
| 1366 |  |  |  | Project | Jira | Jira Fundamentals | Compile and organize issues within JIRA\. |  | 2b |  |  |  |  |
| 1367 |  |  |  | Project | Agile | Tools and Processes | Summarize local 90 COS standard operating procedures \(SOP's\) for development tasks |  | B |  |  |  |  |
| 1368 |  |  |  | Project | Agile | Tools and Processes | Identify the types/use of phone systems located within the 90th COS \(e\.g\. DSN, Classified phone, and long distance\) |  | A |  |  |  |  |
| 1369 |  |  |  | Project | Agile | Scrum Framework | Interpret velocity and how it used to create the sprint goal\. |  | B |  |  |  |  |
| 1370 |  |  |  | Project | Agile | Scrum Framework | Distinguish between user stories, tasks, and subtasks\. |  | B |  |  |  |  |
| 1371 |  |  |  | Project | Agile | Understanding Customers and Users | List internal and external organizations associated with 90 COS \(90 COS, dev teams, etc\) |  | A |  |  |  |  |
| 1372 |  |  |  | Project | Agile | Scrum Development | Demonstrate and in depth understanding of Cyber Development within the 90th COS |  | C |  |  |  |  |
| 1373 |  |  |  | Project | Agile | Agile Fundementals | Describe the 12 principles of agile \(refer to manifesto\) |  | B |  |  |  |  |
| 1374 |  |  |  | Project | Agile | Fundamentals of the Product Owner Role | Identify the difference between the roles of a Product Owner and a Project Manager |  | B |  |  |  |  |
