# Basic Dev Table

Note: Use left/right arrows to scroll the table left/right.

| UID | Req Src | Req Src ID | Requirement Owner\(s\) | Category | Topic/Language/Tool/Application | TASK | SUB TASK | Basic Dev Crit Task | Basic Dev | COMMENTS |  |  |  |
| :--- | :--- | :--- | :--- | :--- | :--- | :--- | :--- | :--- | :--- | :--- | :--- | :--- | :--- |
| 1 |  |  |  | Programming | Pseudocode | Read, Understand, and communicate the function of a program written in Pseudocode  |  |  | 2b |  |  |  |  |
| 2 |  |  |  | Programming | Pseudocode | Write Pseudocode for a sequential process |  |  | 2b |  |  |  |  |
| 3 |  |  |  | Programming | Pseudocode | Write Pseudocode with repeating steps to the process |  |  | 2b |  |  |  |  |
| 4 |  |  |  | Programming | Pseudocode | Solve programming problems with Pseudocode |  |  | 2b |  |  |  |  |
| 5 |  |  |  | Programming | Pseudocode | Given multiple Pseudocode solutions, evaluate general efficiency and effectiveness to select the best solution |  |  | 2b |  |  |  |  |
| 6 |  |  |  | Project | Git | Perform version control in Git |  |  | 3c |  |  |  |  |
| 7 |  |  |  | Project | Git | Add Branch |  |  | 3c |  |  |  |  |
| 8 |  |  |  | Project | Git | Remove Branches |  |  | 3c |  |  |  |  |
| 9 |  |  |  | Project | Git | Commit |  |  | 3c |  |  |  |  |
| 10 |  |  |  | Project | Git | Push |  |  | 3c |  |  |  |  |
| 11 |  |  |  | Project | Git | Merge |  |  | 3c |  |  |  |  |
| 12 |  |  |  | Project | Git | Reset |  |  | 3c |  |  |  |  |
| 13 |  |  |  | Project | Git | Clone |  |  | 3c |  |  |  |  |
| 14 |  |  |  | Project | Git | Checkout |  |  | 3c |  |  |  |  |
| 15 |  |  |  | Project | Git | Diff |  |  | 3c |  |  |  |  |
| 16 |  |  |  | Project | Git | Perform version control in Git | Visual Diff Tools |  | 3c |  |  |  |  |
| 17 |  |  |  | Project | Git | Knowledge of different Git workflows |  |  | B |  |  |  |  |
| 18 |  |  |  | Software | Software Engineering fundamentals | Modular Design |  |  | B |  |  |  |  |
| 19 |  |  |  | Software | Software Engineering fundamentals | Atomic Functions, Break Down |  |  | B | Making sure that you don't have one massive function that does everything, but breaking down the functions by discrete tasks |  |  |  |
| 20 |  |  |  | Software | Software Engineering fundamentals | Code Styling Standards |  |  | B |  |  |  |  |
| 21 |  |  |  | Software | Software Engineering fundamentals | DRY and SOLID design principles |  |  | B | i\.e\. Don't repeat yourself\.\.\. |  |  |  |
| 22 |  |  |  | Software | Software Engineering fundamentals | Problem Deconstruction |  |  | 2c | I\.e\. Identify functions, variables, inputs, outputs, memory considerations, modularity, error conditions, etc |  |  |  |
| 23 |  |  |  | Software | Software Engineering fundamentals | Integrating Functionality |  |  | 2c | Take multiple blocks of code that they have developed/functions and integrate them together |  |  |  |
| 24 |  |  |  | Programming | Python | Describe the purpose and use of the following Python concepts | The return statement |  | B |  |  |  |  |
| 25 |  |  |  | Programming | Python | Describe the purpose and use of the following Python concepts | Data types |  | B |  |  |  |  |
| 26 |  |  |  | Programming | Python | Describe the purpose and use of the following Python concepts | A function |  | B |  |  |  |  |
| 27 |  |  |  | Programming | Python | Describe the purpose and use of the following Python concepts | Parameters |  | B |  |  |  |  |
| 28 |  |  |  | Programming | Python | Describe the purpose and use of the following Python concepts | Scope |  | B |  |  |  |  |
| 29 |  |  |  | Programming | Python | Describe the purpose and use of the following Python concepts | Return values \(return type and reference\) |  | B |  |  |  |  |
| 30 |  |  |  | Programming | Python | Describe the purpose and use of the following Python concepts | Import \(<module>, from, \*, etc\) |  | B |  |  |  |  |
| 31 |  |  |  | Programming | Python | Describe the purpose and use of the following Python concepts | Dictionaries |  | B |  |  |  |  |
| 32 |  |  |  | Programming | Python | Describe the purpose and use of the following Python concepts | Lists |  | B |  |  |  |  |
| 33 |  |  |  | Programming | Python | Describe the purpose and use of the following Python concepts | Tuple |  | B |  |  |  |  |
| 34 |  |  |  | Programming | Python | Describe the purpose and use of the following Python concepts | Structs |  | B |  |  |  |  |
| 35 |  |  |  | Programming | Python | Describe the purpose and use of the following Python concepts | Unions |  | B |  |  |  |  |
| 36 |  |  |  | Programming | Python | Describe the purpose and use of the following Python concepts | Singleton |  | B |  |  |  |  |
| 37 |  |  |  | Programming | Python | Describe the purpose and use of the following Python concepts | The term mutable |  | B |  |  |  |  |
| 38 |  |  |  | Programming | Python | Describe the purpose and use of the following Python concepts | The term immutable |  | B |  |  |  |  |
| 39 |  |  |  | Programming | Python | Demonstrate the ability to parse command line arguments using built\-in functionality\. | Command line Arguments |  | 3c | argparse library; In addition to the program package itself \(program plus comments, documentation, etc\) must PRESENT the functionality to an evaluator\. |  |  |  |
| 40 |  |  |  | Programming | Python | Demonstrate the proper declaration and use of Python data types and object\-oriented constructs\. | Integer \(int\) |  | 3c | In addition to the program package itself \(program plus comments, documentation, etc\) must PRESENT the functionality to an evaluator\. |  |  |  |
| 41 |  |  |  | Programming | Python | Demonstrate the proper declaration and use of Python data types and object\-oriented constructs\. | Float \(float\) |  | 3c | In addition to the program package itself \(program plus comments, documentation, etc\) must PRESENT the functionality to an evaluator\. |  |  |  |
| 42 |  |  |  | Programming | Python | Demonstrate the proper declaration and use of Python data types and object\-oriented constructs\. | String \(str\) |  | 3c | In addition to the program package itself \(program plus comments, documentation, etc\) must PRESENT the functionality to an evaluator\. |  |  |  |
| 43 |  |  |  | Programming | Python | Demonstrate the proper declaration and use of Python data types and object\-oriented constructs\. | List \(list\) |  | 3c | In addition to the program package itself \(program plus comments, documentation, etc\) must PRESENT the functionality to an evaluator\. |  |  |  |
| 44 |  |  |  | Programming | Python | Demonstrate the proper declaration and use of Python data types and object\-oriented constructs\. | Multi\-dimensional list |  | 3c | In addition to the program package itself \(program plus comments, documentation, etc\) must PRESENT the functionality to an evaluator\. |  |  |  |
| 45 |  |  |  | Programming | Python | Demonstrate the proper declaration and use of Python data types and object\-oriented constructs\. | Dictionary |  | 3c | In addition to the program package itself \(program plus comments, documentation, etc\) must PRESENT the functionality to an evaluator\. |  |  |  |
| 46 |  |  |  | Programming | Python | Demonstrate the proper declaration and use of Python data types and object\-oriented constructs\. | Tuple |  | 3c | In addition to the program package itself \(program plus comments, documentation, etc\) must PRESENT the functionality to an evaluator\. |  |  |  |
| 47 |  |  |  | Programming | Python | Demonstrate the proper declaration and use of Python data types and object\-oriented constructs\. | Singleton |  | 2b |  |  |  |  |
| 50 |  |  |  | Programming | Python | Demonstrate the ability to perform conversion between Python 2 and Python 3 | Python 2 vs Python 3 |  | 1a |  |  |  |  |
| 51 |  |  |  | Programming | Python | Demonstrate the ability to utilize the standard library | regex |  | 2b |  |  |  |  |
| 52 |  |  |  | Programming | Python | Demonstrate the ability to utilize the standard library | OS |  | 2b |  |  |  |  |
| 53 |  |  |  | Programming | Python | Demonstrate the ability to utilize the standard library | Crypto |  | 2b |  |  |  |  |
| 54 |  |  |  | Programming | Python | Demonstrate the ability to utilize PIP | Python install packages |  | 2b |  |  |  |  |
| 55 |  |  |  | Programming | Python | Demonstrate the ability to utilize Python Virtual Environments | Python Virtual Environments |  | 2b |  |  |  |  |
| 56 |  |  |  | Programming | Python | Demonstrate the ability to perform basic arithmetic operations using appropriate Python operators while ensuring proper order of operations \(PEMDAS\)\. | Addition |  | 3c | In addition to the program package itself \(program plus comments, documentation, etc\) must PRESENT the functionality to an evaluator\. |  |  |  |
| 57 |  |  |  | Programming | Python | Demonstrate the ability to perform basic arithmetic operations using appropriate Python operators while ensuring proper order of operations \(PEMDAS\)\. | Subtraction |  | 3c | In addition to the program package itself \(program plus comments, documentation, etc\) must PRESENT the functionality to an evaluator\. |  |  |  |
| 58 |  |  |  | Programming | Python | Demonstrate the ability to perform basic arithmetic operations using appropriate Python operators while ensuring proper order of operations \(PEMDAS\)\. | Multiplication |  | 3c | In addition to the program package itself \(program plus comments, documentation, etc\) must PRESENT the functionality to an evaluator\. |  |  |  |
| 59 |  |  |  | Programming | Python | Demonstrate the ability to perform basic arithmetic operations using appropriate Python operators while ensuring proper order of operations \(PEMDAS\)\. | Division |  | 3c | In addition to the program package itself \(program plus comments, documentation, etc\) must PRESENT the functionality to an evaluator\. |  |  |  |
| 60 |  |  |  | Programming | Python | Demonstrate the ability to perform basic arithmetic operations using appropriate Python operators while ensuring proper order of operations \(PEMDAS\)\. | Modulus |  | 3c | In addition to the program package itself \(program plus comments, documentation, etc\) must PRESENT the functionality to an evaluator\. |  |  |  |
| 61 |  |  |  | Programming | Python | Demonstrate the ability to perform file management operations in Python\. | Open an existing file |  | 3c | In addition to the program package itself \(program plus comments, documentation, etc\) must PRESENT the functionality to an evaluator\. |  |  |  |
| 62 |  |  |  | Programming | Python | Demonstrate the ability to perform file management operations in Python\. | Read data from a file |  | 3c | In addition to the program package itself \(program plus comments, documentation, etc\) must PRESENT the functionality to an evaluator\. |  |  |  |
| 63 |  |  |  | Programming | Python | Demonstrate the ability to perform file management operations in Python\. | Parse data from a file |  | 3c | In addition to the program package itself \(program plus comments, documentation, etc\) must PRESENT the functionality to an evaluator\. |  |  |  |
| 64 |  |  |  | Programming | Python | Demonstrate the ability to perform file management operations in Python\. | Write data to a file |  | 3c | In addition to the program package itself \(program plus comments, documentation, etc\) must PRESENT the functionality to an evaluator\. |  |  |  |
| 65 |  |  |  | Programming | Python | Demonstrate the ability to perform file management operations in Python\. | Modify data in a file |  | 3c | In addition to the program package itself \(program plus comments, documentation, etc\) must PRESENT the functionality to an evaluator\. |  |  |  |
| 66 |  |  |  | Programming | Python | Demonstrate the ability to perform file management operations in Python\. | Close an open file |  | 3c | In addition to the program package itself \(program plus comments, documentation, etc\) must PRESENT the functionality to an evaluator\. |  |  |  |
| 67 |  |  |  | Programming | Python | Demonstrate the ability to perform file management operations in Python\. | Print file information to the console |  | 3c | In addition to the program package itself \(program plus comments, documentation, etc\) must PRESENT the functionality to an evaluator\. |  |  |  |
| 68 |  |  |  | Programming | Python | Demonstrate the ability to perform file management operations in Python\. | Create a new file |  | 3c | In addition to the program package itself \(program plus comments, documentation, etc\) must PRESENT the functionality to an evaluator\. |  |  |  |
| 69 |  |  |  | Programming | Python | Demonstrate the ability to perform file management operations in Python\. | Append data to an existing file |  | 3c | In addition to the program package itself \(program plus comments, documentation, etc\) must PRESENT the functionality to an evaluator\. |  |  |  |
| 70 |  |  |  | Programming | Python | Demonstrate the ability to perform file management operations in Python\. | Delete a file |  | 3c | In addition to the program package itself \(program plus comments, documentation, etc\) must PRESENT the functionality to an evaluator\. |  |  |  |
| 71 |  |  |  | Programming | Python | Demonstrate the ability to perform file management operations in Python\. | Determine the size of a file |  | 3c | In addition to the program package itself \(program plus comments, documentation, etc\) must PRESENT the functionality to an evaluator\. |  |  |  |
| 72 |  |  |  | Programming | Python | Demonstrate the ability to perform file management operations in Python\. | Determine location within a file |  | 3c | In addition to the program package itself \(program plus comments, documentation, etc\) must PRESENT the functionality to an evaluator\. |  |  |  |
| 73 |  |  |  | Programming | Python | Demonstrate the ability to perform file management operations in Python\. | Insert data into an existing file |  | 3c | In addition to the program package itself \(program plus comments, documentation, etc\) must PRESENT the functionality to an evaluator\. |  |  |  |
| 74 |  |  |  | Programming | Python | Demonstrate the ability to create and implement functions to meet a requirement\. | A function that returns multiple values |  | 3c | In addition to the program package itself \(program plus comments, documentation, etc\) must PRESENT the functionality to an evaluator\. |  |  |  |
| 75 |  |  |  | Programming | Python | Demonstrate the ability to create and implement functions to meet a requirement\. | A function that receives input from a user |  | 3c | In addition to the program package itself \(program plus comments, documentation, etc\) must PRESENT the functionality to an evaluator\. |  |  |  |
| 76 |  |  |  | Programming | Python | Demonstrate the ability to create and implement functions to meet a requirement\. | A recursive function |  | 3c | In addition to the program package itself \(program plus comments, documentation, etc\) must PRESENT the functionality to an evaluator\. |  |  |  |
| 77 |  |  |  | Programming | Python | Demonstrate the ability to perform data validation\. | Validating received input matches expected input |  | 2c |  |  |  |  |
| 78 |  |  |  | Programming | Python | Demonstrate the ability to perform data validation\. | Creating a method for exception handling |  | 3c | In addition to the program package itself \(program plus comments, documentation, etc\) must PRESENT the functionality to an evaluator\. |  |  |  |
| 79 |  |  |  | Programming | Python | Demonstrate the ability to perform data validation\. | Implementing a method for exception handling |  | 3c | In addition to the program package itself \(program plus comments, documentation, etc\) must PRESENT the functionality to an evaluator\. |  |  |  |
| 80 |  |  |  | Programming | Python | Demonstrate skill in creating an implementing conditional statements, expressions, and constructs\. | for loop |  | 3c | In addition to the program package itself \(program plus comments, documentation, etc\) must PRESENT the functionality to an evaluator\. |  |  |  |
| 81 |  |  |  | Programming | Python | Demonstrate skill in creating an implementing conditional statements, expressions, and constructs\. | while loop |  | 3c | In addition to the program package itself \(program plus comments, documentation, etc\) must PRESENT the functionality to an evaluator\. |  |  |  |
| 82 |  |  |  | Programming | Python | Demonstrate skill in creating an implementing conditional statements, expressions, and constructs\. | do while loop |  | 3c | In addition to the program package itself \(program plus comments, documentation, etc\) must PRESENT the functionality to an evaluator\. |  |  |  |
| 83 |  |  |  | Programming | Python | Demonstrate skill in creating an implementing conditional statements, expressions, and constructs\. | if statement |  | 3c | In addition to the program package itself \(program plus comments, documentation, etc\) must PRESENT the functionality to an evaluator\. |  |  |  |
| 84 |  |  |  | Programming | Python | Demonstrate skill in creating an implementing conditional statements, expressions, and constructs\. | if\->else statement |  | 3c | In addition to the program package itself \(program plus comments, documentation, etc\) must PRESENT the functionality to an evaluator\. |  |  |  |
| 85 |  |  |  | Programming | Python | Demonstrate skill in creating an implementing conditional statements, expressions, and constructs\. | if\->else if\->else statement |  | 3c | In addition to the program package itself \(program plus comments, documentation, etc\) must PRESENT the functionality to an evaluator\. |  |  |  |
| 86 |  |  |  | Programming | Python | Describe Python Standards | Python Enhancement Proposal \(PEP8\) |  | B |  |  |  |  |
| 87 |  |  |  | Programming | Python | Describe Python Standards | PyDocs |  | B |  |  |  |  |
| 88 |  |  |  | Programming | Python | Describe Python Standards | Python 2 vs Python 3 |  | B |  |  |  |  |
| 89 |  |  |  | Programming | Python | Describe Python Standards | Standard Library |  | B |  |  |  |  |
| 90 |  |  |  | Programming | Python | Describe the purpose and use of the following Python concepts\. | White space |  | B | Add to 3\.2\.1 in JQR |  |  |  |
| 91 |  |  |  | Programming | Python | Describe the purpose and use of the following Python concepts\. | Iterators |  | B | Add to 3\.2\.1 in JQR |  |  |  |
| 92 |  |  |  | Programming | Python | Describe the purpose and use of the following Python concepts\. | Generators |  | B | Add to 3\.2\.1 in JQR |  |  |  |
| 93 |  |  |  | Programming | Python | Describe the purpose and use of the following Python concepts\. | Lambda functions |  | B | Add to 3\.2\.1 in JQR |  |  |  |
| 94 |  |  |  | Programming | Python | Describe the purpose and use of the following Python concepts\. | C\-types |  | B | Add to 3\.2\.1 in JQR |  |  |  |
| 95 |  |  |  | Programming | Python | Describe the terms and fundamentals associated with object\-oriented programming using Python\. | Polymorphism |  | B | Add to 3\.2\.9 in JQR |  |  |  |
| 96 |  |  |  | Programming | C | Demonstrate skill in compiling, linking, and debugging\. | Execute a program in a debugger to perform general debugging actions |  | 3c | Set a break point |  observe values of variables |  step through code |  attach to running process |
| 97 |  |  |  | Programming | Unicode | Utilize Unicode strings |  |  | 2b |  |  |  |  |
| 98 |  |  |  | Network | Networking Concepts | Advanced Networking | Error handling |  | 2b | Add to 3\.1\.1 in JQR; not compile errors or logic errors\.\.\. try\-catch, making sure your program can handle errors |  |  |  |
| 99 |  |  |  | Programming | C | Conduct unit testing to validate program functionality | C |  | 2b |  |  |  |  |
| 100 |  |  |  | Programming | Python | Conduct unit testing to validate program functionality | Python |  | 2b |  |  |  |  |
| 101 |  |  |  | Project | Git | Perform version control in Git | CI/CD Pipeline |  | 1a |  |  |  |  |
| 102 |  |  |  | Programming | C | Demonstrate skill in controlling memory\. | Identify memory leaks |  | 2c | Source code analysis |  |  |  |
| 103 |  |  |  | Programming | C | Demonstrate skill in controlling memory\. | Remove identified memory leaks |  | 2c | source code analysis |  |  |  |
| 104 |  |  |  | Programming | C | Demonstrate skill in controlling memory\. | Make effective use of Valgrind with \-leak\-check=full to identify and correct memory leaks and context errors\. |  | 2c | need workstation |  |  |  |
| 105 |  |  |  | Programming | Python | Describe the terms and fundamentals associated with object\-oriented programming using Python\. | Class |  | C | Provide a case study scenario \(ex\. A bank wants you to create a program with users, account numbers, accounts, etc and they have different banks, etc\)\.\.\. write an essay on how you would use \_\_\_\_\_ to implement your program\. |  |  |  |
| 106 |  |  |  | Programming | Python | Describe the terms and fundamentals associated with object\-oriented programming using Python\. | Object |  | C | Provide a case study scenario \(ex\. A bank wants you to create a program with users, account numbers, accounts, etc and they have different banks, etc\)\.\.\. write an essay on how you would use \_\_\_\_\_ to implement your program\. |  |  |  |
| 107 |  |  |  | Programming | Python | Describe the terms and fundamentals associated with object\-oriented programming using Python\. | Difference between an object and a class |  | B |  |  |  |  |
| 108 |  |  |  | Programming | Python | Describe the terms and fundamentals associated with object\-oriented programming using Python\. | Advantages to object\-oriented programming |  | A |  |  |  |  |
| 109 |  |  |  | Programming | Python | Describe the terms and fundamentals associated with object\-oriented programming using Python\. | Inheritance |  | A |  |  |  |  |
| 110 |  |  |  | Programming | Python | Describe the terms and fundamentals associated with object\-oriented programming using Python\. | The keyword "Super" |  | A |  |  |  |  |
| 111 |  |  |  | Programming | Python | Describe the terms and fundamentals associated with object\-oriented programming using Python\. | Initialization function of a constructor |  | A |  |  |  |  |
| 112 |  |  |  | Programming | Python | Describe the terms and fundamentals associated with object\-oriented programming using Python\. | The getter and setter functions |  | A |  |  |  |  |
| 113 |  |  |  | Programming | Python | Describe the terms and fundamentals associated with object\-oriented programming using Python\. | Attributes of a class |  | A |  |  |  |  |
| 114 |  |  |  | Programming | Python | Describe the terms and fundamentals associated with object\-oriented programming using Python\. | Factory design pattern |  | A |  |  |  |  |
| 115 |  |  |  | Programming | Python | Describe the terms and fundamentals associated with object\-oriented programming using Python\. | Singleton design pattern |  | A |  |  |  |  |
| 116 |  |  |  | Programming | Python | Describe the terms and fundamentals associated with object\-oriented programming using Python\. | Adapter design pattern |  | A |  |  |  |  |
| 117 |  |  |  | Programming | Python | Describe the terms and fundamentals associated with object\-oriented programming using Python\. | Bridge design pattern |  | A |  |  |  |  |
| 118 |  |  |  | Programming | C | Demonstrate skill in creating and implementing a hash function\. | Hash function |  | 2b | "create" could mean finding psuedocode online and using it |  |  |  |
| 119 |  |  |  | Programming | C | Demonstrate skill in creating and implementing a sort routine\. | Sort routine |  | 2b | Pick which algorithm to implement; "create" could mean finding psuedocode online and using it |  |  |  |
| 120 |  |  |  | Programming | C | Demonstrate skill in creating and implementing a state machine\. | state machine |  | 2b | "create" could mean finding psuedocode online and using it |  |  |  |
| 121 |  |  |  | Programming | C | Demonstrate skill in implementing Dijkstra's Algorithm to find the optimal path in a weighted graph\. | Dijkstra's algorithm |  | 1a |  |  |  |  |
| 130 |  |  |  | Software | Transversal | Describe the concepts associated with traversal techniques\. | Depth first traversal |  | B |  |  |  |  |
| 131 |  |  |  | Software | Transversal | Describe the concepts associated with traversal techniques\. | Breadth first traversal |  | B |  |  |  |  |
| 132 |  |  |  | Software | Transversal | Describe the concepts associated with traversal techniques\. | The technique of determining the weight of a given path |  | B |  |  |  |  |
| 133 |  |  |  | Software | Transversal | Describe the concepts associated with traversal techniques\. | How the most efficient path for traversing a graph is determined |  | B |  |  |  |  |
| 134 |  |  |  | Software | Hashing | Describe the concepts associated with hashing\. | Data distribution as it relates to hashing |  | B | Hashing for data structures \(not cryptographic hashing\) |  |  |  |
| 135 |  |  |  | Software | Hashing | Describe the concepts associated with hashing\. | Hash function efficiency |  | B | Hashing for data structures \(not cryptographic hashing\) |  |  |  |
| 136 |  |  |  | Software | Hashing | Describe the concepts associated with hashing\. | Hash collisions |  | B | Hashing for data structures \(not cryptographic hashing\) |  |  |  |
| 137 |  |  |  | Software | Software Engineering | Demonstrate the ability to analyze sorting routines to determine the most efficient one to use, using an approximation of Big\-O notation |  |  | 2b | Given a piece of code calculate the Big\-O for that code |  |  |  |
| 138 |  |  |  | Software | Data Structures | Describe the concepts and terms associated with the following data structures\. | Binary search tree |  | B |  |  |  |  |
| 139 |  |  |  | Software | Data Structures | Describe the concepts and terms associated with the following data structures\. | Linked list |  | B |  |  |  |  |
| 140 |  |  |  | Software | Data Structures | Describe the concepts and terms associated with the following data structures\. | Doubly linked list |  | B |  |  |  |  |
| 141 |  |  |  | Software | Data Structures | Describe the concepts and terms associated with the following data structures\. | Circularly linked list |  | B |  |  |  |  |
| 142 |  |  |  | Software | Data Structures | Describe the concepts and terms associated with the following data structures\. | Weighted Graph |  | B |  |  |  |  |
| 143 |  |  |  | Software | Data Structures | Describe the concepts and terms associated with the following data structures\. | Common pitfalls when using linked lists, queues, trees, and graphs |  | B |  |  |  |  |
| 144 |  |  |  | Software | Data Structures | Describe the concepts and terms associated with the following data structures\. | The effect of First in First Out \(FIFO\) and Last in First Out \(LIFO\) |  | B |  |  |  |  |
| 145 |  |  |  | Programming | Python | Demonstrate skill in creating and using a weighted graph\. | Defining the structures required for graphs |  | 2b |  |  |  |  |
| 146 |  |  |  | Programming | Python | Demonstrate skill in creating and using a weighted graph\. | Creating a graph with n number of nodes |  | 2b |  |  |  |  |
| 147 |  |  |  | Programming | Python | Demonstrate skill in creating and using a weighted graph\. | Adding n number of edges to a graph |  | 2b |  |  |  |  |
| 148 |  |  |  | Programming | Python | Demonstrate skill in creating and using a weighted graph\. | Finding a node within an existing graph |  | 2b |  |  |  |  |
| 149 |  |  |  | Programming | Python | Demonstrate skill in creating and using a weighted graph\. | Finding an edge within a graph |  | 2b |  |  |  |  |
| 150 |  |  |  | Programming | Python | Demonstrate skill in creating and using a weighted graph\. | Remove an edge from a graph |  | 2b |  |  |  |  |
| 151 |  |  |  | Programming | Python | Demonstrate skill in creating and using a weighted graph\. | Remove a node and all of its edges from a graph |  | 2b |  |  |  |  |
| 152 |  |  |  | Programming | Python | Demonstrate skill in creating and using a weighted graph\. | Calculate the weight of a path within a graph |  | 2b |  |  |  |  |
| 153 |  |  |  | Programming | Python | Demonstrate skill in creating and using a weighted graph\. | Destroy the graph |  | 2b |  |  |  |  |
| 154 |  |  |  | Programming | Python | Demonstrate skill in creating and using a linked list\. | Creating a linked list with n number of items |  | 3c | C and python |  |  |  |
| 155 |  |  |  | Programming | Python | Demonstrate skill in creating and using a linked list\. | Navigating through a linked list |  | 3c | C and python |  |  |  |
| 156 |  |  |  | Programming | Python | Demonstrate skill in creating and using a linked list\. | Retrieving the first occurrence of an item in a linked list |  | 3c | C and python |  |  |  |
| 157 |  |  |  | Programming | Python | Demonstrate skill in creating and using a linked list\. | Sorting the linked list alphanumerically using a function pointer |  | 3c | C and python |  |  |  |
| 158 |  |  |  | Programming | Python | Demonstrate skill in creating and using a linked list\. | Removing selected items from the linked list |  | 3c | C and python |  |  |  |
| 159 |  |  |  | Programming | Python | Demonstrate skill in creating and using a linked list\. | Inserting an item into a specific location in a linked list |  | 3c | C and python |  |  |  |
| 160 |  |  |  | Programming | Python | Demonstrate skill in creating and using a linked list\. | Destroying a linked list |  | 3c | C and python |  |  |  |
| 161 |  |  |  | Programming | Python | Demonstrate skill in creating an using a doubly linked list\. | Creating a doubly linked list with n number of items |  | 3c | C and python |  |  |  |
| 162 |  |  |  | Programming | Python | Demonstrate skill in creating an using a doubly linked list\. | Navigating through a doubly linked list |  | 3c | C and python |  |  |  |
| 163 |  |  |  | Programming | Python | Demonstrate skill in creating an using a doubly linked list\. | Finding the first occurrence of an item in a doubly linked list |  | 3c | C and python |  |  |  |
| 164 |  |  |  | Programming | Python | Demonstrate skill in creating an using a doubly linked list\. | Sorting the doubly linked list alphanumerically list using a function pointer |  | 3c | C and python |  |  |  |
| 165 |  |  |  | Programming | Python | Demonstrate skill in creating an using a doubly linked list\. | Removing selected items from the doubly linked list |  | 3c | C and python |  |  |  |
| 166 |  |  |  | Programming | Python | Demonstrate skill in creating an using a doubly linked list\. | Inserting an item into a specified location in a doubly linked list |  | 3c | C and python |  |  |  |
| 167 |  |  |  | Programming | Python | Demonstrate skill in creating an using a doubly linked list\. | Removing all items from a doubly linked list |  | 3c | C and python |  |  |  |
| 168 |  |  |  | Programming | Python | Demonstrate skill in creating an using a doubly linked list\. | Destroying a doubly linked list |  | 3c | C and python |  |  |  |
| 169 |  |  |  | Programming | Python | Demonstrate skill in creating and using a circularly linked list\. | Creating a circularly linked list with n number of items |  | 2b | C and python |  |  |  |
| 170 |  |  |  | Programming | Python | Demonstrate skill in creating and using a circularly linked list\. | Navigating through a circularly linked list |  | 2b | C and python |  |  |  |
| 171 |  |  |  | Programming | Python | Demonstrate skill in creating and using a circularly linked list\. | Finding the first occurrence of an item in a circularly linked list |  | 2b | C and python |  |  |  |
| 172 |  |  |  | Programming | Python | Demonstrate skill in creating and using a circularly linked list\. | Sorting the circularly linked list alphanumerically list using a function pointer |  | 2b | C and python |  |  |  |
| 173 |  |  |  | Programming | Python | Demonstrate skill in creating and using a circularly linked list\. | Removing selected items from the circularly linked list |  | 2b | C and python |  |  |  |
| 174 |  |  |  | Programming | Python | Demonstrate skill in creating and using a circularly linked list\. | Inserting an item into a specified location in a circularly linked list |  | 2b | C and python |  |  |  |
| 175 |  |  |  | Programming | Python | Demonstrate skill in creating and using a circularly linked list\. | Removing all items from a circularly linked list |  | 2b | C and python |  |  |  |
| 176 |  |  |  | Programming | Python | Demonstrate skill in creating and using a circularly linked list\. | Destroying a circularly linked list |  | 2b | C and python |  |  |  |
| 177 |  |  |  | Programming | Python | Demonstrate skill in creating and using a queue\. | Creating a queue with n number of items |  | 3c | C and python |  |  |  |
| 178 |  |  |  | Programming | Python | Demonstrate skill in creating and using a queue\. | Navigating through the queue to find the nth item |  | 3c | C and python |  |  |  |
| 179 |  |  |  | Programming | Python | Demonstrate skill in creating and using a queue\. | Finding an item in the queue |  | 3c | C and python |  |  |  |
| 180 |  |  |  | Programming | Python | Demonstrate skill in creating and using a queue\. | Removing selected items from a queue |  | 3c | C and python |  |  |  |
| 181 |  |  |  | Programming | Python | Demonstrate skill in creating and using a queue\. | Removing all items from the queue |  | 3c | C and python |  |  |  |
| 182 |  |  |  | Programming | Python | Demonstrate skill in creating and using a queue\. | Destroying a queue |  | 3c | C and python |  |  |  |
| 183 |  |  |  | Programming | Python | Demonstrate skill in creating and using a tree\. | Creating a tree with n number of items |  | 2b |  |  |  |  |
| 184 |  |  |  | Programming | Python | Demonstrate skill in creating and using a tree\. | Navigating through a tree |  | 2b |  |  |  |  |
| 185 |  |  |  | Programming | Python | Demonstrate skill in creating and using a tree\. | Finding the first occurrence of an item in a tree |  | 2b |  |  |  |  |
| 186 |  |  |  | Programming | Python | Demonstrate skill in creating and using a tree\. | Removing selected items from the tree |  | 2b |  |  |  |  |
| 187 |  |  |  | Programming | Python | Demonstrate skill in creating and using a tree\. | Inserting an item into a specified location in a tree |  | 2b |  |  |  |  |
| 188 |  |  |  | Programming | Python | Demonstrate skill in creating and using a tree\. | Removing all items from the tree |  | 2b |  |  |  |  |
| 189 |  |  |  | Programming | Python | Demonstrate skill in creating and using a tree\. | Destroying a tree |  | 2b |  |  |  |  |
| 190 |  |  |  | Programming | Python | Demonstrate skill in creating and using a binary search tree\. | Creating a binary search tree with n number of items |  | 2b |  |  |  |  |
| 191 |  |  |  | Programming | Python | Demonstrate skill in creating and using a binary search tree\. | Navigating through a binary search tree |  | 2b |  |  |  |  |
| 192 |  |  |  | Programming | Python | Demonstrate skill in creating and using a binary search tree\. | Locating an item in a binary tree |  | 2b |  |  |  |  |
| 193 |  |  |  | Programming | Python | Demonstrate skill in creating and using a binary search tree\. | Removing selected items from the binary search tree |  | 2b |  |  |  |  |
| 194 |  |  |  | Programming | Python | Demonstrate skill in creating and using a binary search tree\. | Removing all items from the binary search tree |  | 2b |  |  |  |  |
| 195 |  |  |  | Programming | Python | Demonstrate skill in creating and using a binary search tree\. | Describe the implementation strategies for a balanced binary search tree |  | 2b |  |  |  |  |
| 196 |  |  |  | Programming | Python | Demonstrate skill in creating and using a binary search tree\. | Destroying a binary search tree |  | 2b |  |  |  |  |
| 197 |  |  |  | Programming | Python | Demonstrate skill in creating and using a hash table\. | Creating a hash table with n number of items |  | 2b |  |  |  |  |
| 198 |  |  |  | Programming | Python | Demonstrate skill in creating and using a hash table\. | Navigating through a hash table to find the nth item |  | 2b |  |  |  |  |
| 199 |  |  |  | Programming | Python | Demonstrate skill in creating and using a hash table\. | Finding an item in a hash table |  | 2b |  |  |  |  |
| 200 |  |  |  | Programming | Python | Demonstrate skill in creating and using a hash table\. | Removing selected items from a hash table |  | 2b |  |  |  |  |
| 201 |  |  |  | Programming | Python | Demonstrate skill in creating and using a hash table\. | Inserting an item into a hash table |  | 2b |  |  |  |  |
| 202 |  |  |  | Programming | Python | Demonstrate skill in creating and using a hash table\. | Implement functionality to mitigate hash collisions within the hash table |  | 2b |  |  |  |  |
| 203 |  |  |  | Programming | Python | Demonstrate skill in creating and using a hash table\. | Removing all items from the hash table |  | 2b |  |  |  |  |
| 204 |  |  |  | Programming | Python | Demonstrate skill in creating and using a stack\. | Creating a stack with n number of items |  | 2b |  |  |  |  |
| 205 |  |  |  | Programming | Python | Demonstrate skill in creating and using a stack\. | Navigating through a stack to find the nth item |  | 2b |  |  |  |  |
| 206 |  |  |  | Programming | Python | Demonstrate skill in creating and using a stack\. | Adding an item to the stack |  | 2b |  |  |  |  |
| 207 |  |  |  | Programming | Python | Demonstrate skill in creating and using a stack\. | Removing selected items from the stack |  | 2b |  |  |  |  |
| 208 |  |  |  | Programming | Python | Demonstrate skill in creating and using a stack\. | Removing all items from the stack |  | 2b |  |  |  |  |
| 209 |  |  |  | Programming | Python | Demonstrate skill in creating and using a stack\. | Destroying the stack |  | 2b |  |  |  |  |
| 210 |  |  |  | Programming | Python | Demonstrate skill in creating and using a stack\. | Preventing a stack overrun |  | 2b |  |  |  |  |
| 211 |  |  |  | Software | Networking Concepts | Advanced Networking | JSON Module |  | 2b |  |  |  |  |
| 212 |  |  |  | Programming | Python | Demonstrate the ability to handle partial reads/writes during serialization and de\-serialization\. |  |  | 2b |  |  |  |  |
| 213 |  |  |  | Programming | Python | Demonstrate the ability to serialize fixed size multi\-byte types between systems of differing endianness\. |  |  | 2c |  |  |  |  |
| 214 |  |  |  | Programming | Python | Demonstrate the ability to serialize and de\-serialize variable sized data structures between systems of differing endianness\. |  |  | 2c |  |  |  |  |
| 215 |  |  |  | Programming | Python | Describe the libraries commonly used to aid in serialization\. |  |  | B | protobuf |  |  |  |
| 216 |  |  |  | Network | Networking Concepts | Sockets | Transmission Control Protocol \(TCP\)  |  | B |  |  |  |  |
| 217 |  |  |  | Network | Networking Concepts | Sockets | User Datagram Protocol \(UDP\) |  | B |  |  |  |  |
| 218 |  |  |  | Network | Networking Concepts | Sockets | Open Systems Interconnect \(OSI\) model |  | B |  |  |  |  |
| 219 |  |  |  | Network | Networking Concepts | Sockets | POSIX API/BSD sockets |  | B |  |  |  |  |
| 220 |  |  |  | Network | Networking Concepts | Sockets | Socket Basics |  | B |  |  |  |  |
| 221 |  |  |  | Network | Networking Concepts | Basic Networking | Request for comments \(RFC\) |  | A |  |  |  |  |
| 222 |  |  |  | Network | Networking Concepts | OSI Layer 3 | Purpose of sub\-netting |  | B |  |  |  |  |
| 223 |  |  |  | Network | Networking Concepts | OSI Layer 2 | ARP |  | B |  |  |  |  |
| 224 |  |  |  | Network | Networking Concepts | OSI Layer 7 | HTTP/HTTPS |  | B |  |  |  |  |
| 225 |  |  |  | Network | Networking Concepts | OSI Layer 7 | DNS |  | B |  |  |  |  |
| 226 |  |  |  | Network | Networking Concepts | OSI Layer 7 | SMTP |  | B |  |  |  |  |
| 227 |  |  |  | Network | Networking Concepts | OSI Layer 3 | ICMP |  | B |  |  |  |  |
| 228 |  |  |  | Network | Networking Concepts | OSI Layer 7 | DHCP |  | B |  |  |  |  |
| 229 |  |  |  | Network | Networking Concepts | OSI Layer 3 | IPv4 |  | B |  |  |  |  |
| 230 |  |  |  | Network | Networking Concepts | OSI Layer 3 | IPv6 |  | B |  |  |  |  |
| 231 |  |  |  | Network | Networking Concepts | OSI Layer 2 | Overview |  | B |  |  |  |  |
| 232 |  |  |  | Network | Networking Concepts | OSI Layer 3 | IPv4 Addressing |  | B |  |  |  |  |
| 233 |  |  |  | Network | Networking Concepts | OSI Layer 3 | IPv6 Addressing |  | B |  |  |  |  |
| 234 |  |  |  | Network | Networking Concepts | OSI Layer 2 | Ethernet |  | B |  |  |  |  |
| 235 |  |  |  | Network | Networking Concepts | Basic Networking | Demonstrate ability to use Wireshark |  | 2b |  |  |  |  |
| 236 |  |  |  | Network | Networking Concepts | OSI Layer 3 | Network address translation \(NAT\) |  | B | Add to 3\.7\.1 in JQR |  |  |  |
| 237 |  |  |  | Network | Networking Concepts | OSI Layer 3 | CIDR notation |  | B | Add to 3\.7\.1 in JQR |  |  |  |
| 238 |  |  |  | Network | Networking Concepts | OSI Layer 2 | Network devices |  | B | Add to 3\.7\.1 in JQR |  |  |  |
| 239 |  |  |  | Network | Networking Concepts | Basic Networking | Serialization |  | B | Add to 3\.7\.1 in JQR |  |  |  |
| 240 |  |  |  | Network | Networking Concepts | Basic Networking | Endianness |  | B | Add to 3\.7\.1 in JQR |  |  |  |
| 241 |  |  |  | Programming | Python | Demonstrate skill in using networking commands accounting for endianness\. | socket\(\) |  | 3c |  |  |  |  |
| 242 |  |  |  | Programming | Python | Demonstrate skill in using networking commands accounting for endianness\. | send\(\) |  | 3c |  |  |  |  |
| 243 |  |  |  | Programming | Python | Demonstrate skill in using networking commands accounting for endianness\. | recv\(\) |  | 3c |  |  |  |  |
| 244 |  |  |  | Programming | Python | Demonstrate skill in using networking commands accounting for endianness\. | sendto\(\) |  | 3c |  |  |  |  |
| 245 |  |  |  | Programming | Python | Demonstrate skill in using networking commands accounting for endianness\. | recvfrom\(\) |  | 3c |  |  |  |  |
| 246 |  |  |  | Programming | Python | Demonstrate skill in using networking commands accounting for endianness\. | bind\(\) |  | 3c |  |  |  |  |
| 247 |  |  |  | Programming | Python | Demonstrate skill in using networking commands accounting for endianness\. | listen\(\) |  | 3c |  |  |  |  |
| 248 |  |  |  | Programming | Python | Demonstrate skill in using networking commands accounting for endianness\. | connect\(\) |  | 3c |  |  |  |  |
| 249 |  |  |  | Programming | Python | Demonstrate skill in using networking commands accounting for endianness\. | accept\(\) |  | 3c |  |  |  |  |
| 250 |  |  |  | Programming | Python | Demonstrate skill in using networking commands accounting for endianness\. | close\(\) |  | 3c |  |  |  |  |
| 251 |  |  |  | Programming | Python | Demonstrate skill in using networking commands accounting for endianness\. | gethostname\(\) |  | 3c |  |  |  |  |
| 252 |  |  |  | Programming | C | Describe the purpose and use of the following C concepts\. | The main\(\) function |  | B |  |  |  |  |
| 253 |  |  |  | Programming | C | Describe the purpose and use of the following C concepts\. | the return statement |  | B |  |  |  |  |
| 254 |  |  |  | Programming | C | Describe the purpose and use of the following C concepts\. | Macro guards |  | B |  |  |  |  |
| 255 |  |  |  | Programming | C | Describe the purpose and use of the following C concepts\. | Data types |  | B |  |  |  |  |
| 256 |  |  |  | Programming | C | Describe the purpose and use of the following C concepts\. | A function |  | B |  |  |  |  |
| 257 |  |  |  | Programming | C | Describe the purpose and use of the following C concepts\. | Parameters |  | B |  |  |  |  |
| 258 |  |  |  | Programming | C | Describe the purpose and use of the following C concepts\. | Scope |  | B |  |  |  |  |
| 259 |  |  |  | Programming | C | Describe the purpose and use of the following C concepts\. | Return values \(return type and reference\) |  | B |  |  |  |  |
| 260 |  |  |  | Programming | C | Describe the purpose and use of the following C concepts\. | Header files |  | B |  |  |  |  |
| 261 |  |  |  | Programming | C | Describe the purpose and use of the following C concepts\. | Keywords \(static, extern, etc\) |  | B |  |  |  |  |
| 262 |  |  |  | Programming | C | Describe the purpose and use of the following C concepts\. | Pointers |  | B |  |  |  |  |
| 263 |  |  |  | Programming | C | Describe the purpose and use of the following C concepts\. | An array |  | B |  |  |  |  |
| 264 |  |  |  | Programming | C | Describe the purpose and use of the following C concepts\. | C pre\-processor |  | B |  |  |  |  |
| 265 |  |  |  | Programming | C | Describe the purpose and use of the following C concepts\. | Casting |  | B |  |  |  |  |
| 266 |  |  |  | Programming | C | Describe the purpose and use of the following C concepts\. | Control flow |  | B |  |  |  |  |
| 267 |  |  |  | Programming | C | Describe the purpose and use of the following C concepts\. | Endianness |  | B |  |  |  |  |
| 268 |  |  |  | Programming | C | Describe the purpose and use of the following C concepts\. | Multi\-threading |  | B |  |  |  |  |
| 269 |  |  |  | Programming | C | Describe the purpose and use of the following C concepts\. | Hashing |  | B |  |  |  |  |
| 270 |  |  |  | Programming | C | Demonstrate skill in using pointers\. | Declaring an integer pointer |  | 3c | In addition to the program package itself \(program plus comments, documentation, etc\) must PRESENT the functionality to an evaluator\. |  |  |  |
| 271 |  |  |  | Programming | C | Demonstrate skill in using pointers\. | Dereferencing a variable to get its value |  | 3c | In addition to the program package itself \(program plus comments, documentation, etc\) must PRESENT the functionality to an evaluator\. |  |  |  |
| 272 |  |  |  | Programming | C | Demonstrate skill in using pointers\. | Printing the address of the variable |  | 3c | In addition to the program package itself \(program plus comments, documentation, etc\) must PRESENT the functionality to an evaluator\. |  |  |  |
| 273 |  |  |  | Programming | C | Demonstrate skill in using pointers\. | Assigning a value to a pointer |  | 3c | In addition to the program package itself \(program plus comments, documentation, etc\) must PRESENT the functionality to an evaluator\. |  |  |  |
| 274 |  |  |  | Programming | C | Demonstrate skill in using pointers\. | Make use of a function pointer to call another function |  | 2b | In addition to the program package itself \(program plus comments, documentation, etc\) must PRESENT the functionality to an evaluator\. |  |  |  |
| 275 |  |  |  | Programming | C | Demonstrate skill in using pointers\. | Make effective use of pointer arithmetic to traverse an array |  | 3c | In addition to the program package itself \(program plus comments, documentation, etc\) must PRESENT the functionality to an evaluator\. |  |  |  |
| 276 |  |  |  | Programming | C | Demonstrate skill in creating an implementing conditional statements, expressions, and constructs\. | for loop |  | 3c | In addition to the program package itself \(program plus comments, documentation, etc\) must PRESENT the functionality to an evaluator\. |  |  |  |
| 277 |  |  |  | Programming | C | Demonstrate skill in creating an implementing conditional statements, expressions, and constructs\. | while loop |  | 3c | In addition to the program package itself \(program plus comments, documentation, etc\) must PRESENT the functionality to an evaluator\. |  |  |  |
| 278 |  |  |  | Programming | C | Demonstrate skill in creating an implementing conditional statements, expressions, and constructs\. | do while loop |  | 3c | In addition to the program package itself \(program plus comments, documentation, etc\) must PRESENT the functionality to an evaluator\. |  |  |  |
| 279 |  |  |  | Programming | C | Demonstrate skill in creating an implementing conditional statements, expressions, and constructs\. | if statement |  | 3c | In addition to the program package itself \(program plus comments, documentation, etc\) must PRESENT the functionality to an evaluator\. |  |  |  |
| 280 |  |  |  | Programming | C | Demonstrate skill in creating an implementing conditional statements, expressions, and constructs\. | if\->else statement |  | 3c | In addition to the program package itself \(program plus comments, documentation, etc\) must PRESENT the functionality to an evaluator\. |  |  |  |
| 281 |  |  |  | Programming | C | Demonstrate skill in creating an implementing conditional statements, expressions, and constructs\. | if\->else if\->else statement |  | 3c | In addition to the program package itself \(program plus comments, documentation, etc\) must PRESENT the functionality to an evaluator\. |  |  |  |
| 282 |  |  |  | Programming | C | Demonstrate skill in creating an implementing conditional statements, expressions, and constructs\. | switch statement |  | 3c | In addition to the program package itself \(program plus comments, documentation, etc\) must PRESENT the functionality to an evaluator\. |  |  |  |
| 283 |  |  |  | Programming | C | Demonstrate skill in creating an implementing conditional statements, expressions, and constructs\. | effective use of goto labels to construct a single exit point within a function |  | 3c | In addition to the program package itself \(program plus comments, documentation, etc\) must PRESENT the functionality to an evaluator\. |  |  |  |
| 284 |  |  |  | Programming | C | Demonstrate skill in compiling, linking, and debugging\. | Execute a program in a debugger to perform general debugging actions |  | 3c | Set a break point; observe values of variables; step through code; attach to running process \(Use Visual Studio Professional & Clion\); In addition to the program package itself \(program plus comments, documentation, etc\) must PRESENT the functionality to an evaluator\. |  |  |  |
| 285 |  |  |  | Programming | C | Demonstrate skill in compiling, linking, and debugging\. | Create a program using the compilation and linking process |  | 3c | In addition to the program package itself \(program plus comments, documentation, etc\) must PRESENT the functionality to an evaluator\. |  |  |  |
| 286 |  |  |  | Programming | C | Demonstrate skill in compiling, linking, and debugging\. | Compile position\-independent code using a cross\-compiler |  | 3c | In addition to the program package itself \(program plus comments, documentation, etc\) must PRESENT the functionality to an evaluator\. |  |  |  |
| 287 |  |  |  | Programming | C | Describe how and when bitwise operators are used\.  | Bitwise and \(&\) |  | B |  |  |  |  |
| 288 |  |  |  | Programming | C | Describe how and when bitwise operators are used\.  | Bitwise or \(&#124;\) |  | B |  |  |  |  |
| 289 |  |  |  | Programming | C | Describe how and when bitwise operators are used\.  | xor \(^\) |  | B |  |  |  |  |
| 290 |  |  |  | Programming | C | Describe how and when bitwise operators are used\.  | Bitwise complement \(~\) |  | B |  |  |  |  |
| 291 |  |  |  | Programming | C | Describe how and when bitwise operators are used\.  | Shift left \(<<\) |  | B |  |  |  |  |
| 292 |  |  |  | Programming | C | Describe how and when bitwise operators are used\.  | Shift right \(>>\) |  | B |  |  |  |  |
| 293 |  |  |  | Programming | C | Describe the following C programming concepts\. | Memory map of a C program |  | B | aka\. Heap is provided in virtual memory; stack grows up in memory map\. Given a scenario need to know where memory gets allocated |  |  |  |
| 294 |  |  |  | Programming | C | Describe the following C programming concepts\. | Stack |  | B | Given a scenario need to know where memory gets allocated |  |  |  |
| 295 |  |  |  | Programming | C | Describe the following C programming concepts\. | Heap |  | B | Given a scenario need to know where memory gets allocated |  |  |  |
| 296 |  |  |  | Programming | C | Describe the following C programming concepts\. | Static vs\. dynamic allocation |  | B | Given a scenario need to know where memory gets allocated |  |  |  |
| 297 |  |  |  | Programming | Bitwise Operators | Demonstrate skill using bitwise operators\. | Bitwise and \(&\) |  | 2b |  |  |  |  |
| 298 |  |  |  | Programming | Bitwise Operators | Demonstrate skill using bitwise operators\. | Bitwise or \(&#124;\) |  | 2b |  |  |  |  |
| 299 |  |  |  | Programming | Bitwise Operators | Demonstrate skill using bitwise operators\. | xor \(^\) |  | 2b |  |  |  |  |
| 300 |  |  |  | Programming | Bitwise Operators | Demonstrate skill using bitwise operators\. | Bitwise complement \(~\) |  | 2b |  |  |  |  |
| 301 |  |  |  | Programming | Bitwise Operators | Demonstrate skill using bitwise operators\. | Shift left \(<<\) |  | 2b |  |  |  |  |
| 302 |  |  |  | Programming | Bitwise Operators | Demonstrate skill using bitwise operators\. | Shift right \(>>\) |  | 2b |  |  |  |  |
| 303 |  |  |  | Programming | C | Demonstrate skill using the C preprocessor\. | Preprocessor |  | 3c | In addition to the program package itself \(program plus comments, documentation, etc\) must PRESENT the functionality to an evaluator\. |  |  |  |
| 304 |  |  |  | Programming | C | Demonstrate the proper declaration, understanding, and use of C data types, and underlying data structure\. | WORD |  | 3c | In addition to the program package itself \(program plus comments, documentation, etc\) must PRESENT the functionality to an evaluator\. |  |  |  |
| 305 |  |  |  | Programming | C | Demonstrate the proper declaration, understanding, and use of C data types, and underlying data structure\. | DWORD |  | 3c | In addition to the program package itself \(program plus comments, documentation, etc\) must PRESENT the functionality to an evaluator\. |  |  |  |
| 306 |  |  |  | Programming | C | Demonstrate the proper declaration, understanding, and use of C data types, and underlying data structure\. | QUADWORD |  | 3c | In addition to the program package itself \(program plus comments, documentation, etc\) must PRESENT the functionality to an evaluator\. |  |  |  |
| 307 |  |  |  | Programming | C | Demonstrate the proper declaration, understanding, and use of C data types, and underlying data structure\. | Short |  | 3c | In addition to the program package itself \(program plus comments, documentation, etc\) must PRESENT the functionality to an evaluator\. |  |  |  |
| 308 |  |  |  | Programming | C | Demonstrate the proper declaration, understanding, and use of C data types, and underlying data structure\. | Integer \(int\) |  | 3c | In addition to the program package itself \(program plus comments, documentation, etc\) must PRESENT the functionality to an evaluator\. |  |  |  |
| 309 |  |  |  | Programming | C | Demonstrate the proper declaration, understanding, and use of C data types, and underlying data structure\. | Float \(float\) |  | 3c | In addition to the program package itself \(program plus comments, documentation, etc\) must PRESENT the functionality to an evaluator\. |  |  |  |
| 310 |  |  |  | Programming | C | Demonstrate the proper declaration, understanding, and use of C data types, and underlying data structure\. | Character \(char\) |  | 3c | In addition to the program package itself \(program plus comments, documentation, etc\) must PRESENT the functionality to an evaluator\. |  |  |  |
| 311 |  |  |  | Programming | C | Demonstrate the proper declaration, understanding, and use of C data types, and underlying data structure\. | Double \(double\) |  | 3c | In addition to the program package itself \(program plus comments, documentation, etc\) must PRESENT the functionality to an evaluator\. |  |  |  |
| 312 |  |  |  | Programming | C | Demonstrate the proper declaration, understanding, and use of C data types, and underlying data structure\. | Long \(long\) |  | 3c | In addition to the program package itself \(program plus comments, documentation, etc\) must PRESENT the functionality to an evaluator\. |  |  |  |
| 313 |  |  |  | Programming | C | Demonstrate the ability to create and implement a function that uses different arrays\. | An array |  | 3c | In addition to the program package itself \(program plus comments, documentation, etc\) must PRESENT the functionality to an evaluator\. |  |  |  |
| 314 |  |  |  | Programming | C | Demonstrate the ability to create and implement a function that uses different arrays\. | A multi\-dimensional array |  | 3c | In addition to the program package itself \(program plus comments, documentation, etc\) must PRESENT the functionality to an evaluator\. |  |  |  |
| 315 |  |  |  | Programming | C | Demonstrate the ability to perform basic arithmetic operations using appropriate C operators while ensuring proper order of operations \(PEMDAS\)\. | Addition |  | 3c | More logical \(AND, OR\) operators also need to be covered\. In addition to the program package itself \(program plus comments, documentation, etc\) must PRESENT the functionality to an evaluator\. |  |  |  |
| 316 |  |  |  | Programming | C | Demonstrate the ability to perform basic arithmetic operations using appropriate C operators while ensuring proper order of operations \(PEMDAS\)\. | Subtraction |  | 3c | More logical \(AND, OR\) operators also need to be covered\. In addition to the program package itself \(program plus comments, documentation, etc\) must PRESENT the functionality to an evaluator\. |  |  |  |
| 317 |  |  |  | Programming | C | Demonstrate the ability to perform basic arithmetic operations using appropriate C operators while ensuring proper order of operations \(PEMDAS\)\. | Multiplication |  | 3c | More logical \(AND, OR\) operators also need to be covered\. In addition to the program package itself \(program plus comments, documentation, etc\) must PRESENT the functionality to an evaluator\. |  |  |  |
| 318 |  |  |  | Programming | C | Demonstrate the ability to perform basic arithmetic operations using appropriate C operators while ensuring proper order of operations \(PEMDAS\)\. | Division |  | 3c | More logical \(AND, OR\) operators also need to be covered\. In addition to the program package itself \(program plus comments, documentation, etc\) must PRESENT the functionality to an evaluator\. |  |  |  |
| 319 |  |  |  | Programming | C | Demonstrate the ability to perform basic arithmetic operations using appropriate C operators while ensuring proper order of operations \(PEMDAS\)\. | Modulus |  | 3c | More logical \(AND, OR\) operators also need to be covered\. In addition to the program package itself \(program plus comments, documentation, etc\) must PRESENT the functionality to an evaluator\. |  |  |  |
| 320 |  |  |  | Programming | C | Demonstrate the ability to perform basic arithmetic operations using appropriate C operators while ensuring proper order of operations \(PEMDAS\)\. | Increment |  | 3c | More logical \(AND, OR\) operators also need to be covered\. In addition to the program package itself \(program plus comments, documentation, etc\) must PRESENT the functionality to an evaluator\. |  |  |  |
| 321 |  |  |  | Programming | C | Demonstrate the ability to perform basic arithmetic operations using appropriate C operators while ensuring proper order of operations \(PEMDAS\)\. | Decrement |  | 3c | More logical \(AND, OR\) operators also need to be covered\. In addition to the program package itself \(program plus comments, documentation, etc\) must PRESENT the functionality to an evaluator\. |  |  |  |
| 322 |  |  |  | Programming | C | Demonstrate the ability to properly use the standard main\(\) entry arguments\. | int argc |  | 3c | In addition to the program package itself \(program plus comments, documentation, etc\) must PRESENT the functionality to an evaluator\. |  |  |  |
| 323 |  |  |  | Programming | C | Demonstrate the ability to properly use the standard main\(\) entry arguments\. | char \*\*argv |  | 3c | In addition to the program package itself \(program plus comments, documentation, etc\) must PRESENT the functionality to an evaluator\. |  |  |  |
| 324 |  |  |  | Programming | C | Demonstrate the ability to properly use the standard main\(\) entry arguments\. | char \*\*envp |  | 2b |  |  |  |  |
| 325 |  |  |  | Programming | C | Demonstrate the ability to perform file management operations in C\. | Open an existing file |  | 3c | In addition to the program package itself \(program plus comments, documentation, etc\) must PRESENT the functionality to an evaluator\. |  |  |  |
| 326 |  |  |  | Programming | C | Demonstrate the ability to perform file management operations in C\. | Read data from a file |  | 3c | In addition to the program package itself \(program plus comments, documentation, etc\) must PRESENT the functionality to an evaluator\. |  |  |  |
| 327 |  |  |  | Programming | C | Demonstrate the ability to perform file management operations in C\. | Write data to a file |  | 3c | In addition to the program package itself \(program plus comments, documentation, etc\) must PRESENT the functionality to an evaluator\. |  |  |  |
| 328 |  |  |  | Programming | C | Demonstrate the ability to perform file management operations in C\. | Modify data in a file |  | 3c | In addition to the program package itself \(program plus comments, documentation, etc\) must PRESENT the functionality to an evaluator\. |  |  |  |
| 329 |  |  |  | Programming | C | Demonstrate the ability to perform file management operations in C\. | Close and open a file |  | 3c | In addition to the program package itself \(program plus comments, documentation, etc\) must PRESENT the functionality to an evaluator\. |  |  |  |
| 330 |  |  |  | Programming | C | Demonstrate the ability to perform file management operations in C\. | Print file information to the console |  | 3c | In addition to the program package itself \(program plus comments, documentation, etc\) must PRESENT the functionality to an evaluator\. |  |  |  |
| 331 |  |  |  | Programming | C | Demonstrate the ability to perform file management operations in C\. | Create a new file |  | 3c | In addition to the program package itself \(program plus comments, documentation, etc\) must PRESENT the functionality to an evaluator\. |  |  |  |
| 332 |  |  |  | Programming | C | Demonstrate the ability to perform file management operations in C\. | Determine the size of a file |  | 3c | In addition to the program package itself \(program plus comments, documentation, etc\) must PRESENT the functionality to an evaluator\. |  |  |  |
| 333 |  |  |  | Programming | C | Demonstrate the ability to perform file management operations in C\. | Determine location within a file |  | 3c | In addition to the program package itself \(program plus comments, documentation, etc\) must PRESENT the functionality to an evaluator\. |  |  |  |
| 334 |  |  |  | Programming | C | Demonstrate the ability to perform file management operations in C\. | Insert data into an existing file |  | 3c | In addition to the program package itself \(program plus comments, documentation, etc\) must PRESENT the functionality to an evaluator\. |  |  |  |
| 335 |  |  |  | Programming | C | Demonstrate the ability to create and implement functions to meet a requirement\. | Proper declarations for created functions |  | 3c | In addition to the program package itself \(program plus comments, documentation, etc\) must PRESENT the functionality to an evaluator\. |  |  |  |
| 336 |  |  |  | Programming | C | Demonstrate the ability to create and implement functions to meet a requirement\. | A function that returns a void |  | 3c | In addition to the program package itself \(program plus comments, documentation, etc\) must PRESENT the functionality to an evaluator\. |  |  |  |
| 337 |  |  |  | Programming | C | Demonstrate the ability to create and implement functions to meet a requirement\. | A function that is passed an argument by value |  | 3c | In addition to the program package itself \(program plus comments, documentation, etc\) must PRESENT the functionality to an evaluator\. |  |  |  |
| 338 |  |  |  | Programming | C | Demonstrate the ability to create and implement functions to meet a requirement\. | A function that is passed an argument by reference |  | 3c | In addition to the program package itself \(program plus comments, documentation, etc\) must PRESENT the functionality to an evaluator\. |  |  |  |
| 339 |  |  |  | Programming | C | Demonstrate the ability to create and implement functions to meet a requirement\. | A function that returns a value using a return statement |  | 3c | In addition to the program package itself \(program plus comments, documentation, etc\) must PRESENT the functionality to an evaluator\. |  |  |  |
| 340 |  |  |  | Programming | C | Demonstrate the ability to create and implement functions to meet a requirement\. | A function that returns a value by reference |  | 3c | In addition to the program package itself \(program plus comments, documentation, etc\) must PRESENT the functionality to an evaluator\. |  |  |  |
| 341 |  |  |  | Programming | C | Demonstrate the ability to create and implement functions to meet a requirement\. | A function that receives input from a user |  | 3c | In addition to the program package itself \(program plus comments, documentation, etc\) must PRESENT the functionality to an evaluator\. |  |  |  |
| 342 |  |  |  | Programming | C | Demonstrate the ability to create and implement functions to meet a requirement\. | A function pointer |  | 2b |  |  |  |  |
| 343 |  |  |  | Programming | C | Demonstrate the ability to create and implement functions to meet a requirement\. | A recursive function |  | 2b |  |  |  |  |
| 344 |  |  |  | Programming | C | Describe the following C programming concepts\. | Error handling |  | B | Add to 3\.1\.1 in JQR; not compile errors or logic errors\.\.\. try\-catch, making sure your program can handle errors |  |  |  |
| 345 |  |  |  | Software | Secure Coding | Conduct error handling | Error handling |  | 2b | Add to 3\.1\.1 in JQR; not compile errors or logic errors\.\.\. try\-catch, making sure your program can handle errors |  |  |  |
| 346 |  |  |  | Programming | C | Describe the following C programming concepts\. | Struct |  | B | Add to 3\.1\.1 in JQR |  |  |  |
| 347 |  |  |  | Programming | C | Describe the following C programming concepts\. | Union |  | B | Add to 3\.1\.1 in JQR |  |  |  |
| 348 |  |  |  | Programming | C | Describe the following C programming concepts\. | Enums |  | B | Add to 3\.1\.1 in JQR |  |  |  |
| 349 |  |  |  | Programming | C | Describe the following C programming concepts\. | TypeDef |  | B | Add to 3\.1\.1 in JQR |  |  |  |
| 350 |  |  |  | Programming | C | Describe the following C programming concepts\. | Strings |  | B | Add to 3\.1\.1 in JQR |  |  |  |
| 351 |  |  |  | Programming | C | Demonstrate skill in using logical operator\. | Logical and \(&&\) |  | 3c | In addition to the program package itself \(program plus comments, documentation, etc\) must PRESENT the functionality to an evaluator\. |  |  |  |
| 352 |  |  |  | Programming | C | Demonstrate skill in using logical operator\. | Logical or \(&#124;&#124;\) |  | 3c | In addition to the program package itself \(program plus comments, documentation, etc\) must PRESENT the functionality to an evaluator\. |  |  |  |
| 353 |  |  |  | Programming | C | Demonstrate skill in using logical operator\. | Logical not \(\!\) |  | 3c | In addition to the program package itself \(program plus comments, documentation, etc\) must PRESENT the functionality to an evaluator\. |  |  |  |
| 354 |  |  |  | Programming | C | Demonstrate skill in using logical operator\. | Logical greater than \(>\) |  | 3c | In addition to the program package itself \(program plus comments, documentation, etc\) must PRESENT the functionality to an evaluator\. |  |  |  |
| 355 |  |  |  | Programming | C | Demonstrate skill in using logical operator\. | Logical greater than or equal to \(>=\) |  | 3c | In addition to the program package itself \(program plus comments, documentation, etc\) must PRESENT the functionality to an evaluator\. |  |  |  |
| 356 |  |  |  | Programming | C | Demonstrate skill in using logical operator\. | Logical less than \(<\) |  | 3c | In addition to the program package itself \(program plus comments, documentation, etc\) must PRESENT the functionality to an evaluator\. |  |  |  |
| 357 |  |  |  | Programming | C | Demonstrate skill in using logical operator\. | Logical less than or equal to \(<=\) |  | 3c | In addition to the program package itself \(program plus comments, documentation, etc\) must PRESENT the functionality to an evaluator\. |  |  |  |
| 358 |  |  |  | Programming | C | Demonstrate skill in using logical operator\. | Equals to \(==\) |  | 3c | In addition to the program package itself \(program plus comments, documentation, etc\) must PRESENT the functionality to an evaluator\. |  |  |  |
| 359 |  |  |  | Programming | C | Demonstrate skill in using logical operator\. | Not equals to \(\!=\) |  | 3c | In addition to the program package itself \(program plus comments, documentation, etc\) must PRESENT the functionality to an evaluator\. |  |  |  |
| 360 |  |  |  | Software | Unicode | Describe the storage considerations of using Unicode |  |  | B |  |  |  |  |
| 361 |  |  |  | Programming | Pseudocode | Pseudocode and logic |  |  | B | i\.e\. Critical thinking to Pseudocode through a problem |  |  |  |
| 367 |  |  |  | Programming | C | Demonstrate skill in debugging a local application in an IDE\. | Set breakpoints\. |  | 2c |  |  |  |  |
| 368 |  |  |  | Programming | C | Demonstrate skill in debugging a local application in an IDE\. | Step through lines of code\. |  | 2c |  |  |  |  |
| 369 |  |  |  | Programming | C | Demonstrate skill in debugging a local application in an IDE\. | View the value of variables\. |  | 2c |  |  |  |  |
| 370 |  |  |  | Programming | C | Demonstrate skill in debugging a remote application in an IDE\. | Setup remote debugging\. |  | 2c |  |  |  |  |
| 371 |  |  |  | Programming | C | Demonstrate skill in debugging a local application in a command line debugger\. | Set breakpoints\. |  | 2c |  |  |  |  |
| 372 |  |  |  | Programming | C | Demonstrate skill in debugging a local application in a command line debugger\. | Step through lines of code\. |  | 2c |  |  |  |  |
| 373 |  |  |  | Programming | C | Demonstrate skill in debugging a local application in a command line debugger\. | View the value of variables\. |  | 2c |  |  |  |  |
| 374 |  |  |  | Programming | C | Demonstrate skill in debugging a local application in a command line debugger\. | View the contents of the stack and heap\. |  | 2c |  |  |  |  |
| 375 |  |  |  | Programming | C | Demonstrate skill in debugging a remote application in a command line debugger\. | Setup remote debugging\. |  | 2c |  |  |  |  |
| 376 |  |  |  | Programming | Python | Demonstrate skill in debugging a local application in an IDE\. | Set breakpoints\. |  | 2c |  |  |  |  |
| 377 |  |  |  | Programming | Python | Demonstrate skill in debugging a local application in an IDE\. | Step through lines of code\. |  | 2c |  |  |  |  |
| 378 |  |  |  | Programming | Python | Demonstrate skill in debugging a local application in an IDE\. | View the value of variables\. |  | 2c |  |  |  |  |
| 379 |  |  |  | Programming | Python | Demonstrate skill in debugging a remote application in an IDE\. | Setup remote debugging\. |  | 2c |  |  |  |  |
| 380 |  |  |  | Programming | Python | Demonstrate skill in debugging a local application in a command line debugger\. | Set breakpoints\. |  | 2c |  |  |  |  |
| 381 |  |  |  | Programming | Python | Demonstrate skill in debugging a local application in a command line debugger\. | Step through lines of code\. |  | 2c |  |  |  |  |
| 382 |  |  |  | Programming | Python | Demonstrate skill in debugging a local application in a command line debugger\. | View the value of variables\. |  | 2c |  |  |  |  |
| 383 |  |  |  | Programming | Python | Demonstrate skill in debugging a local application in a command line debugger\. | View the contents of the stack and heap\. |  | 2c |  |  |  |  |
| 384 |  |  |  | Programming | Python | Demonstrate skill in debugging a remote application in a command line debugger\. | Setup remote debugging\. |  | 2c |  |  |  |  |
| 389 |  |  |  | Software | Algorithms | Demonstrate the ability to calculate runtime efficiency for a given algorithm using Big\-O notation\. | Asymptotic notation \(Big\-O\) |  | B | Given scenario know when or when not to use certain algorithms due to processing time |  |  |  |
| 390 |  |  |  | Software | Algorithms | Demonstrate the ability to calculate runtime efficiency for a given algorithm using Big\-O notation\. | Insertion Sort |  | B |  |  |  |  |
| 391 |  |  |  | Software | Algorithms | Demonstrate the ability to calculate runtime efficiency for a given algorithm using Big\-O notation\. | Selection Sort |  | B |  |  |  |  |
| 392 |  |  |  | Software | Algorithms | Demonstrate the ability to calculate runtime efficiency for a given algorithm using Big\-O notation\. | Merge Sort |  | B |  |  |  |  |
| 393 |  |  |  | Software | Algorithms | Demonstrate the ability to calculate runtime efficiency for a given algorithm using Big\-O notation\. | Heap Sort |  | B |  |  |  |  |
| 394 |  |  |  | Software | Algorithms | Demonstrate the ability to calculate runtime efficiency for a given algorithm using Big\-O notation\. | Quick Sort |  | B |  |  |  |  |
| 395 |  |  |  | Software | Algorithms | Demonstrate the ability to calculate runtime efficiency for a given algorithm using Big\-O notation\. | State Machine |  | B | Doesn't really make sense, but ok |  |  |  |
| 396 |  |  |  | Software | Algorithms | Demonstrate the ability to calculate runtime efficiency for a given algorithm using Big\-O notation\. | Hashing |  | B |  |  |  |  |
| 397 |  |  |  | Software | Transversal | Describe the concepts associated with traversal techniques\. | Depth first traversal |  | B |  |  |  |  |
| 398 |  |  |  | Software | Transversal | Describe the concepts associated with traversal techniques\. | Breadth first traversal |  | B |  |  |  |  |
| 399 |  |  |  | Software | Transversal | Describe the concepts associated with traversal techniques\. | The technique of determining the weight of a given path |  | B |  |  |  |  |
| 400 |  |  |  | Software | Transversal | Describe the concepts associated with traversal techniques\. | How the most efficient path for traversing a graph is determined |  | B |  |  |  |  |
| 401 |  |  |  | Software | Hashing | Describe the concepts associated with hashing\. | Data distribution as it relates to hashing |  | B | Hashing for data structures \(not cryptographic hashing\) |  |  |  |
| 402 |  |  |  | Software | Hashing | Describe the concepts associated with hashing\. | Hash function efficiency |  | B | Hashing for data structures \(not cryptographic hashing\) |  |  |  |
| 403 |  |  |  | Software | Hashing | Describe the concepts associated with hashing\. | Hash collisions |  | B | Hashing for data structures \(not cryptographic hashing\) |  |  |  |
| 404 |  |  |  | Software | Software Engineering | Demonstrate the ability to analyze sorting routines to determine the most efficient one to use, using an approximation of Big\-O notation |  |  | 2b | Given a piece of code calculate the Big\-O for that code |  |  |  |
| 405 |  |  |  | Programming | C | Describe the concepts and terms associated with the following data structures\. | Binary search tree |  | B |  |  |  |  |
| 406 |  |  |  | Programming | C | Describe the concepts and terms associated with the following data structures\. | Linked list |  | B |  |  |  |  |
| 407 |  |  |  | Programming | C | Describe the concepts and terms associated with the following data structures\. | Doubly linked list |  | B |  |  |  |  |
| 408 |  |  |  | Programming | C | Describe the concepts and terms associated with the following data structures\. | Circularly linked list |  | B |  |  |  |  |
| 409 |  |  |  | Programming | C | Describe the concepts and terms associated with the following data structures\. | Weighted Graph |  | B |  |  |  |  |
| 410 |  |  |  | Programming | C | Describe the concepts and terms associated with the following data structures\. | Common pitfalls when using linked lists, queues, trees, and graphs |  | B |  |  |  |  |
| 411 |  |  |  | Programming | C | Describe the concepts and terms associated with the following data structures\. | The effect of First in First Out \(FIFO\) and Last in First Out \(LIFO\) |  | B |  |  |  |  |
| 412 |  |  |  | Programming | C | Demonstrate skill in creating and using a weighted graph\. | Defining the structures required for graphs |  | 2b |  |  |  |  |
| 413 |  |  |  | Programming | C | Demonstrate skill in creating and using a weighted graph\. | Creating a graph with n number of nodes |  | 2b |  |  |  |  |
| 414 |  |  |  | Programming | C | Demonstrate skill in creating and using a weighted graph\. | Adding n number of edges to a graph |  | 2b |  |  |  |  |
| 415 |  |  |  | Programming | C | Demonstrate skill in creating and using a weighted graph\. | Finding a node within an existing graph |  | 2b |  |  |  |  |
| 416 |  |  |  | Programming | C | Demonstrate skill in creating and using a weighted graph\. | Finding an edge within a graph |  | 2b |  |  |  |  |
| 417 |  |  |  | Programming | C | Demonstrate skill in creating and using a weighted graph\. | Remove an edge from a graph |  | 2b |  |  |  |  |
| 418 |  |  |  | Programming | C | Demonstrate skill in creating and using a weighted graph\. | Remove a node and all of its edges from a graph |  | 2b |  |  |  |  |
| 419 |  |  |  | Programming | C | Demonstrate skill in creating and using a weighted graph\. | Calculate the weight of a path within a graph |  | 2b |  |  |  |  |
| 420 |  |  |  | Programming | C | Demonstrate skill in creating and using a weighted graph\. | Destroy the graph |  | 2b |  |  |  |  |
| 421 |  |  |  | Programming | C | Demonstrate skill in creating and using a linked list\. | Creating a linked list with n number of items |  | 3c | C and python |  |  |  |
| 422 |  |  |  | Programming | C | Demonstrate skill in creating and using a linked list\. | Navigating through a linked list |  | 3c | C and python |  |  |  |
| 423 |  |  |  | Programming | C | Demonstrate skill in creating and using a linked list\. | Retrieving the first occurrence of an item in a linked list |  | 3c | C and python |  |  |  |
| 424 |  |  |  | Programming | C | Demonstrate skill in creating and using a linked list\. | Sorting the linked list alphanumerically using a function pointer |  | 3c | C and python |  |  |  |
| 425 |  |  |  | Programming | C | Demonstrate skill in creating and using a linked list\. | Removing selected items from the linked list |  | 3c | C and python |  |  |  |
| 426 |  |  |  | Programming | C | Demonstrate skill in creating and using a linked list\. | Inserting an item into a specific location in a linked list |  | 3c | C and python |  |  |  |
| 427 |  |  |  | Programming | C | Demonstrate skill in creating and using a linked list\. | Destroying a linked list |  | 3c | C and python |  |  |  |
| 428 |  |  |  | Programming | C | Demonstrate skill in creating an using a doubly linked list\. | Creating a doubly linked list with n number of items |  | 3c | C and python |  |  |  |
| 429 |  |  |  | Programming | C | Demonstrate skill in creating an using a doubly linked list\. | Navigating through a doubly linked list |  | 3c | C and python |  |  |  |
| 430 |  |  |  | Programming | C | Demonstrate skill in creating an using a doubly linked list\. | Finding the first occurrence of an item in a doubly linked list |  | 3c | C and python |  |  |  |
| 431 |  |  |  | Programming | C | Demonstrate skill in creating an using a doubly linked list\. | Sorting the doubly linked list alphanumerically list using a function pointer |  | 3c | C and python |  |  |  |
| 432 |  |  |  | Programming | C | Demonstrate skill in creating an using a doubly linked list\. | Removing selected items from the doubly linked list |  | 3c | C and python |  |  |  |
| 433 |  |  |  | Programming | C | Demonstrate skill in creating an using a doubly linked list\. | Inserting an item into a specified location in a doubly linked list |  | 3c | C and python |  |  |  |
| 434 |  |  |  | Programming | C | Demonstrate skill in creating an using a doubly linked list\. | Removing all items from a doubly linked list |  | 3c | C and python |  |  |  |
| 435 |  |  |  | Programming | C | Demonstrate skill in creating an using a doubly linked list\. | Destroying a doubly linked list |  | 3c | C and python |  |  |  |
| 436 |  |  |  | Programming | C | Demonstrate skill in creating and using a circularly linked list\. | Creating a circularly linked list with n number of items |  | 2b | C and python |  |  |  |
| 437 |  |  |  | Programming | C | Demonstrate skill in creating and using a circularly linked list\. | Navigating through a circularly linked list |  | 2b | C and python |  |  |  |
| 438 |  |  |  | Programming | C | Demonstrate skill in creating and using a circularly linked list\. | Finding the first occurrence of an item in a circularly linked list |  | 2b | C and python |  |  |  |
| 439 |  |  |  | Programming | C | Demonstrate skill in creating and using a circularly linked list\. | Sorting the circularly linked list alphanumerically list using a function pointer |  | 2b | C and python |  |  |  |
| 440 |  |  |  | Programming | C | Demonstrate skill in creating and using a circularly linked list\. | Removing selected items from the circularly linked list |  | 2b | C and python |  |  |  |
| 441 |  |  |  | Programming | C | Demonstrate skill in creating and using a circularly linked list\. | Inserting an item into a specified location in a circularly linked list |  | 2b | C and python |  |  |  |
| 442 |  |  |  | Programming | C | Demonstrate skill in creating and using a circularly linked list\. | Removing all items from a circularly linked list |  | 2b | C and python |  |  |  |
| 443 |  |  |  | Programming | C | Demonstrate skill in creating and using a circularly linked list\. | Destroying a circularly linked list |  | 2b | C and python |  |  |  |
| 444 |  |  |  | Programming | C | Demonstrate skill in creating and using a circularly linked list\. | Creating a circularly linked list with n number of items\. |  | 2b | C and python |  |  |  |
| 445 |  |  |  | Programming | C | Demonstrate skill in creating and using a queue\. | Creating a queue with n number of items |  | 3c | C and python |  |  |  |
| 446 |  |  |  | Programming | C | Demonstrate skill in creating and using a queue\. | Navigating through the queue to find the nth item |  | 3c | C and python |  |  |  |
| 447 |  |  |  | Programming | C | Demonstrate skill in creating and using a queue\. | Finding an item in the queue |  | 3c | C and python |  |  |  |
| 448 |  |  |  | Programming | C | Demonstrate skill in creating and using a queue\. | Removing selected items from a queue |  | 3c | C and python |  |  |  |
| 449 |  |  |  | Programming | C | Demonstrate skill in creating and using a queue\. | Removing all items from the queue |  | 3c | C and python |  |  |  |
| 450 |  |  |  | Programming | C | Demonstrate skill in creating and using a queue\. | Destroying a queue |  | 3c | C and python |  |  |  |
| 451 |  |  |  | Programming | C | Demonstrate skill in creating and using a tree\. | Creating a tree with n number of items |  | 2b |  |  |  |  |
| 452 |  |  |  | Programming | C | Demonstrate skill in creating and using a tree\. | Navigating through a tree |  | 2b |  |  |  |  |
| 453 |  |  |  | Programming | C | Demonstrate skill in creating and using a tree\. | Finding the first occurrence of an item in a tree |  | 2b |  |  |  |  |
| 454 |  |  |  | Programming | C | Demonstrate skill in creating and using a tree\. | Removing selected items from the tree |  | 2b |  |  |  |  |
| 455 |  |  |  | Programming | C | Demonstrate skill in creating and using a tree\. | Inserting an item into a specified location in a tree |  | 2b |  |  |  |  |
| 456 |  |  |  | Programming | C | Demonstrate skill in creating and using a tree\. | Removing all items from the tree |  | 2b |  |  |  |  |
| 457 |  |  |  | Programming | C | Demonstrate skill in creating and using a tree\. | Destroying a tree |  | 2b |  |  |  |  |
| 458 |  |  |  | Programming | C | Demonstrate skill in creating and using a binary search tree\. | Creating a binary search tree with n number of items |  | 2b |  |  |  |  |
| 459 |  |  |  | Programming | C | Demonstrate skill in creating and using a binary search tree\. | Navigating through a binary search tree |  | 2b |  |  |  |  |
| 460 |  |  |  | Programming | C | Demonstrate skill in creating and using a binary search tree\. | Locating an item in a binary tree |  | 2b |  |  |  |  |
| 461 |  |  |  | Programming | C | Demonstrate skill in creating and using a binary search tree\. | Removing selected items from the binary search tree |  | 2b |  |  |  |  |
| 462 |  |  |  | Programming | C | Demonstrate skill in creating and using a binary search tree\. | Removing all items from the binary search tree |  | 2b |  |  |  |  |
| 463 |  |  |  | Programming | C | Demonstrate skill in creating and using a binary search tree\. | Describe the implementation strategies for a balanced binary search tree |  | 2b |  |  |  |  |
| 464 |  |  |  | Programming | C | Demonstrate skill in creating and using a binary search tree\. | Destroying a binary search tree |  | 2b |  |  |  |  |
| 465 |  |  |  | Programming | C | Demonstrate skill in creating and using a hash table\. | Creating a hash table with n number of items |  | 2b |  |  |  |  |
| 466 |  |  |  | Programming | C | Demonstrate skill in creating and using a hash table\. | Navigating through a hash table to find the nth item |  | 2b |  |  |  |  |
| 467 |  |  |  | Programming | C | Demonstrate skill in creating and using a hash table\. | Finding an item in a hash table |  | 2b |  |  |  |  |
| 468 |  |  |  | Programming | C | Demonstrate skill in creating and using a hash table\. | Removing selected items from a hash table |  | 2b |  |  |  |  |
| 469 |  |  |  | Programming | C | Demonstrate skill in creating and using a hash table\. | Inserting an item into a hash table |  | 2b |  |  |  |  |
| 470 |  |  |  | Programming | C | Demonstrate skill in creating and using a hash table\. | Implement functionality to mitigate hash collisions within the hash table |  | 2b |  |  |  |  |
| 471 |  |  |  | Programming | C | Demonstrate skill in creating and using a hash table\. | Removing all items from the hash table |  | 2b |  |  |  |  |
| 472 |  |  |  | Programming | C | Demonstrate skill in creating and using a stack\. | Creating a stack with n number of items |  | 2b |  |  |  |  |
| 473 |  |  |  | Programming | C | Demonstrate skill in creating and using a stack\. | Navigating through a stack to find the nth item |  | 2b |  |  |  |  |
| 474 |  |  |  | Programming | C | Demonstrate skill in creating and using a stack\. | Adding an item to the stack |  | 2b |  |  |  |  |
| 475 |  |  |  | Programming | C | Demonstrate skill in creating and using a stack\. | Removing selected items from the stack |  | 2b |  |  |  |  |
| 476 |  |  |  | Programming | C | Demonstrate skill in creating and using a stack\. | Removing all items from the stack |  | 2b |  |  |  |  |
| 477 |  |  |  | Programming | C | Demonstrate skill in creating and using a stack\. | Destroying the stack |  | 2b |  |  |  |  |
| 478 |  |  |  | Programming | C | Demonstrate skill in creating and using a stack\. | Preventing a stack overrun |  | 2b |  |  |  |  |
| 479 |  |  |  | Programming | C | With references and required resources, describe terms associated with compiling, linking, debugging, and executables\. | Portable executable \(PE\) |  | B |  |  |  |  |
| 480 |  |  |  | Programming | C | With references and required resources, describe terms associated with compiling, linking, debugging, and executables\. | Executable and Linkable Format \(ELF\) |  | B |  |  |  |  |
| 481 |  |  |  | Programming | C | With references and required resources, describe terms associated with compiling, linking, debugging, and executables\. | Difference between PE and ELF |  | B |  |  |  |  |
| 482 |  |  |  | Programming | C | With references and required resources, describe terms associated with compiling, linking, debugging, and executables\. | Difference between a library and a regular executable program |  | B |  |  |  |  |
| 483 |  |  |  | Programming | C | With references and required resources, describe terms associated with compiling, linking, debugging, and executables\. | Calling convention/Application Binary Interface \(ABI\) |  | B |  |  |  |  |
| 484 |  |  |  | Programming | Visual Studio | Utilize Visual studio to modify existing dll, and command line projects\. |  |  | 2b |  |  |  |  |
| 485 |  |  |  | Programming |  | Import an existing dll and utilize within a project\. |  |  | 2b |  |  |  |  |
| 486 |  |  |  | Programming |  | Import an existing shared object and utilize within a project\. |  |  | 2b |  |  |  |  |
| 487 |  |  |  | Programming | Linux | Create shared object \(Linux\) and utilize within a project\. |  |  | 2b |  |  |  |  |
| 488 |  |  |  | Programming |  | Import an existing shared object and utilize within a project\. |  |  | 2b |  |  |  |  |
| 489 |  |  |  | Programming |  | Build and utilize a static library |  |  | 2b |  |  |  |  |
| 490 |  |  |  | Programming |  | Build a statically linked program |  |  | 3c |  |  |  |  |
| 491 |  |  |  | Programming | JSON | Demonstrate the ability to utilize the standard library | JSON |  | 2b |  |  |  |  |
| 492 |  |  |  | Programming | C | Demonstrate the ability to handle partial reads/writes during serialization and de\-serialization\. |  |  | 2b |  |  |  |  |
| 493 |  |  |  | Programming | C | Demonstrate the ability to serialize fixed size multi\-byte types between systems of differing endianness\. |  |  | 2c |  |  |  |  |
| 494 |  |  |  | Programming | C | Demonstrate the ability to serialize and de\-serialize variable sized data structures between systems of differing endianness\. |  |  | 2c |  |  |  |  |
| 495 |  |  |  | Programming | C | Describe the libraries commonly used to aid in serialization\. |  |  | B | protobuf |  |  |  |
| 496 |  |  |  | Programming | C | Demonstrate skill in using networking commands accounting for endianness\. | socket\(\) |  | 3c |  |  |  |  |
| 497 |  |  |  | Programming | C | Demonstrate skill in using networking commands accounting for endianness\. | send\(\) |  | 3c |  |  |  |  |
| 498 |  |  |  | Programming | C | Demonstrate skill in using networking commands accounting for endianness\. | recv\(\) |  | 3c |  |  |  |  |
| 499 |  |  |  | Programming | C | Demonstrate skill in using networking commands accounting for endianness\. | sendto\(\) |  | 3c |  |  |  |  |
| 500 |  |  |  | Programming | C | Demonstrate skill in using networking commands accounting for endianness\. | recvfrom\(\) |  | 3c |  |  |  |  |
| 501 |  |  |  | Programming | C | Demonstrate skill in using networking commands accounting for endianness\. | bind\(\) |  | 3c |  |  |  |  |
| 502 |  |  |  | Programming | C | Demonstrate skill in using networking commands accounting for endianness\. | connect\(\) |  | 3c |  |  |  |  |
| 503 |  |  |  | Programming | C | Demonstrate skill in using networking commands accounting for endianness\. | accept\(\) |  | 3c |  |  |  |  |
| 504 |  |  |  | Programming | C | Demonstrate skill in using networking commands accounting for endianness\. | getsockopt\(\) |  | 3c |  |  |  |  |
| 505 |  |  |  | Programming | C | Demonstrate skill in using networking commands accounting for endianness\. | setsockopt\(\) |  | 3c |  |  |  |  |
| 506 |  |  |  | Programming | C | Demonstrate skill in using networking commands accounting for endianness\. | getaddrinfo\(\) |  | 3c |  |  |  |  |
| 507 |  |  |  | Programming | C | Demonstrate skill in using networking commands accounting for endianness\. | gethostname |  | 3c |  |  |  |  |
| 508 |  |  |  | Programming | C | Demonstrate skill in using networking commands accounting for endianness\. | sethostname |  | 3c |  |  |  |  |
| 509 |  |  |  | Programming | C | Demonstrate skill in using networking commands accounting for endianness\. | struct sockaddr\{\} |  | 3c |  |  |  |  |
| 510 |  |  |  | Programming | C | Demonstrate skill in using networking commands accounting for endianness\. | struct sockaddr\_in\{\} |  | 3c |  |  |  |  |
| 511 |  |  |  | Programming | C | Demonstrate skill in using networking commands accounting for endianness\. | struct sockaddr\_un\{\} |  | 3c |  |  |  |  |
| 512 |  |  |  | Network | Networking Concepts | Basic Networking | Demonstrate skill in identifying bugs in socket programs using Wireshark |  | 2b |  |  |  |  |
| 513 |  |  |  | Software | OS | Describe the terms and concepts associated with OS Virtualization\. | Processes |  | A | Replace "OS Virtualization" with "Computer Architecture" |  |  |  |
| 514 |  |  |  | Software | OS | Describe the terms and concepts associated with OS Virtualization\. | CPU Scheduling |  | A | Replace "OS Virtualization" with "Computer Architecture" |  |  |  |
| 515 |  |  |  | Software | OS | Describe the terms and concepts associated with OS Virtualization\. | Paging tables |  | A | Replace "OS Virtualization" with "Computer Architecture" |  |  |  |
| 516 |  |  |  | Software | OS | Describe the terms and concepts associated with OS Virtualization\. | Caching |  | A | Replace "OS Virtualization" with "Computer Architecture" |  |  |  |
| 517 |  |  |  | Software | OS | Describe the terms and concepts associated with OS Virtualization\. | Kernel and user\-mode memory |  | A | Replace "OS Virtualization" with "Computer Architecture" |  |  |  |
| 518 |  |  |  | Software | Virtualization | Demonstrate the ability to use the following constructs associated with OS Virtualization\. | Interrupts |  | 2b | Replace "OS Virtualization" with "Computer Architecture"; writing code that implements interrupt; not sure what is meant by interrupt\.\.\. process interrupt?\.\.\.cliff notes to the dinosaur book |  |  |  |
| 519 |  |  |  | Software | Virtualization | Demonstrate the ability to use the following constructs associated with OS Virtualization\. | Signal handling |  | 2b | Replace "OS Virtualization" with "Computer Architecture"; writing code that implements signal handling\.\.\.cliff notes to the dinosaur book |  |  |  |
| 520 |  |  |  | Software | OS | Describe the terms and concepts associated with OS Concurrency\. | Threading |  | B |  |  |  |  |
| 521 |  |  |  | Software | OS | Describe the terms and concepts associated with OS Concurrency\. | Locks |  | B |  |  |  |  |
| 522 |  |  |  | Software | OS | Describe the terms and concepts associated with OS Concurrency\. | Race conditions |  | B |  |  |  |  |
| 523 |  |  |  | Software | OS | Describe the terms and concepts associated with OS Concurrency\. | Deadlocks |  | B |  |  |  |  |
| 524 |  |  |  | Software | OS | Describe the terms and concepts associated with OS Concurrency\. | Scheduling modules \(e\.g\. round\-robin, etc\) |  | B |  |  |  |  |
| 525 |  |  |  | Software | OS | Demonstrate the ability to use the following constructs associated with OS Concurrency\. | Threads |  | 2b |  |  |  |  |
| 526 |  |  |  | Software | OS | Demonstrate the ability to use the following constructs associated with OS Concurrency\. | Locks |  | 2b |  |  |  |  |
| 527 |  |  |  | Software | OS | Describe the terms and concepts associated with OS Persistence\. | Von Nuemann Architecture |  | A | Data persistence not CNO persistence |  |  |  |
| 528 |  |  |  | Software | OS | Describe the terms and concepts associated with OS Persistence\. | Harvard Architecture |  | A | Data persistence not CNO persistence |  |  |  |
| 529 |  |  |  | Software | OS | Describe the terms and concepts associated with OS Persistence\. | File Systems |  | B | Data persistence not CNO persistence |  |  |  |
| 530 |  |  |  | Software | OS | Describe the terms and concepts associated with OS Persistence\. | The boot process |  | A | Data persistence not CNO persistence |  |  |  |
| 531 |  |  |  | Software | Linux | Understand Inter Process Communication \(IPC\) fundamentals in Linux | Shared Memory |  | B | Both Windows and Linux |  |  |  |
| 532 |  |  |  | Software | Linux | Understand Inter Process Communication \(IPC\) fundamentals in Linux | Pipes |  | B | Both Windows and Linux |  |  |  |
| 533 |  |  |  | Software | OS | Describe the terms and concepts associated with OS Architecture | Memory Hierarchy |  | B |  |  |  |  |
| 534 |  |  |  | Software | OS | Describe the terms and concepts associated with OS Architecture | Shared Memory |  | B |  |  |  |  |
| 535 |  |  |  | Software | OS | Describe the terms and concepts associated with OS Architecture | Virtual Memory |  | B |  |  |  |  |
| 536 |  |  |  | Software | OS | Describe the terms and concepts associated with OS Architecture | Virtualization Concepts |  | B |  |  |  |  |
| 537 |  |  |  | Programming | C | Describe the concepts and terminology associated with multi\-threaded programs\. | Thread |  | B |  |  |  |  |
| 538 |  |  |  | Programming | C | Describe the concepts and terminology associated with multi\-threaded programs\. | Pthread |  | B |  |  |  |  |
| 539 |  |  |  | Programming | C | Describe the concepts and terminology associated with multi\-threaded programs\. | Fork |  | B |  |  |  |  |
| 540 |  |  |  | Programming | C | Describe the concepts and terminology associated with multi\-threaded programs\. | Join |  | B |  |  |  |  |
| 541 |  |  |  | Programming | C | Describe the concepts and terminology associated with multi\-threaded programs\. | Create |  | B |  |  |  |  |
| 542 |  |  |  | Programming | C | Describe the concepts and terminology associated with multi\-threaded programs\. | Exit |  | B |  |  |  |  |
| 543 |  |  |  | Programming | C | Describe the concepts and terminology associated with multi\-threaded programs\. | Detach |  | B |  |  |  |  |
| 544 |  |  |  | Programming | C | Describe the concepts and terminology associated with multi\-threaded programs\. | Mutex |  | B |  |  |  |  |
| 545 |  |  |  | Programming | C | Describe the concepts and terminology associated with multi\-threaded programs\. | Semaphore |  | B |  |  |  |  |
| 546 |  |  |  | Programming | C | Describe the concepts and terminology associated with multi\-threaded programs\. | Race condition |  | B |  |  |  |  |
| 547 |  |  |  | Programming | C | Describe the concepts and terminology associated with multi\-threaded programs\. | Deadlock |  | B |  |  |  |  |
| 548 |  |  |  | Programming | C | Describe the concepts and terminology associated with multi\-threaded programs\. | Thread safe |  | B |  |  |  |  |
| 549 |  |  |  | Programming | C | Describe the concepts and terminology associated with multi\-threaded programs\. | Thread id |  | B |  |  |  |  |
| 550 |  |  |  | Programming | C | Demonstrate the ability to manage memory in multi\-threaded programs that make effective use of multithreaded programming constructs\. | threads |  | 2c |  |  |  |  |
| 551 |  |  |  | Programming | C | Demonstrate the ability to manage memory in multi\-threaded programs that make effective use of multithreaded programming constructs\. | semaphores |  | 2c |  |  |  |  |
| 552 |  |  |  | Programming | C | Demonstrate the ability to manage memory in multi\-threaded programs that make effective use of multithreaded programming constructs\. | mutexes |  | 2c |  |  |  |  |
| 553 |  |  |  | Programming | C | Demonstrate the ability to perform data validation\. | Validating input receives matches input expected\. |  | 2c |  |  |  |  |
| 554 |  |  |  | Programming | C | Describe the terms and concepts associated with secure coding practices\. | Common string handling functions |  | B |  |  |  |  |
| 555 |  |  |  | Programming | C | Describe the terms and concepts associated with secure coding practices\. | Which functions guarantee null terminated strings |  | B |  |  |  |  |
| 556 |  |  |  | Programming | Secure Coding | Describe the terms and concepts associated with secure coding practices\. | An off\-by\-one error |  | B |  |  |  |  |
| 557 |  |  |  | Programming | Secure Coding | Describe the terms and concepts associated with secure coding practices\. | An integer overflow |  | B |  |  |  |  |
| 558 |  |  |  | Programming | C | Describe the terms and concepts associated with secure coding practices\. | A buffer overflow |  | B |  |  |  |  |
| 559 |  |  |  | Programming | Secure Coding | Describe the terms and concepts associated with secure coding practices\. | The concept of use\-after\-free |  | B |  |  |  |  |
| 560 |  |  |  | Programming | Secure Coding | Describe the terms and concepts associated with secure coding practices\. | Resource acquisition is initialization \(RAII\) |  | B |  |  |  |  |
| 561 |  |  |  | Programming | Secure Coding | Describe the terms and concepts associated with secure coding practices\. | The difference between a regular expression and a context\-free grammar\. |  | A |  |  |  |  |
| 562 |  |  |  | Programming | Secure Coding | Describe the terms and concepts associated with secure coding practices\. | The difference between input validation vs input sanitization\. |  | B |  |  |  |  |
| 563 |  |  |  | Programming | Secure Coding | Describe the terms and concepts associated with secure coding practices\. | The meaning of a pure function and if a function has a side\-effect |  | A |  |  |  |  |
| 564 |  |  |  | Programming | Secure Coding | Describe the terms and concepts associated with secure coding practices\. | General low\-level crypto basics |  | A |  |  |  |  |
| 565 |  |  |  | Programming | C | Demonstrate skill in using secure coding techniques\. | Formatting string vulnerabilities |  | 2c | Avoiding/remediating\.\.\. formatting string vulnerabilities |  |  |  |
| 566 |  |  |  | Programming | C | Demonstrate skill in using secure coding techniques\. | Safe buffer size allocation |  | 2c |  |  |  |  |
| 567 |  |  |  | Programming | Secure Coding | Demonstrate skill in using secure coding techniques\. | Input sanitization |  | 2c |  |  |  |  |
| 568 |  |  |  | Programming | Secure Coding | Demonstrate skill in using secure coding techniques\. | Input validation |  | 2c |  |  |  |  |
| 569 |  |  |  | Programming | Secure Coding | Demonstrate skill in using secure coding techniques\. | Modeling complex functionality as state\-machines |  | 2b |  |  |  |  |
| 570 |  |  |  | Programming | Secure Coding | Demonstrate skill in using secure coding techniques\. | Establish a secure communications channel using an SSL library |  | 2c |  |  |  |  |
| 571 |  |  |  | Programming | C | Demonstrate skill in using secure coding techniques\. | Securely zeroing\-out memory |  | 2c |  |  |  |  |
| 572 |  |  |  | Programming | Secure Coding | Describe characteristics of a secure cryptographic hash | Hashing |  | B |  |  |  |  |
| 573 |  |  |  | Programming | Assembly Programming | Describe the purpose and use of assembly programming terminology\. \(With references and required resources\) | x86 |  | B | Needs additional section |   |  |  |
| 574 |  |  |  | Programming | Assembly Programming | Describe the purpose and use of assembly programming terminology\. \(With references and required resources\) | x86\-64 |  | B | Needs additional section |   |  |  |
| 575 |  |  |  | Programming | Assembly Programming | Describe the purpose and use of assembly programming terminology\. \(With references and required resources\) | The stack |  | B | Needs additional section |   |  |  |
| 576 |  |  |  | Programming | Assembly Programming | Describe the purpose and use of assembly programming terminology\. \(With references and required resources\) | Memory \(registers, cache, system memory, disk\) |  | B | Needs additional section |   |  |  |
| 577 |  |  |  | Programming | Assembly Programming | Describe the purpose and use of assembly programming terminology\. \(With references and required resources\) | Virtual memory |  | B | Needs additional section |   |  |  |
| 578 |  |  |  | Programming | Assembly Programming | Describe the purpose and use of assembly programming terminology\. \(With references and required resources\) | Instructions |  | B | Needs additional section |   |  |  |
| 579 |  |  |  | Programming | Assembly Programming | Describe the purpose and use of assembly programming terminology\. \(With references and required resources\) | Operands |  | B | Needs additional section |   |  |  |
| 580 |  |  |  | Programming | Assembly Programming | Describe the purpose and use of assembly programming terminology\. \(With references and required resources\) | Opcodes |  | B | Needs additional section |   |  |  |
| 581 |  |  |  | Programming | Assembly Programming | Describe the purpose and use of assembly programming terminology\. \(With references and required resources\) | Assemblers |  | B | Needs additional section |   |  |  |
| 582 |  |  |  | Programming | Assembly Programming | Describe the purpose and use of assembly programming terminology\. \(With references and required resources\) | Endianness |  | B | Needs additional section |   |  |  |
| 583 |  |  |  | Programming | Assembly Programming | Demonstrate use of assembly programming concepts\. \(With references and required resources\) | Structures |  | 2b | Needs additional section |   |  |  |
| 584 |  |  |  | Programming | Assembly Programming | Describe the purpose and use of assembly programming terminology\. \(With references and required resources\) | Two's complement |  | B | Needs additional section |   |  |  |
| 585 |  |  |  | Programming | Assembly Programming | Demonstrate use of assembly programming concepts\. \(With references and required resources\) | Bit operations |  | 2b | Needs additional section |   |  |  |
| 586 |  |  |  | Programming | Assembly Programming | Demonstrate use of assembly programming concepts\. \(With references and required resources\) | Flags |  | 2b | Needs additional section |   |  |  |
| 587 |  |  |  | Programming | Assembly Programming | Demonstrate use of assembly programming concepts\. \(With references and required resources\) | Labels |  | 2b | Needs additional section |   |  |  |
| 588 |  |  |  | Programming | Assembly Programming | Demonstrate use of assembly programming concepts\. \(With references and required resources\) | Conditional Control Flow |  | 2b | Needs additional section |   |  |  |
| 589 |  |  |  | Programming | Assembly Programming | Demonstrate use of assembly programming concepts\. \(With references and required resources\) | Functions |  | 2b | Needs additional section |   |  |  |
| 590 |  |  |  | Programming | Assembly Programming | Demonstrate use of assembly programming concepts\. \(With references and required resources\) | Processor modes |  | 2b | Needs additional section |   |  |  |
| 591 |  |  |  | Programming | Assembly Programming | Demonstrate use of assembly programming concepts\. \(With references and required resources\) | Interrupts |  | 2b | Needs additional section |   |  |  |
| 592 |  |  |  | Programming | Assembly Programming | Demonstrate use of assembly programming concepts\. \(With references and required resources\) | Utility functions \(strlen, memcpy, memset, etc\) |  | 2b | Needs additional section |   |  |  |
| 593 |  |  |  | Programming | Assembly Programming | Demonstrate use of assembly programming concepts\. \(With references and required resources\) | System calls |  | 2b | Needs additional section |   |  |  |
| 594 |  |  |  | Programming | Assembly Programming | Demonstrate use of assembly programming concepts\. \(With references and required resources\) | Memory allocation |  | 2b | Needs additional section |   |  |  |
| 595 |  |  |  | Programming | Assembly Programming | Demonstrate use of assembly programming concepts\. \(With references and required resources\) | File handling |  | 2b | Needs additional section |   |  |  |
| 596 |  |  |  | Programming | Assembly Programming | With references and required resources, describe the purpose and use of registers\. | 64\-bit registers |  | B | Needs additional section |   |  |  |
| 597 |  |  |  | Programming | Assembly Programming | With references and required resources, describe the purpose and use of registers\. | 32\-bit registers |  | B | Needs additional section |   |  |  |
| 598 |  |  |  | Programming | Assembly Programming | With references and required resources, describe the purpose and use of registers\. | Lower 16\-bit registers |  | B | Needs additional section |   |  |  |
| 599 |  |  |  | Programming | Assembly Programming | With references and required resources, describe the purpose and use of registers\. | High 8\-bit registers |  | B | Needs additional section |   |  |  |
| 600 |  |  |  | Programming | Assembly Programming | With references and required resources, describe the purpose and use of registers\. | Lower 8\-bit registers |  | B | Needs additional section |   |  |  |
| 601 |  |  |  | Programming | Assembly Programming | With references and required resources, describe the purpose and use of general purpose instructions\. | mov |  | 2b | Needs additional section |   |  |  |
| 602 |  |  |  | Programming | Assembly Programming | With references and required resources, describe the purpose and use of general purpose instructions\. | movsx |  | 2b | Needs additional section |   |  |  |
| 603 |  |  |  | Programming | Assembly Programming | With references and required resources, describe the purpose and use of general purpose instructions\. | movzx |  | 2b | Needs additional section |   |  |  |
| 604 |  |  |  | Programming | Assembly Programming | With references and required resources, describe the purpose and use of general purpose instructions\. | lea |  | 2b | Needs additional section |   |  |  |
| 605 |  |  |  | Programming | Assembly Programming | With references and required resources, describe the purpose and use of general purpose instructions\. | xchg |  | 2b | Needs additional section |   |  |  |
| 606 |  |  |  | Programming | Assembly Programming | With references and required resources, describe the purpose and use of general purpose instructions\. | push |  | 2b | Needs additional section |   |  |  |
| 607 |  |  |  | Programming | Assembly Programming | With references and required resources, describe the purpose and use of general purpose instructions\. | pop |  | 2b | Needs additional section |   |  |  |
| 608 |  |  |  | Programming | Assembly Programming | With references and required resources, describe the purpose and use of general purpose instructions\. | pushf |  | 2b | Needs additional section |   |  |  |
| 609 |  |  |  | Programming | Assembly Programming | With references and required resources, describe the purpose and use of general purpose instructions\. | popf |  | 2b | Needs additional section |   |  |  |
| 610 |  |  |  | Programming | Assembly Programming | With references and required resources, describe the purpose and use of general purpose instructions\. | call |  | 2b | Needs additional section |   |  |  |
| 611 |  |  |  | Programming | Assembly Programming | With references and required resources, describe the purpose and use of general purpose instructions\. | ret |  | 2b | Needs additional section |   |  |  |
| 612 |  |  |  | Programming | Assembly Programming | With references and required resources, describe the purpose and use of arithmetic instructions\. | add |  | 2b | Needs additional section |   |  |  |
| 613 |  |  |  | Programming | Assembly Programming | With references and required resources, describe the purpose and use of arithmetic instructions\. | sub |  | 2b | Needs additional section |   |  |  |
| 614 |  |  |  | Programming | Assembly Programming | With references and required resources, describe the purpose and use of arithmetic instructions\. | mul |  | 2b | Needs additional section |   |  |  |
| 615 |  |  |  | Programming | Assembly Programming | With references and required resources, describe the purpose and use of arithmetic instructions\. | div |  | 2b | Needs additional section |   |  |  |
| 616 |  |  |  | Programming | Assembly Programming | With references and required resources, describe the purpose and use of arithmetic instructions\. | inc |  | 2b | Needs additional section |   |  |  |
| 617 |  |  |  | Programming | Assembly Programming | With references and required resources, describe the purpose and use of arithmetic instructions\. | dec |  | 2b | Needs additional section |   |  |  |
| 618 |  |  |  | Programming | Assembly Programming | With references and required resources, describe the purpose and use of binary instructions\. | shl |  | 2b | Needs additional section |   |  |  |
| 619 |  |  |  | Programming | Assembly Programming | With references and required resources, describe the purpose and use of binary instructions\. | shr |  | 2b | Needs additional section |   |  |  |
| 620 |  |  |  | Programming | Assembly Programming | With references and required resources, describe the purpose and use of binary instructions\. | and |  | 2b | Needs additional section |   |  |  |
| 621 |  |  |  | Programming | Assembly Programming | With references and required resources, describe the purpose and use of binary instructions\. | or |  | 2b | Needs additional section |   |  |  |
| 622 |  |  |  | Programming | Assembly Programming | With references and required resources, describe the purpose and use of binary instructions\. | not |  | 2b | Needs additional section |   |  |  |
| 623 |  |  |  | Programming | Assembly Programming | With references and required resources, describe the purpose and use of binary instructions\. | xor |  | 2b | Needs additional section |   |  |  |
| 624 |  |  |  | Programming | Assembly Programming | With references and required resources, describe the purpose and use of binary instructions\. | rol |  | 2b | Needs additional section |   |  |  |
| 625 |  |  |  | Programming | Assembly Programming | With references and required resources, describe the purpose and use of binary instructions\. | ror |  | 2b | Needs additional section |   |  |  |
| 626 |  |  |  | Programming | Assembly Programming | With references and required resources, describe the purpose and use of binary instructions\. | sar |  | 2b | Needs additional section |   |  |  |
| 627 |  |  |  | Programming | Assembly Programming | With references and required resources, describe the purpose and use of binary instructions\. | sal |  | 2b | Needs additional section |   |  |  |
| 628 |  |  |  | Programming | Assembly Programming | With references and required resources, describe the purpose and use of common conditional instructions\. | cmp |  | 2b | Needs additional section |   |  |  |
| 629 |  |  |  | Programming | Assembly Programming | With references and required resources, describe the purpose and use of common conditional instructions\. | test |  | 2b | Needs additional section |   |  |  |
| 630 |  |  |  | Programming | Assembly Programming | With references and required resources, describe the purpose and use of common conditional instructions\. | jcc \(je/jz, jne/jnz, ja, jb/jc\) |  | 2b | Needs additional section |   |  |  |
| 631 |  |  |  | Programming | Assembly Programming | With references and required resources, describe the purpose and use of common conditional instructions\. | loop |  | 2b | Needs additional section |   |  |  |
| 632 |  |  |  | Programming | Assembly Programming | With references and required resources, describe the purpose and use of common string instructions\. | scas |  | 2b | Needs additional section |   |  |  |
| 633 |  |  |  | Programming | Assembly Programming | With references and required resources, describe the purpose and use of common string instructions\. | stos |  | 2b | Needs additional section |   |  |  |
| 634 |  |  |  | Programming | Assembly Programming | With references and required resources, describe the purpose and use of common string instructions\. | lods |  | 2b | Needs additional section |   |  |  |
| 635 |  |  |  | Programming | Assembly Programming | With references and required resources, describe the purpose and use of common string instructions\. | movs |  | 2b | Needs additional section |   |  |  |
| 636 |  |  |  | Programming | Assembly Programming | With references and required resources, describe the purpose and use of common string instructions\. | cmps |  | 2b | Needs additional section |   |  |  |
| 637 |  |  |  | Programming | Assembly Programming | With references and required resources, describe the purpose and use of common string instructions\. | rep |  | 2b | Needs additional section |   |  |  |
| 638 |  |  |  | Programming | Assembly Programming | With references and required resources, describe the purpose and use of common string instructions\. | repne |  | 2b | Needs additional section |   |  |  |
| 639 |  |  |  | Programming | Assembly Programming | With references and required resources, describe the purpose and use of common string instructions\. | repe |  | 2b | Needs additional section |   |  |  |
| 640 |  |  |  | Programming | Assembly Programming | With references and required resources, describe the purpose and use of common string instructions\. | std |  | 2b | Needs additional section |   |  |  |
| 641 |  |  |  | Programming | Assembly Programming | With references and required resources, describe the purpose and use of common string instructions\. | cld |  | 2b | Needs additional section |   |  |  |
| 642 |  |  |  | Programming | Assembly Programming | With references and required resources, describe the purpose and use of access memory and how to dereference addresses\. | Data sizes |  | B |  |  |  |  |
| 643 |  |  |  | Programming | Assembly Programming | With references and required resources, describe the purpose and use of access memory and how to dereference addresses\. | Restrictions |  | B |  |  |  |  |
| 644 |  |  |  | Programming | Assembly Programming | With references and required resources, describe the purpose and use of access memory and how to dereference addresses\. | Iteration of consecutive memory addresses |  | B |  |  |  |  |
| 645 |  |  |  | Programming | Assembly Programming | With references and required resources, describe the purpose and use of access memory and how to dereference addresses\. | Structures |  | B |  |  |  |  |
| 646 |  |  |  | Programming | Assembly Programming | With references and required resources, describe the purpose, use, and concepts of the NASM assembler\. | Generating opcodes |  | B |  |  |  |  |
| 647 |  |  |  | Programming | Assembly Programming | With references and required resources, describe the purpose, use, and concepts of the NASM assembler\. | How the assembler works |  | B |  |  |  |  |
| 648 |  |  |  | Programming | Assembly Programming | With references and required resources, describe the purpose, use, and concepts of the NASM assembler\. | Differences between assemblers |  | B |  |  |  |  |
| 649 |  |  |  | Programming | Assembly Programming | With references and required resources, use a debugger to identify errors in a program\. | GDB |  | 2b |  |  |  |  |
| 650 |  |  |  | Programming | Assembly Programming | With references and required resources, use a debugger to identify errors in a program\. | WinDBG |  | 2b |  |  |  |  |
| 651 |  |  |  | Programming | Assembly Programming | With references and required resources, describe the concepts, use, and terms associated with the process stack\. | Stack frame |  | B |  |  |  |  |
| 652 |  |  |  | Programming | Assembly Programming | With references and required resources, describe the concepts, use, and terms associated with the process stack\. | Push/pop/ret |  | B |  |  |  |  |
| 653 |  |  |  | Programming | Assembly Programming | With references and required resources, describe the concepts, use, and terms associated with the process stack\. | Manual allocation/deallocation |  | B |  |  |  |  |
| 654 |  |  |  | Programming | Assembly Programming | With references and required resources, describe the concepts, use, and terms associated with the process stack\. | Relationship to functions |  | B |  |  |  |  |
| 655 |  |  |  | Programming | Assembly Programming | With references and required resources, describe the purpose and use of functions with respect to use in assembly\. | Calling conventions |  | B |  |  |  |  |
| 656 |  |  |  | Programming | Assembly Programming | With references and required resources, describe the purpose and use of functions with respect to use in assembly\. | Arguments in relation to stack |  | B |  |  |  |  |
| 657 |  |  |  | Programming | Assembly Programming | With references and required resources, describe the purpose and use of functions with respect to use in assembly\. | Call stack handling |  | B |  |  |  |  |
| 658 |  |  |  | Programming | Assembly Programming | With references and required resources, describe the purpose and use of functions with respect to use in assembly\. | Name mangling |  | B |  |  |  |  |
| 659 |  |  |  | Programming | Assembly Programming | With references and required resources, describe the purpose and use of the various system flag registers\. | How assembly programs set flags |  | B |  |  |  |  |
| 660 |  |  |  | Programming | Assembly Programming | With references and required resources, describe the purpose and use of the various system flag registers\. | How to manually set the flags |  | B |  |  |  |  |
| 661 |  |  |  | Programming | Assembly Programming | With references and required resources, describe the purpose and use of the various system flag registers\. | Relation to conditional branch instructions |  | B |  |  |  |  |
| 662 |  |  |  | Programming | Assembly Programming | With references and required resources, describe the purpose and use of system interrupts\. | System interrupts |  | B |  |  |  |  |
| 663 |  |  |  | Programming | Assembly Programming | With references and required resources, describe the purpose, use, and terms of operating system calls \(syscall\) to request kernel mode services\. | sysenter/sysexit |  | A |  |  |  |  |
| 664 |  |  |  | Programming | Assembly Programming | With references and required resources, describe the purpose, use, and terms of operating system calls \(syscall\) to request kernel mode services\. | sys\_read |  | A |  |  |  |  |
| 665 |  |  |  | Programming | Assembly Programming | With references and required resources, describe the purpose, use, and terms of operating system calls \(syscall\) to request kernel mode services\. | sys\_write |  | A |  |  |  |  |
| 666 |  |  |  | Programming | Assembly Programming | With references and required resources, describe the purpose, use, and terms of operating system calls \(syscall\) to request kernel mode services\. | sys\_open |  | A |  |  |  |  |
| 667 |  |  |  | Programming | Assembly Programming | With references and required resources, describe the purpose, use, and terms of operating system calls \(syscall\) to request kernel mode services\. | sys\_mmap |  | A |  |  |  |  |
| 668 |  |  |  | Programming | Assembly Programming | With references and required resources, describe the purpose, use, and terms of operating system calls \(syscall\) to request kernel mode services\. | sys\_brk |  | A |  |  |  |  |
| 669 |  |  |  | Programming | Assembly Programming | With references and required resources, describe the purpose, use, and terms of operating system calls \(syscall\) to request kernel mode services\. | sys\_exit |  | A |  |  |  |  |
| 670 |  |  |  | Programming | Assembly Programming | With references and required resources, describe the purpose, use, and terms of operating system calls \(syscall\) to request kernel mode services\. | etc |  | A |  |  |  |  |
| 671 |  |  |  | Programming | Assembly Programming | With references and required resources, describe the purpose and use of memory segmentation and how a system or process' memory is laid into a section\. | Memory segmentation/process memory |  | A |  |  |  |  |
| 672 |  |  |  | Programming | Assembly Programming | With references and required resources, describe the purpose and use of SIMD instructions\. Utilize use of registers and instructions that can operate on multiple data items at once\. | XMM0\-XMM7 |  | A |  |  |  |  |
| 673 |  |  |  | Programming | Assembly Programming | Given a high level language \(C or Python\) code snippet, write the assembly program that accomplishes the same thing | Write in assembly |  | 2b | Needs additional section |   |  |  |
| 674 |  |  |  | Programming | Reverse Engineering | With references and required resources, reverse a basic binary program to reconstruct high level psuedocode\. | Static analysis |  | 2b | Something 'simple' like hello world |  |  |  |
| 675 |  |  |  | Programming | Reverse Engineering | With references and required resources, reverse a basic binary program to reconstruct high level psuedocode\. | Dynamic analysis |  | 2b | Something 'simple' like hello world |  |  |  |
| 676 |  |  |  | Programming | Reverse Engineering | With references and required resources, demonstrate skill in utilizing the following tools to reverse binaries\. | IDA Pro |  | 2b | Something 'simple' like hello world |  |  |  |
| 677 |  |  |  | Programming | Reverse Engineering | With references and required resources, demonstrate skill in utilizing the following tools to reverse binaries\. | WinDBG |  | 2b | Something 'simple' like hello world |  |  |  |
| 678 |  |  |  | Programming | Reverse Engineering | With references and required resources, describe the purpose and use of IDA Pro additional features\. | IDA extensions |  | A |  |  |  |  |
| 679 |  |  |  | Programming | Reverse Engineering | With references and required resources, describe the purpose and use of IDA Pro additional features\. | IDA scripts via Python |  | A |  |  |  |  |
| 680 |  |  |  | Programming | Reverse Engineering | With references and required resources, describe the purpose and use of IDA Pro additional features\. | Advanced IDA features |  | A |  |  |  |  |
| 681 |  |  |  | Programming | Reverse Engineering | With references and required resources, reverse a basic binary program to reconstruct high level psuedocode\. | Function calls |  | 1a | Something 'simple' like hello world |  |  |  |
| 682 |  |  |  | Programming | Reverse Engineering | With references and required resources, demonstrate skill in utilizing the following tools to reverse binaries\. | GDB |  | 2b | Something 'simple' like hello world |  |  |  |
| 683 |  |  |  | Programming | Containerization | Utilize Containerization to build infrastructure as code |  |  | 2b | e\.g\. Docker |  |  |  |
| 684 |  |  |  | Project | Agile | Creating and Refining Items | Describe Agile Development Concept |  | B |  |  |  |  |
| 685 |  |  |  | Project | Scrum | Describe Scrum Terms and Methodology |  |  | B |  |  |  |  |
| 686 |  |  |  | Project | Development Operations | Describe Development Operations \(DevOps\) |  |  | B |  |  |  |  |
| 687 |  |  |  | Programming | Development Operations | Build robust CI pipeline using containers |  |  | 1a |  |  |  |  |
| 688 |  |  |  | Programming | Development Operations | Perform paired programming |  |  | 1a |  |  |  |  |
| 689 |  |  |  | Project | Development Operations | Perform test driven development |  |  | 1a |  |  |  |  |
| 690 |  |  |  | Project | Development Operations | Given a set of requirements develop user story |  |  | 2b |  |  |  |  |
| 691 |  |  |  | Project | Development Operations | Package tool for delivery | Create user guide |  | 2b |  |  |  |  |
| 692 |  |  |  | Project | Development Operations | Package tool for delivery | Validate operability on clean host |  | 2b |  |  |  |  |
| 693 |  |  |  | Project | Development Operations | Package tool for delivery | Create install documentation |  | 2b |  |  |  |  |
| 694 |  |  |  | Project | Development Operations | Package tool for delivery | Describe the required elements of package delivery |  | A |  |  |  |  |
| 695 |  |  |  | Project | Development Operations | Utilize ticketing tool to structure team activities |  |  | 2b |  |  |  |  |
| 696 |  |  |  | Project | Development Operations | Utilize knowledge management tool to document internal team knowledge and end user documentation |  |  | 2b |  |  |  |  |
| 697 |  |  |  | Software | Drivers | Describe the functionality of driver kits | Device Driver Kit \(DDK\) |  | B |  |  |  |  |
| 698 |  |  |  | Software | Drivers | Describe the functionality of driver kits | Windows Driver Kit \(WDK\) |  | B |  |  |  |  |
| 699 |  |  |  | Programming | C | Perform Windows API error analysis | Utilize GetLastError\(\) to analyze windows error codes |  | 2b |  |  |  |  |
| 700 |  |  |  | Programming | C | Perform Windows API error analysis | Utilize net helpmsg to analyze windows error codes |  | 2b |  |  |  |  |
| 701 |  |  |  | Software | Windows | Describe calling conventions used in Windows API \(32bit & 64bit\) | Stdcall |  | B |  |  |  |  |
| 702 |  |  |  | Software | Windows | Describe calling conventions used in Windows API \(32bit & 64bit\) | cdecl |  | B |  |  |  |  |
| 703 |  |  |  | Software | Windows | Describe calling conventions used in Windows API \(32bit & 64bit\) | fastcall |  | B |  |  |  |  |
| 704 |  |  |  | Software | Windows | Describe calling conventions used in Windows API \(32bit & 64bit\) | thiscall |  | B |  |  |  |  |
| 705 |  |  |  | Programming | C | Utilize Windows API File IO | CreateFile\(\) |  | 2b | Show how to use MSDN then use the function |  |  |  |
| 706 |  |  |  | Programming | C | Utilize Windows API File IO | HANDLE |  | 2b | Show how to use MSDN then use the function |  |  |  |
| 707 |  |  |  | Programming | C | Utilize Windows API File IO | FindFirstFile\(\) |  | 2b | Show how to use MSDN then use the function |  |  |  |
| 708 |  |  |  | Programming | C | Utilize Windows API File IO | FindFirstFileEx\(\) |  | 2b | Show how to use MSDN then use the function |  |  |  |
| 709 |  |  |  | Programming | C | Utilize Windows API File IO | FindNextFile\(\) |  | 2b | Show how to use MSDN then use the function |  |  |  |
| 710 |  |  |  | Programming | C | Utilize Windows API File IO | FindClose\(\) |  | 2b | Show how to use MSDN then use the function |  |  |  |
| 711 |  |  |  | Programming | C | Utilize Windows API File IO | ReadFile\(\) |  | 2b | Show how to use MSDN then use the function |  |  |  |
| 712 |  |  |  | Programming | C | Utilize Windows API File IO | WriteFiLE\(\) |  | 2b | Show how to use MSDN then use the function |  |  |  |
| 713 |  |  |  | Programming | C | Utilize Windows API File IO | CloseHandle\(\) |  | 2b | Show how to use MSDN then use the function |  |  |  |
| 714 |  |  |  | Programming | C | Utilize MSDN Documents to find function details | Registry |  | 2b | RegOpenKeyEx\(\)\.\.\.\. |  |  |  |
| 715 |  |  |  | Programming | C | Utilize MSDN Documents to find function details and utilize functions | Processes |  | 2b | CreateProcess\(\), Synchronization: WaitForSingleObject\(\) |  |  |  |
| 716 |  |  |  | Programming | C | Utilize MSDN Documents to find function details and utilize functions | Threads |  | 2b | CreateThread\(\), CreateMutex\(\) |  |  |  |
| 717 |  |  |  | Programming | C | Utilize MSDN Documents to find function details and utilize functions | Windows Sockets |  | 2b | WSAStartup\), WSACleanup\(\)\.\.\. |  |  |  |
| 718 |  |  |  | Programming | C | Utilize MSDN Documents to find function details and utilize functions | Windows Services |  | 2b | StartServiceCtrlDispatcher\(\)\.\.\. |  |  |  |
| 719 |  |  |  | Software | Windows | Understand the different Windows APIs | Describe the Nt API |  | A |  |  |  |  |
| 720 |  |  |  | Software | Windows | Understand the different Windows APIs | Describe the Zw API |  | A |  |  |  |  |
| 721 |  |  |  | Software | Windows | Understand the different Windows APIs | Describe the Rtl API |  | A |  |  |  |  |
| 722 |  |  |  | Software | Windows | Understand the different Windows APIs | Describe the Ldr API |  | A |  |  |  |  |
| 723 |  |  |  | Software | Windows | Understand naming conventions and macro theory associated with Windows API | Describe Windows data types |  | B | LPCTSTR decoded\.\.\. |  |  |  |
| 724 |  |  |  | Software | Windows | Understand naming conventions and macro theory associated with Windows API | Describe the Windows naming conventions |  | B | Functions, Structures, Parms, Vars, Macros, Constants |  |  |  |
| 725 |  |  |  | Software | Windows | Understand naming conventions and macro theory associated with Windows API | Describe the Windows API macro theory |  | B | Create file versus createfileA and CreateFileW |  |  |  |
| 726 |  |  |  | Software | Windows | Understand naming conventions and macro theory associated with Windows API | Describe how using Unicode affects Windows API function calls and data types |  | B |  |  |  |  |
| 727 |  |  |  | Software | Windows | Understand naming conventions and macro theory associated with Windows API | Describe the date/time convention |  | B |  |  |  |  |
| 728 |  |  |  | Software | Windows | Understand the Windows Registry | Describe registry key |  | A |  |  |  |  |
| 729 |  |  |  | Software | Windows | Understand the Windows Registry | Describe registry hive |  | A |  |  |  |  |
| 730 |  |  |  | Software | Windows | Understand the Windows Registry | Utilize Regedit to view a key |  | 1a |  |  |  |  |
| 731 |  |  |  | Programming | C | Utilize man pages to find function details and utilize function | File IO |  | 2b |  |  |  |  |
| 732 |  |  |  | Programming | C | Utilize man pages to find function details and utilize function | File and Directory Management |  | 2b |  |  |  |  |
| 733 |  |  |  | Programming | C | Utilize man pages to find function details and utilize function | Date/Time Conventions |  | 2b |  |  |  |  |
| 734 |  |  |  | Programming | C | Utilize man pages to find function details and utilize function | Signals |  | 2b |  |  |  |  |
| 735 |  |  |  | Programming | C | Utilize man pages to find function details and utilize function | IPC |  | 2b |  |  |  |  |
| 736 |  |  |  | Programming | C | Utilize man pages to find function details and utilize function | Sockets |  | 2b |  |  |  |  |
| 737 |  |  |  | Programming | C | Utilize man pages to find function details and utilize function | Inline Functions |  | 2b |  |  |  |  |
| 738 |  |  |  | Software | Linux | Understand the Linux configuration startup | etc |  | B |  |  |  |  |
| 739 |  |  |  | Software | Linux | Understand the Linux configuration startup | init |  | B |  |  |  |  |
| 740 |  |  |  | Software | Linux | Understand the Linux configuration startup | rc scripts |  | B |  |  |  |  |
| 741 |  |  |  | Software | Linux | Understand the Linux configuration startup | systemd |  | B |  |  |  |  |
| 742 |  |  |  | Programming | C | Utilize man pages to find function details and utilize functions | Buffered IO |  | 2b |  |  |  |  |
| 743 |  |  |  | Programming | C | Utilize man pages to find function details and utilize functions | Advanced IO |  | 2b |  |  |  |  |
| 744 |  |  |  | Programming | C | Utilize man pages to find function details on Processes and Threads and utilize functions | PIDS |  | 2b |  |  |  |  |
| 745 |  |  |  | Programming | C | Utilize man pages to find function details on Processes and Threads and utilize functions | Fork\(\) |  | 2b |  |  |  |  |
| 746 |  |  |  | Programming | C | Utilize man pages to find function details on Processes and Threads and utilize functions | exec\(\) |  | 2b |  |  |  |  |
| 747 |  |  |  | Programming | C | Utilize man pages to find function details on Processes and Threads and utilize functions | wait\(\) |  | 2b |  |  |  |  |
| 748 |  |  |  | Programming | C | Utilize man pages to find function details on Processes and Threads and utilize functions | waidpid\(\) |  | 2b |  |  |  |  |
| 749 |  |  |  | Programming | C | Utilize man pages to find function details on Processes and Threads and utilize functions | system\(\) |  | 2b |  |  |  |  |
| 750 |  |  |  | Programming | C | Utilize man pages to find function details on Processes and Threads and utilize functions | setuid\(\) |  | 2b |  |  |  |  |
| 751 |  |  |  | Programming | C | Utilize man pages to find function details on Processes and Threads and utilize functions | setgid\(\) |  | 2b |  |  |  |  |
| 752 |  |  |  | Programming | C | Utilize man pages to find function details on Processes and Threads and utilize functions | setreuid\(\) |  | 2b |  |  |  |  |
| 753 |  |  |  | Programming | C | Utilize man pages to find function details on Processes and Threads and utilize functions | setreguid\(\) |  | 2b |  |  |  |  |
| 754 |  |  |  | Programming | C | Utilize man pages to find function details on Processes and Threads and utilize functions | setpgid\(\) |  | 2b |  |  |  |  |
| 755 |  |  |  | Programming | C | Utilize man pages to find function details on Processes and Threads and utilize functions | getpriority\(\) |  | 2b |  |  |  |  |
| 756 |  |  |  | Programming | C | Utilize man pages to find function details on Processes and Threads and utilize functions | setpriority\(\) |  | 2b |  |  |  |  |
| 757 |  |  |  | Programming | C | Utilize man pages to find function details on Processes and Threads and utilize functions | nice\(\) |  | 2b |  |  |  |  |
| 758 |  |  |  | Software | Linux | Understand process fundamentals in Linux | Parentage |  | B |  |  |  |  |
| 759 |  |  |  | Software | Linux | Understand process fundamentals in Linux | Daemons |  | B |  |  |  |  |
| 760 |  |  |  | Software | Linux | Understand process fundamentals in Linux | Resource Limits |  | B |  |  |  |  |
| 761 |  |  |  | Software | Linux | Understand signal fundamentals in Linux | Ignore |  | B |  |  |  |  |
| 762 |  |  |  | Software | Linux | Understand signal fundamentals in Linux | Catch |  | B |  |  |  |  |
| 763 |  |  |  | Software | Linux | Understand signal fundamentals in Linux | Handle |  | B |  |  |  |  |
| 764 |  |  |  | Software | Linux | Understand signal fundamentals in Linux | Default Action |  | B |  |  |  |  |
| 765 |  |  |  | Software | Linux | Understand signal fundamentals in Linux | Supported Signals |  | B |  |  |  |  |
| 766 |  |  |  | Project | Tool Development | Given a 2\-3 person team and customer requirements/assigned scenario, develop a tool\. | Build a tool that writes a registry entry to start a process at boot |  | 2b | Difficulty level 1 |  |  |  |
| 767 |  |  |  | Project | Tool Development | Given a 2\-3 person team and customer requirements/assigned scenario, develop a tool\. | Build a tool that writes an RC script to start a process at boot |  | 2b | Difficulty level 1 |  |  |  |
| 768 |  |  |  | Project | Windows | Given a 2\-3 person team and customer requirements/assigned scenario, develop a tool\. | Build a Windows tool in C that receives data and writes to disk |  | 2b | Difficulty level 1 |  |  |  |
| 769 |  |  |  | Project | Linux | Given a 2\-3 person team and customer requirements/assigned scenario, develop a tool\. | Build a Linux tool in C that receives data and writes to disk |  | 2b | Difficulty level 1 |  |  |  |
| 770 |  |  |  | Project | Tool Development | Given a 2\-3 person team and customer requirements/assigned scenario, develop a tool\. | Write a binary patch to change assembly program functionality |  | 2b | Difficulty level 2 |  |  |  |
| 771 |  |  |  | Project | Linux | Given a 2\-3 person team and customer requirements/assigned scenario, develop a tool\. | Build a Linux tool in Python to perform bulk encryption, decryption and export key to persistent location |  | 2b | Difficulty level 3 |  |  |  |
| 772 |  |  |  | Project | Windows | Given a 2\-3 person team and customer requirements/assigned scenario, develop a tool\. | Build a Windows tool in C that will list running processes\. |  | 2b | Difficulty level 3 |  |  |  |
| 773 |  |  |  | Project | Linux | Given a 2\-3 person team and customer requirements/assigned scenario, develop a tool\. | Build a Linux tool in C that will list running processes\. |  | 2b | Difficulty level 3 |  |  |  |
| 774 |  |  |  | Project | Tool Development | Given a 2\-3 person team and customer requirements/assigned scenario, develop a tool\. | Write a DLL that pops a message box with text \(or other effect\)\. |  | 2b | Difficulty level 3 |  |  |  |
| 775 |  |  |  | Project | Windows | Given a 2\-3 person team and customer requirements/assigned scenario, develop a tool\. | Build a client on Windows and server on Linux to transfer files back and forth\. |  | 2b | Difficulty level 4 |  |  |  |
| 776 |  |  |  | Project | Tool Development | Given a 2\-3 person team and incomplete customer requirements/assigned scenario, develop a tool\. | Build a tool \(choice of C or Python\) that performs a DNS query without gethostbyname\(\) |  | 2b | Difficulty level 4 |  |  |  |
| 777 |  |  |  | Project | Tool Development | Given a 2\-3 person team and incomplete customer requirements/assigned scenario, develop a tool\. | Build a tool that recursively finds parses MP3 file headers to display ID3 tags |  | 2b | Difficulty level 4 |  |  |  |
| 778 |  |  |  | Project | Tool Development | Given a 2\-3 person team and incomplete customer requirements/assigned scenario, develop a tool\. | Build a tool in Python that controls another process using standard in, standard out, and standard error  |  | 2b | Difficulty Level 4 |  |  |  |
| 779 |  |  |  | Project | Tool Development | Given a 2\-3 person team and incomplete customer requirements/assigned scenario, develop a tool\. | Build a console application in C that executes commands as child processes and prints the output \(list processes, directories, current user, /etc/shadow, config files, specific registry keys\) all in uppercase |  | 2b | Difficulty level 2 |  |  |  |
| 780 |  |  |  | Project | Tool Development | Given a 2\-3 person team and incomplete customer requirements/assigned scenario, develop a tool\. | Build a console application in C that makes an API call to retrieve information \(list processes, directories, current user, /etc/shadow, config files, specific registry keys\) |  | 2b | Difficulty Level 3 |  |  |  |
| 781 |  |  |  | Project | Tool Development | Given a 2\-3 person team and incomplete customer requirements/assigned scenario, develop a tool\. | Build a console application in C that makes an API call to retrieve information \(list processes, directories, current user, /etc/shadow, config files, specific registry keys\) that writes the data to the disk and encrypts the data |  | 2b | Difficulty Level 4 |  |  |  |
| 782 |  |  | CYA  | Project | Tool Development | Given a 2\-3 person team and incomplete customer requirements/assigned scenario, develop a tool\. | Write a client\-server application in python and C that serializes data and transfers over the network from an instructor provided protocol definition |  | 2b | Difficulty Level 4 |  |  |  |
| 783 |  |  |  | Project | Tool Development | Given a 2\-3 person team and incomplete customer requirements/assigned scenario, develop a tool\. | Write a program in C that accepts connections and updates a global data structure with information and simultaneously allows the user to query the contents of the data structure |  | 2b | Difficulty Level 4 |  |  |  |
| 784 |  |  |  | Project | Linux | Given a 4\-6 person team and incomplete customer requirements/assigned scenario, develop a tool\. | Build a Linux tool in C to spoof ARP for host to assume MITM |  | 2b | Difficulty level 5 |  |  |  |
| 785 |  |  |  | Project | Windows | Given a 4\-6 person team and incomplete customer requirements/assigned scenario, develop a tool\. | Build a Windows tool to perform local system survey and write to disk |  | 2b | Difficulty level 5 |  |  |  |
| 786 |  |  |  | Project | Tool Development | Given a 4\-6 person team and incomplete customer requirements/assigned scenario, develop a tool\. | Build a console application in C that makes an API call to retrieve information \(list processes, directories, current user, /etc/shadow, config files, specific registry keys\) that writes the data to the disk and encrypts the data |  | 2b | Difficulty Level 5 |  |  |  |
| 787 |  |  |  | Project | Windows | Given a 4\-6 person team and customer requirements/assigned scenario, develop a tool\. | Develop a Windows Remote Access Tool & a Linus Remote Access Tool |  | 2b | RATs Windows and Linux that implement the following functionality: Execute user commands \(e\.g\. ps, netstat, etc\.\) and returns output\. Capture running processes by API and return output\. Return the current user\. \(Windows\) Pull registry keys specified by instructor \(e\.g\. Run, RunOnce\)\. \(Linux\) Pull the contents of /etc/shadow and /etc/issue\. Return a file/directory listing using API\. Upload and download a file from the system the RAT is on\. Change the current working directory of the RAT \(future user executed commands should inherit the current working directory\)\. Add self to startup process \(Persistence\- students are told exactly where\)\. Burn off \- Delete persistence and exit process\. Determine hosts on the collision domain \(ARP sniffing\)\. Implements a time\-based call back \(use standard TCP for C2\)\. Create custom C2 protocol\. Encrypt file \(use instructor provided library\)\. Receive call back domain/IP via command line arguments\. Notes: Not expecting this to be an enterprise level tool\. This will be proof of concept\. Not looking for OPSEC, stealth or authentication of RATs or network traffic\. |  |  |  |
| 788 |  |  |  | Project | Tool Development | Given a 4\-6 person team and customer requirements/assigned scenario, develop a tool\. | Develop Remote Access Trojan Operator Interface |  | 2b | Operator Interface: Handles all RATs call backs, tasking and operator inputs simultaneously\. Allow one operator to task up to 5 RATs\. Track active \(within last 10 minutes\) RATs\. Must accept user input commands as defined by custom C2 protocol to task specific RATs\. Tasks commands apply to one RAT at a time\. Must be able to specify the RATs for tasking\. Must print the response from the task to the screen when task is complete\. Operator interface serves files for transfer over TCP Notes: Not expecting this to be an enterprise level tool\. This will be proof of concept\. Not looking for OPSEC, stealth or authentication of RATs or network traffic\. |  |  |  |
| 961 |  |  |  | Project | Organization Overview | 90 IOS Mission and Vision |  |  | B |  |  |  |  |
| 962 |  |  |  | Project | Organization Overview | 90 IOS Organizational Structure |  |  | C |  |  |  |  |
| 963 |  |  |  | Project | Organization Overview | Real\-Time Operations and Innovation \(RTO&I\) Development Requirements |  |  | B |  |  |  |  |
| 964 |  |  |  | Project | Organization Overview | Capability Development Initiation |  |  | A |  |  |  |  |
| 965 |  |  |  | Project | Organization Overview | Flight Missions |  |  | B |  |  |  |  |
| 966 |  |  |  | Project | Supporting/Supported Organizations | 90 IOS\-Supported Organizations |  |  | B |  |  |  |  |
| 967 |  |  |  | Project | Supporting/Supported Organizations | Organizations Supporting 90 IOS |  |  | B |  |  |  |  |
| 968 |  |  |  | Project | Supporting/Supported Organizations | Mission Area Responsibilities of Supported/Supporting Organizations |  |  | B |  |  |  |  |
| 970 |  |  |  | Project | Cyber Mission Forces | Cyber Mission Forces \(CMF\) Teams |  |  | B |  |  |  |  |
| 971 |  |  |  | Project | Cyber Mission Forces | 90 IOS CMF\-Role |  |  | C |  |  |  |  |
| 972 |  |  |  | Project | Cyber Mission Forces | Interaction Between CMF Teams |  |  | C |  |  |  |  |
| 973 |  |  |  | Project | Cyber Mission Forces | 90 IOS CMF Teams |  |  | C |  |  |  |  |
| 975 |  |  |  | Project | Exercises | Common 90 IOS Exercises |  |  | C |  |  |  |  |
| 977 |  |  |  | Project | Policy and Compliance | Executive Order |  |  | A |  |  |  |  |
| 978 |  |  |  | Project | Policy and Compliance | DoD Directive |  |  | A |  |  |  |  |
| 979 |  |  |  | Project | Policy and Compliance | NSA/CSS Policy |  |  | A |  |  |  |  |
| 980 |  |  |  | Project | Policy and Compliance | Federal Act |  |  | A |  |  |  |  |
| 981 |  |  |  | Project | Policy and Compliance | Signals Intelligence \(SIGINT\) |  |  | A |  |  |  |  |
| 982 |  |  |  | Project | Policy and Compliance | USSID |  |  | A |  |  |  |  |
| 983 |  |  |  | Project | Policy and Compliance | Classification Markings |  |  | A |  |  |  |  |
| 984 |  |  |  | Project | Policy and Compliance | United States Code Titles |  |  | A |  |  |  |  |
| 985 |  |  |  | Project | Policy and Compliance | Legal Targeting |  |  | B |  |  |  |  |
| 1119 |  |  |  | Programming | Python | Conversion Functions |  |  | 3c |  |  |  |  |
| 1122 |  |  |  | Programming | Python | Modules |  |  | 3c |  |  |  |  |
| 1126 |  |  |  | Programming | Python | Python Standard Library |  |  | 1a |  |  |  |  |
| 1127 |  |  |  | Programming | Python | Programming with Functions and Metaclasses |  |  | 2c |  |  |  |  |
| 1128 |  |  |  | Programming | Python | Multi\-threading |  |  | 1a |  |  |  |  |
| 1129 |  |  |  | Programming | Python | Networking modules |  |  | 3c |  |  |  |  |
| 1130 |  |  |  | Programming | Python | Client/Server programming in Python |  |  | 2c |  |  |  |  |
| 1132 |  |  |  | Programming | Python | Python from Command line |  |  | A |  |  |  |  |
| 1140 |  |  |  | Programming | C | Advanced Pointers |  |  | 3c |  |  |  |  |
| 1141 |  |  |  | Programming | C | Structures |  |  | 3c |  |  |  |  |
| 1142 |  |  |  | Programming | C | Function/structure recursion |  |  | 3c |  |  |  |  |
| 1143 |  |  |  | Programming | C | Memory Management concepts |  |  | 3c |  |  |  |  |
| 1145 |  |  |  | Programming | C | How the process stack functions |  |  | 3c |  |  |  |  |
| 1146 |  |  |  | Programming | C | Stack based memory and heap based memory |  |  | 3c |  |  |  |  |
| 1147 |  |  |  | Programming | C | Overview of the process stack memory |  |  | 3c |  |  |  |  |
| 1148 |  |  |  | Programming | C | Stack pointers |  |  | 3c |  |  |  |  |
| 1149 |  |  |  | Programming | C | Header files and program structure |  |  | 3c |  |  |  |  |
| 1150 |  |  |  | Programming | C | Casting in C |  |  | 3c |  |  |  |  |
| 1151 |  |  |  | Programming | C | C Standard Library |  |  | 3c |  |  |  |  |
| 1152 |  |  |  | Programming | C | Basic Data Structures |  |  | 3c |  |  |  |  |
| 1153 |  |  |  | Programming | C\+\+ | Shared Pointers |  |  | 3c |  |  |  |  |
| 1154 |  |  |  | Programming | C\+\+ | Unique Pointers |  |  | 3c |  |  |  |  |
| 1155 |  |  |  | Programming | C\+\+ | Classes |  |  | 3c |  |  |  |  |
| 1156 |  |  |  | Programming | C\+\+ | Standard Template Library |  |  | 3c |  |  |  |  |
| 1157 |  |  |  | Programming | C\+\+ | Unique C\+\+ Tools |  |  | 3c |  |  |  |  |
| 1158 |  |  |  | Programming | C\+\+ | Virtual Functions |  |  | B |  |  |  |  |
| 1159 |  |  |  | Programming | C\+\+ | Virtual Classes |  |  | B |  |  |  |  |
| 1160 |  |  |  | Programming | C\+\+ | Polymorphism |  |  | B |  |  |  |  |
| 1161 |  |  |  | Programming | C\+\+ | Operator Overloading |  |  | B |  |  |  |  |
| 1162 |  |  |  | Programming | C\+\+ | Inheritance |  |  | B |  |  |  |  |
| 1163 |  |  |  | Programming | C\+\+ | Templates |  |  | B |  |  |  |  |
| 1164 |  |  |  | Programming | C\+\+ | C\+\+ Types |  |  | 2b |  |  |  |  |
| 1165 |  |  |  | Programming | C\+\+ | History |  |  | B |  |  |  |  |
| 1166 |  |  |  | Programming | Assembly Programming | Overview |  |  | A |  |  |  |  |
| 1167 |  |  |  | Programming | Assembly Programming | x86 64 |  |  | B |  |  |  |  |
| 1168 |  |  |  | Programming | Assembly Programming | Syscall |  |  | B |  |  |  |  |
| 1169 |  |  |  | Programming | Assembly Programming | Sysenter |  |  | B |  |  |  |  |
| 1170 |  |  |  | Programming | Assembly Programming | Interrupts |  |  | B |  |  |  |  |
| 1171 |  |  |  | Programming | Assembly Programming | Memory segmentation |  |  | B |  |  |  |  |
| 1172 |  |  |  | Programming | Assembly Programming | x86 64 Calling Conventions |  |  | B |  |  |  |  |
| 1173 |  |  |  | Programming | Assembly Programming | Single Instruction Multiple Data \(SIMD\) |  |  | A |  |  |  |  |
| 1174 |  |  |  | Programming | Assembly Programming | Privilege Levels |  |  | A |  |  |  |  |
| 1175 |  |  |  | Programming | Assembly Programming | Stack frame |  |  | 2c |  |  |  |  |
| 1176 |  |  |  | Programming | Assembly Programming | General Purpose Instruction |  |  | 2c |  |  |  |  |
| 1177 |  |  |  | Programming | Assembly Programming | Registers and Sub\-registers |  |  | 2c |  |  |  |  |
| 1178 |  |  |  | Programming | Assembly Programming | Branch in |  |  | 2c |  |  |  |  |
| 1179 |  |  |  | Programming | Assembly Programming | Memory access |  |  | 2c |  |  |  |  |
| 1180 |  |  |  | Programming | Assembly Programming | Functions |  |  | 2c |  |  |  |  |
| 1181 |  |  |  | Programming | Assembly Programming | x86 Calling Conventions |  |  | 2c |  |  |  |  |
| 1182 |  |  |  | Programming | Assembly Programming | Name Mangling |  |  | 2c |  |  |  |  |
| 1183 |  |  |  | Programming | Assembly Programming | Flags |  |  | 2c |  |  |  |  |
| 1184 |  |  |  | Programming | Assembly Programming | String Instructions |  |  | 2c |  |  |  |  |
| 1185 |  |  |  | Programming | Assembly Programming | Bitwise Instructions |  |  | 2c |  |  |  |  |
| 1186 |  |  |  | Programming | Assembly Programming | Protection Mechanisms |  |  | 2c |  |  |  |  |
| 1187 |  |  |  | Programming | Assembly Programming | Opcodes |  |  | 2c |  |  |  |  |
| 1188 |  |  |  | Programming | Assembly Programming | Byte ordering |  |  | 2c |  |  |  |  |
| 1189 |  |  |  | Programming | Assembly Programming | Begin debugging with the GNU Source\-Level Debugger \(GDB\) |  |  | 2c |  |  |  |  |
| 1190 |  |  |  | Programming | Assembly Programming | Arithmetic Operations |  |  | 2c |  |  |  |  |
| 1191 |  |  |  | Programming | PowerShell | Overview |  |  | A |  |  |  |  |
| 1218 |  |  |  | Programming | Bash Scripting | Overview |  |  | A |  |  |  |  |
| 1229 |  |  |  | Software | Virtualization Software | Overview |  |  | B |  |  |  |  |
| 1234 |  |  |  | Software | Virtualization Software | Core Concepts |  |  | A |  |  |  |  |
| 1235 |  |  |  | Software | Virtualization Software | Tracking Issues |  |  | A |  |  |  |  |
| 1236 |  |  |  | Project | Wiki | Overview |  |  | A |  |  |  |  |
| 1238 |  |  |  | Programming | Immunity | Overview |  |  | A |  |  |  |  |
| 1247 |  |  |  | Software | Metasploit | Overview |  |  | A |  |  |  |  |
| 1256 |  |  |  | Network | Networking Concepts | Packet Captures |  |  | 2c |  |  |  |  |
| 1257 |  |  |  | Network | Networking Concepts | Packet Capture \(PCAP\) File Analysis |  |  | 2c |  |  |  |  |
| 1258 |  |  |  | Network | Networking Concepts | Capture Filters |  |  | 2c |  |  |  |  |
| 1259 |  |  |  | Network | Networking Concepts | Trace Connection |  |  | 2c |  |  |  |  |
| 1260 |  |  |  | Network | Networking Concepts | Secure vs Non\-secure Traffic \(encrypted vs non\-encrypted\) |  |  | 2c |  |  |  |  |
| 1261 |  |  |  | Network | Networking Concepts | Packet Capture \(PCAP\) based Intrusion Detection System \(IDS\) Analysis |  |  | B |  |  |  |  |
| 1262 |  |  |  | Programming | Reverse Engineering | IDA Extensions |  |  | A |  |  |  |  |
| 1263 |  |  |  | Programming | Reverse Engineering | IDA Scripting via Python |  |  | A |  |  |  |  |
| 1264 |  |  |  | Programming | Reverse Engineering | Advanced IDA Features |  |  | A |  |  |  |  |
| 1265 |  |  |  | Software | Reverse Engineering | WinDbg Dynamic Analysis |  |  | 2c |  |  |  |  |
| 1266 |  |  |  | Programming | Reverse Engineering | Disassemble Binary |  |  | 2c |  |  |  |  |
| 1267 |  |  |  | Software | Reverse Engineering | Static Analysis |  |  | 2b |  |  |  |  |
| 1268 |  |  |  | Software | Reverse Engineering | Dynamic Analysis |  |  | 2b |  |  |  |  |
| 1269 |  |  |  | Software | Reverse Engineering | Areas of Interest |  |  | 2b |  |  |  |  |
| 1270 |  |  |  | Programming | Reverse Engineering | Disassembly and C Programming |  |  | 2b |  |  |  |  |
| 1271 |  |  |  | Software | Reverse Engineering | Combine Dynamic and Static Analysis |  |  | 2b |  |  |  |  |
| 1272 |  |  |  | Software | Reverse Engineering | Static/Dynamic Tools |  |  | 2b |  |  |  |  |
| 1273 |  |  |  | Software | Hacking Methodologies | Hacking operating systems and software |  |  | A |  |  |  |  |
| 1274 |  |  |  | Software | Hacking Methodologies | Hacking fuzzing fundamentals |  |  | A |  |  |  |  |
| 1293 |  |  |  | Network | Networking Concepts | Basic Networking | Ports and Protocols |  | B |  |  |  |  |
| 1294 |  |  |  | Network | Networking Concepts | Basic Networking | Address Resolution Protocol \(ARP\) |  | B |  |  |  |  |
| 1295 |  |  |  | Network | Networking Concepts | Basic Networking | Hypertext Transfer Protocol/Secure \(HTTP/HTTPS\) |  | B |  |  |  |  |
| 1296 |  |  |  | Network | Networking Concepts | Basic Networking | Domain Name System \(DNS\) |  | B |  |  |  |  |
| 1297 |  |  |  | Network | Networking Concepts | Basic Networking | Simple Mail Transfer Protocol \(SMTP\) |  | B |  |  |  |  |
| 1299 |  |  |  | Network | Networking Concepts | Sockets | Intro to Sockets |  | 3c |  |  |  |  |
| 1300 |  |  |  | Network | Networking Concepts | String\-based Protocol |  |  | 2b |  |  |  |  |
| 1301 |  |  |  | Network | Networking Concepts | Bitwise Protocol |  |  | 2b |  |  |  |  |
| 1302 |  |  |  | Network | Networking Concepts | Basic Networking | Internal Control Message Protocol \(ICMP\) |  | B |  |  |  |  |
| 1303 |  |  |  | Network | Networking Concepts | Basic Networking | Dynamic Host Configuration Protocol \(DHCP\) |  | B |  |  |  |  |
| 1304 |  |  |  | Network | Networking Concepts | Basic Networking | Reading Request For Comments \(RFC\) |  | C |  |  |  |  |
| 1305 |  |  |  | Network | Networking Concepts | POSIX API/BSD Sockets |  |  | C |  |  |  |  |
| 1306 |  |  |  | Network | Networking Concepts | C Socket Programming \(move to apprentice?\) |  |  | 3c |  |  |  |  |
| 1307 |  |  |  | Network | Networking Concepts | Advanced Networking | Python Socket Programming |  | 3c |  |  |  |  |
| 1308 |  |  |  | Network | Networking Concepts | OSI Layer 3 | Subnets |  | 2b |  |  |  |  |
| 1309 |  |  |  | Network | Networking Concepts | OSI Layer 3 | IPv4 Addressing |  | 2b |  |  |  |  |
| 1310 |  |  |  | Network | Networking Concepts | OSI Layer 3 | IPv6 Addressing |  | 2b |  |  |  |  |
| 1311 |  |  |  | Project | Agile Development | Overview |  |  | B |  |  |  |  |
| 1312 |  |  |  | Project | Agile Development | Agile Development Roles |  |  | B |  |  |  |  |
| 1313 |  |  |  | Project | Agile Development | Scrum Event Distinction and Process Order |  |  | B |  |  |  |  |
| 1314 |  |  |  | Project | Agile Development | Product Backlog Grooming |  |  | B |  |  |  |  |
| 1315 |  |  |  | Project | Agile Development | Daily Scrum Meeting |  |  | B |  |  |  |  |
| 1316 |  |  |  | Project | Agile Development | Scrum Advantages to Rapid Capability Development |  |  | B |  |  |  |  |
| 1317 |  |  |  | Project | Agile Development | Unit Testing |  |  | B |  |  |  |  |
| 1318 |  |  |  | Project | Agile Development | Size and Complexity of Functional Test Items |  |  | B |  |  |  |  |
| 1319 |  |  |  | Project | Agile Development | User Story Development |  |  | B |  |  |  |  |
| 1321 |  |  |  | Project | Technical Writing | Writing Executive Summaries |  |  | A |  |  |  |  |
| 1322 |  |  |  | Project | Technical Writing | Edit Executive Summaries |  |  | A |  |  |  |  |
| 1323 |  |  |  | Project | Technical Writing | Writing User Guides |  |  | A |  |  |  |  |
| 1324 |  |  |  | Project | Technical Writing | Edit User Guides |  |  | A |  |  |  |  |
| 1325 |  |  |  | Project | Technical Writing | Writing Analysis Reports |  |  | A |  |  |  |  |
| 1326 |  |  |  | Project | Technical Writing | Reading Capability Requirements Specifications |  |  | A |  |  |  |  |
| 1327 |  |  |  | Project | Technical Writing | Writing Capability Requirements Specifications |  |  | A |  |  |  |  |
| 1328 |  |  |  | Project | Technical Writing | Edit Capability Requirements Specifications |  |  | A |  |  |  |  |
| 1329 |  |  |  | Project | Technical Writing | Writing Statement of Requirements |  |  | A |  |  |  |  |
| 1330 |  |  |  | Project | Technical Writing | Edit Statement of Requirements |  |  | A |  |  |  |  |
| 1331 |  |  |  | Project | Technical Writing | Writing Test Plan/Report |  |  | A |  |  |  |  |
| 1336 |  |  |  | Software | Software Development Process | Overview |  |  | B |  |  |  |  |
| 1337 |  |  |  | Software | Software Development Process | Version Control |  |  | B |  |  |  |  |
| 1338 |  |  |  | Software | Software Development Process | Best Practices |  |  | B |  |  |  |  |
| 1339 |  |  |  | Software | Software Development Process | Peer Review |  |  | 2b |  |  |  |  |
| 1340 |  |  |  | Software | Software Development Process | Integrated Development Environments |  |  | B |  |  |  |  |
| 1341 |  |  |  | Software | Software Development Process | Debugging Tools |  |  | A |  |  |  |  |
| 1403 | JCT&CS | T001 | USCYBERCOM/J7 | Cyber Capabilities | Mission | \(U\) Modify cyber capabilites to detect and disrupt nation\-state cyber threat actors across the threat chain \(kill chain\) of adversary execution |  |  | 1 |  |  |  |  |
| 1404 | JCT&CS | T002 | USCYBERCOM/J7 | Cyber Capabilities | Mission | \(U\) Create or enhance cyberspace capabilities to compromise, deny, degrade, disrupt, destroy, or manipulate automated information systems\. |  |  | 1 |  |  |  |  |
| 1405 | JCT&CS | T003 | USCYBERCOM/J7 | Cyber Capabilities | Mission | \(U\) Create or enhance cyberspace solutions to enable surveillance and reconnaissance of automated information systems\. |  |  | 1 |  |  |  |  |
| 1406 | JCT&CS | T004 | USCYBERCOM/J7 | Cyber Capabilities | Mission | \(U\) Create or enhance cyberspace solutions to enable the defense of national interests\. |  |  | 1 |  |  |  |  |
| 1407 | JCT&CS | T005 | USCYBERCOM/J7 | Project | Agile | \(U\) Interpret customer requirements and evaluate resource and system constraints to create solution design specifications\. |  |  | 1 |  |  |  |  |
| 1408 | JCT&CS | T006 | USCYBERCOM/J7 | Project | Agile | \(U\) Reference capability repositories and other sources to identify existing capabilities which fully/partially meet customer requirements \(with or without modification\)\. |  |  | 1 |  |  |  |  |
| 1409 | JCT&CS | T007 | USCYBERCOM/J7 | Project | Agile | \(U\) Collaborate with stakeholders to identify and/or develop appropriate cyberspace solutions\. |  |  | 1 |  |  |  |  |
| 1410 | JCT&CS | T008 | USCYBERCOM/J7 | Programming | Kernel Development | \(U\) Analyze, modify, develop, debug and document software and applications which run in kernel and user space\. |  |  | 1 |  |  |  |  |
| 1411 | JCT&CS | T009 | USCYBERCOM/J7 | Programming | C | \(U\) Analyze, modify, develop, debug and document software and applications in C programming language\. |  |  | 2 |  |  |  |  |
| 1412 | JCT&CS | T010 | USCYBERCOM/J7 | Programming | Python | \(U\) Analyze, modify, develop, debug and document software and applications in Python programming language\. |  |  | 2 |  |  |  |  |
| 1413 | JCT&CS | T011 | USCYBERCOM/J7 | Network | Network Analysis/Programming | \(U\) Analyze, modify, develop, debug and document software and applications utilizing standard, non\-standard, specialized, and/or unique network communication protocols\. |  |  | 1 |  |  |  |  |
| 1414 | JCT&CS | T012 | USCYBERCOM/J7 | Programming | Software Engineering | \(U\) Analyze, modify, develop, debug and document custom interface specifications between interconnected systems\. |  |  | 1 |  |  |  |  |
| 1415 | JCT&CS | T013 | USCYBERCOM/J7 | Project | Version Control | \(U\) Manage source code using version control \(e\.g\. Git\) |  |  | 2 |  |  |  |  |
| 1416 | JCT&CS | T014 | USCYBERCOM/J7 | Programming | DevOps | \(U\) Develop, modify, and utilize automation technologies to enable employment of capabilities as efficiently as possible \(e\.g\. TDD, CI/CD, etc\.\) |  |  | 1 |  |  |  |  |
| 1417 | JCT&CS | T015 | USCYBERCOM/J7 | Software | Software Testing | \(U\) Build, test, and modify prototypes using representative environments\. |  |  | 1 |  |  |  |  |
| 1418 | JCT&CS | T016 | USCYBERCOM/J7 | Software | Software Testing | \(U\) Analyze and correct technical problems encountered during testing and implementation of cyberspace solutions\. |  |  | 1 |  |  |  |  |
| 1419 | JCT&CS | T017 | USCYBERCOM/J7 | Programming | Assembly Programming | \(U\) Analyze, modify, develop, debug and document software and applications using assembly languages\. |  |  | 1 |  |  |  |  |
| 1420 | JCT&CS | T018 | USCYBERCOM/J7 | Software | Reverse Engineering | \(U\) Utilize tools to decompile, disassembe, analzye, and reverse engineer compiled binaries\. |  |  | 1 |  |  |  |  |
| 1421 | JCT&CS | T019 | USCYBERCOM/J7 | Programming | Programming | \(U\) Make use of compiler attributes and platform\-specific features\. |  |  | 0 |  |  |  |  |
| 1422 | JCT&CS | T020 | USCYBERCOM/J7 | Programming | Debugging | \(U\) Perform static and dynamic analysis in order to find errors and flaws\. |  |  | 1 |  |  |  |  |
| 1423 | JCT&CS | T021 | USCYBERCOM/J7 | Security | Reverse Engineering | \(U\) Conduct hardware and/or software static and dynamic analysis to reverse engineer malicious or benign systems |  |  | 1 |  |  |  |  |
| 1424 | JCT&CS | T022 | USCYBERCOM/J7 | Security | Vulnerability Research | \(U\) Research, reference, discover, analyze, modify, develop and document known and new vulnerabilities in computer systems\. |  |  | 0 |  |  |  |  |
| 1425 | JCT&CS | T023 | USCYBERCOM/J7 | Programming | Software Engineering | \(U\) Design and develop data requirements, database structure, process flow, systematic procedures, algorithms, data analysis, and file structures\. |  |  | 1 |  |  |  |  |
| 1426 | JCT&CS | T024 | USCYBERCOM/J7 | Programming | Software Engineering | \(U\) Utilize data structures to organize, sort, and manipulate elements of information |  |  | 2 |  |  |  |  |
| 1427 | JCT&CS | T025 | USCYBERCOM/J7 | Programming | Software Engineering | \(U\) Design and develop user interfaces \(e\.g\. web pages, GUIs, CLIs, Console Interfaces\) |  |  | 1 |  |  |  |  |
| 1428 | JCT&CS | T026 | USCYBERCOM/J7 | Programming | Software Engineering | \(U\) Utilize secure coding techniques during development of software and applications |  |  | 1 |  |  |  |  |
| 1429 | JCT&CS | T027 | USCYBERCOM/J7 | Security | Implement Security Measures | \(U\) Employ tradecraft to provide confidentiality, integrity, availability, authentication, and non\-repudiation of developed accesses, payloads and tools\. |  |  | 1 |  |  |  |  |
| 1430 | JCT&CS | T028 | USCYBERCOM/J7 | Security | Implement Security Measures | \(U\) Apply cryptography primitives to protect the confidentiality and integrity of sensitive data\. |  |  | 1 |  |  |  |  |
| 1431 | JCT&CS | T029 | USCYBERCOM/J7 | Programming | Secure Coding | \(U\) Perform code review and analysis to inform OPSEC analysis and application \(attribution, sanitization, etc\.\) |  |  | 0 |  |  |  |  |
| 1432 | JCT&CS | T030 | USCYBERCOM/J7 | Programming | Secure Coding | \(U\) Produce artifacts to inform risk analysis, acceptance testing, and legal review\. |  |  | 1 |  |  |  |  |
| 1433 | JCT&CS | T031 | USCYBERCOM/J7 | Standardization | Standards | \(U\) Locate and utilize technical specifications and industry standards \(e\.g\. Internet Engineering Task Force \(IETF\), IEEE, IEC, International Standards Organization \(ISO\)\)\. |  |  | 1 |  |  |  |  |
| 1434 | JCT&CS | T032 | USCYBERCOM/J7 | Standardization | Technical Writing | \(U\) Deliver technical documentation \(e\.g\. user guides, design documentation, test plans, change logs, training materials, etc\.\) to end users\. |  |  | 1 |  |  |  |  |
| 1435 | JCT&CS | T033 | USCYBERCOM/J7 | Standardization | Project Management | \(U\) Implement project management, software engineering philosophies, modern capability development methodologies \(Agile, TDD, CI/CD, etc\) |  |  | 1 |  |  |  |  |
| 1436 | JCT&CS | T034 | USCYBERCOM/J7 | Standardization | Software Engineering | \(U\) Apply software engineering best practices to enable sustainability and extesibility\. |  |  | 1 |  |  |  |  |
| 1437 | JCT&CS | T035 | USCYBERCOM/J7 | Standardization | Project Management Tools | \(U\) Enter work into Task and project management tools used for software development \(e\.g\. Jira, Confluence, Trac, MediaWiki, etc\.\) |  |  | 1 |  |  |  |  |
| 1438 | JCT&CS | T036 | USCYBERCOM/J7 | Cyber Capabilities | Software Engineering | \(U\) Enhance capability design strategies and tactics by synthesizing information, processes, and techniques in the areas of malicious software, vulnerabilities, reverse engineering, secure software engineering, and exploitation\. |  |  | 0 |  |  |  |  |
| 1439 | JCT&CS | T037 | USCYBERCOM/J7 | Standardization | Lessons Learned | \(U\) Document and communicate tradecraft, best practices, TTPs, training, briefings, presentations, papers, studies, lessons learned, etc\. to both technical and non\-technical audiences |  |  | 0 |  |  |  |  |
| 1440 | JCT&CS | KSA1 | USCYBERCOM/J7 | Organization | USCC Basic Info | \(U\) Explain US Cyber Command  authorities, responsibilities, and contributions to the National Cyber Mission\. |  |  | B |  |  |  |  |
| 1441 | JCT&CS | KSA2 | USCYBERCOM/J7 | Organization | Organizational Partner Info | \(U\) Describe mission and organizational partners, including information needs, mission, structure, capabilities, etc\. |  |  | B |  |  |  |  |
| 1442 | JCT&CS | KSA3 | USCYBERCOM/J7 | Organization | Cyber Mission Forces | \(U\) Describe cyber mission force equipment taxonomy \(Platform\-Access\-Payloads/Toolset\), capability development process and repository |  |  | A |  |  |  |  |
| 1443 | JCT&CS | KSA4 | USCYBERCOM/J7 | Organization | USCC Basic Info | \(U\) Describe USCYBERCOM legal, policy, and compliance requirements relevant to capability development\. |  |  | A |  |  |  |  |
| 1444 | JCT&CS | KSA5 | USCYBERCOM/J7 | Organization | Threats & Vulnerabilities | \(U\) Identify cyber adversary threat tier taxonomy \(2014 National Intellegence Estimate \(NIE\)\), DIA/NSA Standard Cyber Threat Model, etc\.\) |  |  | A |  |  |  |  |
| 1445 | JCT&CS | KSA6 | USCYBERCOM/J7 | Organization | USCC Basic Info | \(U\) Describe sources and locations of cyber capability registries and repositories \(E\.g\. Cyber Capability Registry \(CCR\), Agency and service repositories, etc\.\) |  |  | A |  |  |  |  |
| 1446 | JCT&CS | KSA7 | USCYBERCOM/J7 | Organization | USCC Basic Info | \(U\) Describe sources and locations \(public and classified\) of capability development TTPs and tradecraft information/intelligence used by the US Gov and others\. |  |  | A |  |  |  |  |
| 1447 | JCT&CS | KSA8 | USCYBERCOM/J7 | Network | Networking Concepts | \(U\) Describe Network Classes and their key characteristics, and common payload & access approaches \(T1, T2, T3\)  \(General Purpose, Telepresence, Commercial Mobile, Control Systems, Battlefield Systems\) |  |  | A |  |  |  |  |
| 1448 | JCT&CS | KSA9 | USCYBERCOM/J7 | Organization | Supported Organization Processes | \(U\) Describe supported organizations requirements processes\. |  |  | A |  |  |  |  |
| 1449 | JCT&CS | KSA10 | USCYBERCOM/J7 | Organization | Supported Organization Processes | \(U\) Describe the supported organization's approval process for operational use of a capability\. |  |  | A |  |  |  |  |
| 1450 | JCT&CS | KSA11 | USCYBERCOM/J7 | Organization | Mission | \(U\) Describe relevant mission processes including version control processes, release processes, documentation requirements, and testing requirements\. |  |  | B |  |  |  |  |
| 1451 | JCT&CS | KSA12 | USCYBERCOM/J7 | Programming | Software Engineering | \(U\) Explain how to apply modern software engineering practices\. |  |  | B |  |  |  |  |
| 1452 | JCT&CS | KSA13 | USCYBERCOM/J7 | Programming | Software Development Process | \(U\) Describe modern software development methodologies \(e\.g\. Continuous Integration \(CI\), Continuous Delivery \(CD\), Test Driven Development \(TDD\), etc\.\) |  |  | B |  |  |  |  |
| 1453 | JCT&CS | KSA14 | USCYBERCOM/J7 | Organization | Project Management | \(U\) Explain your organizations project management, timeline estimation, and software engineering philosophy \(e\.g\. CI/CD, TDD, etc\.\) |  |  | B |  |  |  |  |
| 1454 | JCT&CS | KSA15 | USCYBERCOM/J7 | Standardization | Software Testing | \(U\) Describe principles, methodologies, and tools used to improve quality of software \(e\.g\. regression testing, test coverage, code review, pair programming, etc\.\) |  |  | A |  |  |  |  |
| 1455 | JCT&CS | KSA16 | USCYBERCOM/J7 | Software | OS | \(U\) Explain terms and concepts of operating system fundamentals \(e\.g\. File systems, I/O, Memory Management, Process Abstraction, etc\.\) |  |  | B |  |  |  |  |
| 1456 | JCT&CS | KSA17 | USCYBERCOM/J7 | Programming | Software Engineering Fundamentals | \(U\) Explain basic programming concepts \(e\.g\., levels, structures, compiled vs\. interpreted languages\)\. |  |  | C |  |  |  |  |
| 1457 | JCT&CS | KSA18 | USCYBERCOM/J7 | Programming | Software Engineering Fundamentals | \(U\) Explain procedural and object\-oriented programming paradigms\. |  |  | B |  |  |  |  |
| 1458 | JCT&CS | KSA19 | USCYBERCOM/J7 | Programming | Standards | \(U\) Explain how to utilize reference documentation for C, Python, assembly, and other international technical standards and specifications \(IEEE, ISO, IETF, etc\.\) |  |  | B |  |  |  |  |
| 1459 | JCT&CS | KSA20 | USCYBERCOM/J7 | Programming | C | \(U\) Explain the purpose and usage of elements and components of the C programming language |  |  | B |  |  |  |  |
| 1460 | JCT&CS | KSA21 | USCYBERCOM/J7 | Programming | Python | \(U\) Explain the purpose and usage of elements and components of the Python programming language |  |  | B |  |  |  |  |
| 1461 | JCT&CS | KSA22 | USCYBERCOM/J7 | Programming | Assembly Programming | \(U\) Explain the purpose and usage of elements of Assembly language \(e\.g\., x86 and x86\_64, ARM, PowerPC\)\. |  |  | B |  |  |  |  |
| 1462 | JCT&CS | KSA23 | USCYBERCOM/J7 | Programming | Software Engineering | \(U\) Explain the use and application of static and dynamic program analysis\. |  |  | A |  |  |  |  |
| 1463 | JCT&CS | KSA24 | USCYBERCOM/J7 | Security | Reverse Engineering | \(U\) Explain what it is to decompile, disassembe, analzye, and reverse engineer compiled binaries\. |  |  | A |  |  |  |  |
| 1464 | JCT&CS | KSA25 | USCYBERCOM/J7 | Security | Reverse Engineering | \(U\) Identify commonly used tools to decompile, disassembe, analzye, and reverse engineer compiled binaries\. |  |  | A |  |  |  |  |
| 1465 | JCT&CS | KSA26 | USCYBERCOM/J7 | Programming | Software Engineering | \(U\) Explain software optimization techniques\. |  |  | B |  |  |  |  |
| 1466 | JCT&CS | KSA27 | USCYBERCOM/J7 | Network | Networking Concepts | \(U\) Explain terms and concepts of networking protocols \(e\.g\. Ethernet, IP, DHCP, ICMP, SMTP, DNS, TCP/OSI, etc\.\) |  |  | C |  |  |  |  |
| 1467 | JCT&CS | KSA28 | USCYBERCOM/J7 | Network | Networking Concepts | \(U\) Describe and explain physical and logical network infrastructure, to include hubs, switches, routers, firewalls, etc\. |  |  | C |  |  |  |  |
| 1468 | JCT&CS | KSA29 | USCYBERCOM/J7 | Network | Networking Concepts | \(U\) Explain network defense mechanisms\. \(e\.g\., encryption, authentication, detection, perimeter protection\)\. |  |  | B |  |  |  |  |
| 1469 | JCT&CS | KSA30 | USCYBERCOM/J7 | Software | Data Serialization | \(U\) Explain data serialization formats \(e\.g\. XML, JSON, etc\.\) |  |  | B |  |  |  |  |
| 1470 | JCT&CS | KSA31 | USCYBERCOM/J7 | Programming | Data Structures | \(U\) Explain the concepts and terminology of datastructures and associated algorithms \(e\.g\., search, sort, traverse, insert, delete\)\. |  |  | B |  |  |  |  |
| 1471 | JCT&CS | KSA32 | USCYBERCOM/J7 | Programming | Secure Coding | \(U\) Explain terms and concepts of secure coding practices\. |  |  | B |  |  |  |  |
| 1472 | JCT&CS | KSA33 | USCYBERCOM/J7 | Security | Network Security | \(U\) Describe capability OPSEC analysis and application \(attribution, sanitization, etc\.\) |  |  | A |  |  |  |  |
| 1473 | JCT&CS | KSA34 | USCYBERCOM/J7 | Security | Network Security | \(U\) Explain standard, non\-standard, specialized, and/or unique communication techniques used to provide confidentiality, integrity, availability, authentication, and non\-repudiation \. |  |  | A |  |  |  |  |
| 1474 | JCT&CS | KSA35 | USCYBERCOM/J7 | Security | Cryptography | \(U\) Describe common pitfalls surrounding the use, design, and implementation of cryptographic facilities\. |  |  | A |  |  |  |  |
| 1475 | JCT&CS | KSA36 | USCYBERCOM/J7 | Security | Cryptography | \(U\) Demonstrate understanding of pervasive cryptographic primitives\. |  |  | A |  |  |  |  |
| 1476 | JCT&CS | KSA37 | USCYBERCOM/J7 | Project | Project Management | \(U\) Explain Task and project management tools used for software development \(e\.g\. Jira, Confluence, Trac, MediaWiki, etc\.\) |  |  | B |  |  |  |  |
| 1477 | JCT&CS | KSA38 | USCYBERCOM/J7 | Organization | USCC Basic Info | \(U\) Identify cyber development conferences used to share development and community of interest information \(e\.g\. CNEDev, MTEM, etc\.\) |  |  | A |  |  |  |  |
