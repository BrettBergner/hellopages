# Senior PO Table

Note: Use left/right arrows to scroll the table left/right.

| UID | Req Src | Req Src ID | Requirement Owner\(s\) | Category | Topic/Language/Tool/Application | TASK | SUB TASK | Senior PO Crit Task | Senior PO | COMMENTS |  |  |  |
| :--- | :--- | :--- | :--- | :--- | :--- | :--- | :--- | :--- | :--- | :--- | :--- | :--- | :--- |
| 857 |  |  |  | Project | Agile | Examining the Product Owner Role | Analyze the skills, capabilities and practices of a Product Owner to help the organization realize value\. |  | 3c |  |  |  |  |
| 858 |  |  |  | Project | Agile | Examining the Product Owner Role | Illustrate why Scrum and Product Ownership is important\. |  | 2b |  |  |  |  |
| 863 |  |  |  | Project | Agile | Working with Stakeholders | Identify at least four major stakeholder groups\. |  | B |  |  |  |  |
| 864 |  |  |  | Project | Agile | Working with Stakeholders | Demonstrate at least three techniques to interact with stakeholders over multiple Sprints\. |  | 2b |  |  |  |  |
| 865 |  |  |  | Project | Agile | Working with Stakeholders | Describe at least three techniques to collaborate with the key stakeholders\. |  | B |  |  |  |  |
| 866 |  |  |  | Project | Agile | Working with Stakeholders | Using two concrete examples, recognize when the Product Owner should not act as the facilitator for the stakeholders\. |  | C |  |  |  |  |
| 867 |  |  |  | Project | Agile | Working with Stakeholders | Demonstrate at least three facilitative listening techniques\. |  | 2b |  |  |  |  |
| 868 |  |  |  | Project | Agile | Working with Stakeholders | List at least three alternatives to open discussion\. |  | A |  |  |  |  |
| 869 |  |  |  | Project | Agile | Working with Stakeholders | Demonstrate one alternative to open discussion\. |  | 2b |  |  |  |  |
| 870 |  |  |  | Project | Agile | Working with Stakeholders | Identify at least three indicators when a group is engaged in divergent thinking and at least three indicators where a group is engaged in convergent thinking\. |  | B |  |  |  |  |
| 871 |  |  |  | Project | Agile | Working with Stakeholders | Describe at least three challenges of integrating multiple perspectives\. |  | B |  |  |  |  |
| 872 |  |  |  | Project | Agile | Working with Stakeholders | Describe at least three ways a group of stakeholders could reach their final decision\. |  | B |  |  |  |  |
| 874 |  |  |  | Project | Agile | Working with the Development Team | Define technical debt and explain why the Product Owner should be cautious about accumulating technical debt\. |  | C |  |  |  |  |
| 875 |  |  |  | Project | Agile | Working with the Development Team | List at least five development practices that will help Scrum Teams deliver a high\-quality product Increment and reduce technical debt each Sprint\. |  | A |  |  |  |  |
| 876 |  |  |  | Project | Agile | Working with the Development Team | List at least three ways development practices may impact the Product Owner's ability to maximize business value each Sprint\. |  | A |  |  |  |  |
| 878 |  |  |  | Project | Agile | Product Ownership with Multiple Teams | Describe at least two different scaling frameworks or approaches\. |  | B |  |  |  |  |
| 879 |  |  |  | Project | Agile | Product Ownership with Multiple Teams | Describe the benefits of managing dependencies when compared to reducing/removing dependencies\. |  | C |  |  |  |  |
| 880 |  |  |  | Project | Agile | Product Ownership with Multiple Teams | Describe at least three techniques for visualizing, managing, or reducing dependencies or constraints\. |  | B |  |  |  |  |
| 885 |  |  |  | Project | Agile | Developing Practical Product Strategies | Discuss a real\-world example of how product strategy is operationalized and evolves over time in an Agile organization\. |  | B |  |  |  |  |
| 886 |  |  |  | Project | Agile | Developing Practical Product Strategies | Demonstrate at least two approaches to identify purpose or define strategy\. |  | 2b |  |  |  |  |
| 887 |  |  |  | Project | Agile | Product Planning and Forecasting | Create a product plan or forecast with stakeholders\. |  | 3c |  |  |  |  |
| 888 |  |  |  | Project | Agile | Product Planning and Forecasting | Demonstrate how to plan a product release\. |  | 3c |  |  |  |  |
| 896 |  |  |  | Project | Agile | Customer Research and Discover | Use one technique to connect teams directly to customers and users\. |  | 3c |  |  |  |  |
| 897 |  |  |  | Project | Agile | Customer Research and Discover | Demonstrate at least two techniques of product discovery\. |  | 2c |  |  |  |  |
| 898 |  |  |  | Project | Agile | Customer Research and Discover | Demonstrate at least one technique to visualize and communicate product strategy, product ideas, features, and assumptions\. |  | 3c |  |  |  |  |
| 902 |  |  |  | Project | Agile | Validating Product Assumptions | List two cognitive biases that may impact the Product Owner's capability to effectively deliver business value\. |  | A |  |  |  |  |
| 903 |  |  |  | Project | Agile | Validating Product Assumptions | Appraise how effectively the Sprint Review is used to inspect and adapt based on the product Increment that was built in the Sprint\. |  | 2b |  |  |  |  |
| 904 |  |  |  | Project | Agile | Validating Product Assumptions | Compare at least three approaches to validating assumptions by their cost and the quality of learning\. |  | C |  |  |  |  |
| 905 |  |  |  | Project | Agile | Validating Product Assumptions | Demonstrate approaches to incorporate validating assumptions into the Scrum framework\. |  | 2b |  |  |  |  |
| 906 |  |  |  | Project | Agile | Validating Product Assumptions | Develop hypotheses for a target customer/user segment and create a plan to test one hypothesis\. |  | 2b |  |  |  |  |
| 919 |  |  |  | Project | Agile | Differentiating Outcome and Output | Describe one benefit of maximizing outcomes and impact while minimizing output |  | B |  |  |  |  |
| 920 |  |  |  | Project | Agile | Defining Value | Demonstrate at least two techniques to model value and use at least two techniques to measure value\. |  | 2b |  |  |  |  |
| 921 |  |  |  | Project | Agile | Ordering Items | Differentiate between at least four techniques to order a Product Backlog\. |  | B |  |  |  |  |
| 922 |  |  |  | Project | Agile | Ordering Items | Apply at least two techniques to organize and filter a Product Backlog to link to product goals or strategy\. |  | 3c |  |  |  |  |
| 923 |  |  |  | Project | Agile | Product Backlog Refinement | Demonstrate at least one technique to generate new Product Backlog items\. |  | 3c |  |  |  |  |
| 924 |  |  |  | Project | Agile | Product Backlog Refinement | Illustrate how the Product Owner can ensure that enough Product Backlog items are "ready" for the upcoming Sprint\. |  | C |  |  |  |  |
| 925 |  |  |  | Project | Agile | Product Backlog Refinement | Organize and facilitate a session with stakeholders to break down a solution or feature as progressively smaller items that may be completed in Sprints\. |  | 3c |  |  |  |  |
| 926 |  |  |  | Project | Agile | Product Backlog Refinement | Integrate feedback from at least three sources to generate and order Product Backlog items\. |  | 3c |  |  |  |  |
| 927 |  |  |  | Project | Agile | Product Backlog Refinement | Assess how to refine the Product Backlog and recognize at least three ways to improve\. |  | 4d |  |  |  |  |
| 935 |  |  |  | Project | 90 MQT | Project in Progress | Conduct an effective Product Backlog Grooming Meeting in accordance with squadron standards\. |  | C | http://mpls\-as\-701v:8090/display/9CMDP/%28U%29\+Guidance\+for\+Facilitating\+a\+Product\+Backlog\+Grooming\+Meeting |  |  |  |
| 939 |  |  |  | Project | 90 MQT | Project in Progress | Demonstrate the ability to create product burn\-down charts\. |  | B |  |  |  |  |
| 940 |  |  |  | Project | 90 MQT | Project in Progress | Discuss the advantages and disadvantages of using burn\-down charts\. |  | B |  |  |  |  |
| 961 |  |  |  | Project | Organization Overview | 90 IOS Mission and Vision |  |  | C |  |  |  |  |
| 962 |  |  |  | Project | Organization Overview | 90 IOS Organizational Structure |  |  | C |  |  |  |  |
| 963 |  |  |  | Project | Organization Overview | Real\-Time Operations and Innovation \(RTO&I\) Development Requirements |  |  | B |  |  |  |  |
| 964 |  |  |  | Project | Organization Overview | Capability Development Initiation |  |  | C |  |  |  |  |
| 965 |  |  |  | Project | Organization Overview | Flight Missions |  |  | B |  |  |  |  |
| 966 |  |  |  | Project | Supporting/Supported Organizations | 90 IOS\-Supported Organizations |  |  | C |  |  |  |  |
| 967 |  |  |  | Project | Supporting/Supported Organizations | Organizations Supporting 90 IOS |  |  | C |  |  |  |  |
| 968 |  |  |  | Project | Supporting/Supported Organizations | Mission Area Responsibilities of Supported/Supporting Organizations |  |  | B |  |  |  |  |
| 970 |  |  |  | Project | Cyber Mission Forces | Cyber Mission Forces \(CMF\) Teams |  |  | B |  |  |  |  |
| 971 |  |  |  | Project | Cyber Mission Forces | 90 IOS CMF\-Role |  |  | B |  |  |  |  |
| 972 |  |  |  | Project | Cyber Mission Forces | Interaction Between CMF Teams |  |  | B |  |  |  |  |
| 973 |  |  |  | Project | Cyber Mission Forces | 90 IOS CMF Teams |  |  | B |  |  |  |  |
| 974 |  |  |  | Project | Cyber Mission Forces | 90 IOS CMF Team Back\-Shop Support Requests/Receipt |  |  | B |  |  |  |  |
| 975 |  |  |  | Project | Exercises | Common 90 IOS Exercises |  |  | A |  |  |  |  |
| 977 |  |  |  | Project | Policy and Compliance | Executive Order |  |  | A |  |  |  |  |
| 978 |  |  |  | Project | Policy and Compliance | DoD Directive |  |  | A |  |  |  |  |
| 979 |  |  |  | Project | Policy and Compliance | NSA/CSS Policy |  |  | A |  |  |  |  |
| 980 |  |  |  | Project | Policy and Compliance | Federal Act |  |  | A |  |  |  |  |
| 981 |  |  |  | Project | Policy and Compliance | Signals Intelligence \(SIGINT\) |  |  | A |  |  |  |  |
| 982 |  |  |  | Project | Policy and Compliance | USSID |  |  | A |  |  |  |  |
| 983 |  |  |  | Project | Policy and Compliance | Classification Markings |  |  | A |  |  |  |  |
| 984 |  |  |  | Project | Policy and Compliance | United States Code Titles |  |  | A |  |  |  |  |
| 985 |  |  |  | Project | Policy and Compliance | Legal Targeting |  |  | C |  |  |  |  |
| 987 |  |  |  | Software | Windows Operating System | Basic Administration Using The Graphic User Interface \(GUI\) |  |  | B |  |  |  |  |
| 1354 |  |  |  | Project | Agile | Fundamentals of the Product Owner Role | Apply a thorough understanding of tools associated with JIRA to manage development team projects |  | 4d |  |  |  |  |
| 1361 |  |  |  | Project | Agile | Understanding Customers and Users | Relate customer expectations with team members for product development |  | 4d |  |  |  |  |
