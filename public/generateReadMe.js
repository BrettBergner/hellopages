var marked = require('marked');
var fs = require('fs');
var path = require('path'), fs=require('fs');

const filename_string = '';

function fromDir(startPath,filter){
    var file_array = []

    //console.log('Starting from dir '+startPath+'/');

    // Check if the start path doesnt exist
    if (!fs.existsSync(startPath)){
        console.log("no dir ",startPath);
        return;
    }

    // Get a list of files in the start path
    var files=fs.readdirSync(startPath);
    // Loop through those files and create a full path
    for(var i=0;i<files.length;i++){
        var filename=path.join(startPath,files[i]);
        var stat = fs.lstatSync(filename);
        // Check if the filename is actually a directory. If it is, recurse into it.
        if (stat.isDirectory()){
            fromDir(filename,filter); //recurse
        }
        // Check if that file has the extension
        else if (filename.indexOf(filter)>=0) {
            console.log('-- found: ',filename);
            //Start converting
            var mobile_report = fs.readFileSync(filename, 'utf-8');
            var markdown_report = marked(mobile_report);
            var html_file = path.basename(filename)
            // Strip file extension
            html_file = html_file.split('.').slice(0, -1).join('.')
            // Give it a path and html file extension
            html_file = '/site/' + html_file + '.html';
            console.log('-- writing file: ',html_file);
            console.log('-- file contents: ',markdown_report);
            file_array.push(html_file)
            fs.writeFileSync('./public' + html_file, markdown_report);  
        };
    };
    return file_array
};

// Build the index.html from the created array of convered files
function buildHtml(file_array) {
  console.log('-- file array: ', file_array);
  var header = '';
  var body = '';
  var object = '<object data="./site/Mobile.html" border="1" type="text/html" width="1400" height="500"></object><br>';
  var object_array = []
  
  file_array.forEach(function(item){
      console.log('-- item: ', item);
      object_array.push('<object data=".' + item + '" border="1" type="text/html" width="1400" height="500"></object><br>')
      // Need to convert object array to a string
  });
  console.log('-- object_array: ', object_array);
  console.log('-- object_array.join: ', object_array.join(""));

  return '<!DOCTYPE html>' +
'<html>' +
  '<head>' +
    '<meta charset="utf-8">' +
    '<meta name="generator" content="GitLab Pages">' +
    '<title>Plain HTML site using GitLab Pages</title>' +
    '<link rel="stylesheet" href="style.css">' +
  '</head>' +
  '<body>' +
    '<div class="navbar">' +
      '<a href="https://gitlab.com/90COS/">90th COS Repos</a>' +
      '<a href="https://gitlab.com/90COS/Private/evaluation/questions/">90th Test Bank Repo</a>' +
      '<a href="https://gitlab.com/90COS/public/mttl">MTTL Repo</a>' +
    '</div>' +

    '<h1>Hello Pages!</h1>' +
    '<p>A report converted from markdown to html formatted.</p>' +

    // object +
    object_array.join("") +
    // '<object data="./site/Mobile.html" border="1" type="text/html" width="1400" height="500"></object><br>' +
    // '<object data="./site/Mobile.html" border="1" type="text/html" width="1400" height="500"></object><br>' +

  '</body>' +
'</html>';
//   return '<!DOCTYPE html>'
//        + '<html><head>' + header + '</head><body>' + body + '</body></html>';
};

var file_array = fromDir('./public/combined_reports','.md');

var index_html = buildHtml(file_array);
fs.writeFileSync('public/index.html', index_html);

// var mobile_report = fs.readFileSync('./public/combined_reports/Mobile.md', 'utf-8');
// var markdown_mobile_report = marked(mobile_report);

// fs.writeFileSync('./public/site/Mobile.html', markdown_mobile_report);